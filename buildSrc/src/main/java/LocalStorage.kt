object LocalStorage { // Room + DataStore
    private const val room = "2.2.6"
    private const val store = "1.0.0-alpha06"
    const val roomCompiler = "androidx.room:room-compiler:${room}"
    const val roomKtx = "androidx.room:room-ktx:${room}"
    const val roomCoroutines = "androidx.room:room-coroutines:${room}"
    const val roomRuntime = "androidx.room:room-runtime:${room}"
    const val dataStore = "androidx.datastore:datastore-preferences:${store}"
}