object DI { // Koin
    const val koin = "3.0.2"
    const val koinExt = "io.insert-koin:koin-android-ext:${koin}"
    const val koinAndroid = "io.insert-koin:koin-android:${koin}"
}