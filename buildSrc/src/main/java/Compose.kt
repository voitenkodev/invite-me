object Compose { // jetpack compose
    const val composeVers = "1.0.0-beta08"
    const val activityComposeVers = "1.3.0-alpha07"

    const val ui = "androidx.compose.ui:ui:${composeVers}"
    const val tools = "androidx.compose.ui:ui-tooling:${composeVers}"
    const val foundation = "androidx.compose.foundation:foundation:${composeVers}"
    const val material = "androidx.compose.material:material:${composeVers}"
    const val materialIcons = "androidx.compose.material:material-icons-core:${composeVers}"
    const val materialIconsExt = "androidx.compose.material:material-icons-extended:${composeVers}"

    // additional
    const val activity = "androidx.activity:activity-compose:${activityComposeVers}"
}