object Firebase { // crashlytics + analytics
    private const val crashlyticsVers = "18.0.1"
    private const val analyticsVers = "19.0.0"
    private const val crashPlug = "2.3.0"
    private const val fabricToolsVers = "1.31.2"
    const val fabricRepo = "https://maven.fabric.io/public"
    const val crashlytics = "com.google.firebase:firebase-crashlytics:${crashlyticsVers}"
    const val analytics = "com.google.firebase:firebase-analytics:${analyticsVers}"
    const val crashlyticsPlugin = "com.google.firebase:firebase-crashlytics-gradle:${crashPlug}"
    const val fabricTools = "io.fabric.tools:gradle:${fabricToolsVers}"
    const val crashlyticsPluginId = "com.google.firebase.crashlytics"
}