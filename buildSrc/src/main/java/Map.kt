object Map { // MapBox
    private const val versionMap = "9.6.1"
    private const val markerVers = "0.9.0"
    const val mapRepository = "https://api.mapbox.com/downloads/v2/releases/maven"
    const val username = "mapbox"
    const val mapBox = "com.mapbox.mapboxsdk:mapbox-android-sdk:${versionMap}"
    const val marker = "com.mapbox.mapboxsdk:mapbox-android-plugin-annotation-v9:${markerVers}"

    const val cleanMapKey =
        "sk.eyJ1IjoibWF4aW52aXRlbWUiLCJhIjoiY2ttb252bTMxMGhtNTJwcnZlMzZ4dDV0dCJ9.wnvurSWnmv8SGz3K89Sb3A"
    const val mapKey = "\"$cleanMapKey\""
}