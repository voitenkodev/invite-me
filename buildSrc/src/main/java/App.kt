object App {
    const val compile = 30
    const val build = "30.0.2"
    const val min = 21
    const val target = 30
    const val versionCode = 10
    const val versionName = "1.4.5"
    const val appPackage = "com.voitenko.dev.inviteme"
}

object Other {
    const val kotlinVers = "1.5.10"
    private const val playcoreVers = "1.8.1"
    private const val lifeCycleVers = "2.2.0"
    private const val lifeCycleRuntimeVers = "2.4.0-alpha01"
    private const val coreKtxVers = "1.5.0"
    private const val coroutinesVers = "1.5.0"

    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${kotlinVers}"
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${coroutinesVers}"
    const val coreKtx = "androidx.core:core-ktx:${coreKtxVers}"
    const val playCore = "com.google.android.play:core-ktx:${playcoreVers}"
    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${lifeCycleVers}"
    const val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:${lifeCycleRuntimeVers}"

//    const val extensions = "androidx.lifecycle:lifecycle-extensions:${lifeCycleVers}"

    private const val coilVers = "1.2.2"
    private const val coilToolsVers = "0.11.1"
    const val coilTools = "com.google.accompanist:accompanist-coil:${coilToolsVers}"
    const val coil = "io.coil-kt:coil:${coilVers}"

    // View ----------------------------------------------------------------------------------------

    private const val appCompatVers = "1.3.0"
    private const val constraintVers = "2.0.4"
    private const val lottieVers = "3.7.0"
    private const val qrCodeVers = "3.4.1"
    private const val timePickerVers = "2.0"
    private const val scannerVers = "2.1.0"
    private const val fragmentApiVers = "1.3.4"

    const val appCompat = "androidx.appcompat:appcompat:${appCompatVers}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${constraintVers}"
    const val lottie = "com.airbnb.android:lottie:${lottieVers}"
    const val qrCode = "com.google.zxing:core:${qrCodeVers}"
    const val scanner = "com.budiyev.android:code-scanner:${scannerVers}"
    const val timePicker = "com.github.Kunzisoft:Android-SwitchDateTimePicker:${timePickerVers}"
    const val fragmentApi = "androidx.fragment:fragment-ktx:${fragmentApiVers}"
}

object Plugins {
    private const val googleServicesVers = "4.3.4"
    private const val updateDependVers = "0.34.0"
    private const val gradleVers = "7.0.0-beta03"
    const val googleServices = "com.google.gms:google-services:${googleServicesVers}"
    const val gradle = "com.android.tools.build:gradle:${gradleVers}"
    const val gradleKotlinPlug = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Other.kotlinVers}"
    const val updateDependencies = "com.github.ben-manes:gradle-versions-plugin:${updateDependVers}"
}

object Repo {
    const val jitpack = "https://jitpack.io"
    const val mavenGoogle = "https://maven.google.com"
}