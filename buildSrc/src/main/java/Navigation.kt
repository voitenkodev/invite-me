object Navigation { // Navigation Component + NavArgs
    private const val navigation = "2.3.5"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${navigation}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${navigation}"
    const val pluginArgs = "androidx.navigation.safeargs" // app
    const val navPlugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${navigation}" // global
}