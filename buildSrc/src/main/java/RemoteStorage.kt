object RemoteStorage { // Ktor + OkHttp + Serialization
    private const val ktor = "1.6.0"
    private const val serialization = "1.0.1"
    const val ktorSerializationLib = "io.ktor:ktor-client-serialization-jvm:${ktor}"
    const val ktorCoreLib = "io.ktor:ktor-client-core:${ktor}"
    const val ktorLoggingLib = "io.ktor:ktor-client-logging-jvm:${ktor}"
    const val ktorOkHttpLib = "io.ktor:ktor-client-okhttp:${ktor}"
    const val serializationLib = "org.jetbrains.kotlinx:kotlinx-serialization-core:${serialization}"
    const val serializationPluginLib =
        "org.jetbrains.kotlin:kotlin-serialization:${Other.kotlinVers}" // global
    const val serializationPluginId = "kotlinx-serialization" // app
}