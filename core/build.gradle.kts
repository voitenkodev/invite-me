plugins {
    id("com.android.library")
    id(RemoteStorage.serializationPluginId)
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdk = App.compile
    buildToolsVersion = App.build
    defaultConfig {
        minSdk = App.min
        targetSdk = App.target
        buildConfigField("String", "MAP_KEY", Map.mapKey)
    }

    buildTypes.getByName("release") {
        isMinifyEnabled = true
        val defGuard = getDefaultProguardFile("proguard-android-optimize.txt")
        proguardFiles(defGuard, ("proguard-rules.pro"))
    }

    testOptions.unitTests.isIncludeAndroidResources = true
    compileOptions.sourceCompatibility(JavaVersion.VERSION_11)
    compileOptions.targetCompatibility(JavaVersion.VERSION_11)
    configurations.all { resolutionStrategy.force("org.objenesis:objenesis:2.6") } // mockito force min-api 26
}

dependencies {
    implementation(Other.kotlin)
    implementation(Other.coroutines)
    implementation(Other.coreKtx)

    implementation(DI.koinExt)
    implementation(DI.koinAndroid)

    implementation(RemoteStorage.serializationLib)
    implementation(RemoteStorage.ktorCoreLib)
    implementation(RemoteStorage.ktorLoggingLib)
    implementation(RemoteStorage.ktorOkHttpLib)
    implementation(RemoteStorage.ktorSerializationLib)

    kapt(LocalStorage.roomCompiler)
    implementation(LocalStorage.roomKtx)
    implementation(LocalStorage.roomRuntime)
    implementation(LocalStorage.dataStore)

    // TESTING -------------------------------------------------------------------------------------

    testImplementation(Test.junit)
    testImplementation(Test.mockito)
    testImplementation(Test.testExt)
    testImplementation(Test.testCore)
    testImplementation(Test.roboElectric)
    testImplementation(Test.koinTest)
}