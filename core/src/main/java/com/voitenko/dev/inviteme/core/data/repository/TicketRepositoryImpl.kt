package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.datasource.local.LocalSource
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class TicketRepositoryImpl constructor(private val localSource: LocalSource) :
    TicketRepository {

    override fun findGuestOnEvent(eventId: Long, guestId: Long): Flow<Ticket?> =
        localSource.tableEventGuest().getTicket(eventId, guestId)

    override fun setTicket(ticket: Ticket) = flow {
        emit(
            localSource.tableEventGuest()
                .updateTicketStatus(ticket.eventId, ticket.guestId, ticket.status)
        )
    }
}
