package com.voitenko.dev.inviteme.core.data.models.models.search

import com.voitenko.dev.inviteme.core.data.models.models.EventApp

data class SearchEvent(val event: EventApp) : SearchItemCombine {
    override fun getSearchType() = SearchItemCombine.SearchItemType.SEARCH_EVENT
}