package com.voitenko.dev.inviteme.core.data.models.dto

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.voitenko.dev.inviteme.core.utils.DEFAULT_LONG
import java.util.*

@Entity
internal data class Event(
    @PrimaryKey(autoGenerate = true) var eventId: Long = DEFAULT_LONG,
    val title: String,
    val description: String,
    val date: Calendar,
    @Embedded val addressApp: Address,
    val imageLocalPath: String?
)
