package com.voitenko.dev.inviteme.core.domain.settings_uc

import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class TryToUpdateCountOfOpenUc : BaseUc() {

    private val settingsRepo by inject<SettingsRepositoryImpl>()

    fun invoke() = settingsRepo.getCountOpen().map { currentCount ->
        if (currentCount > 4) {
            settingsRepo.setCountOpen(0)
            false
        } else {
            settingsRepo.setCountOpen(currentCount + 1)
            true
        }
    }
}