package com.voitenko.dev.inviteme.core.data.models.models.guest_info

interface GuestInfoItemCombine {
    fun getGuestInfoType(): GuestInfoItemType

    enum class GuestInfoItemType { GUEST_INFO, EVENT_GUEST_INFO, SEARCH_INFO, PLACE_HOLDER, SPACE_BOX }
}
