package com.voitenko.dev.inviteme.core.domain.guest_uc

import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class GetGuestByIdUc : BaseUc() {

    private val guestRepo by inject<GuestRepositoryImpl>()

    fun invoke(guestId: Long) = guestRepo.getGuestById(guestId).map { it?.toGuestApp() }
}