package com.voitenko.dev.inviteme.core.data.models.models.address

import android.os.Parcelable
import com.voitenko.dev.inviteme.core.utils.DEFAULT_STRING
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressApp(
    val name: String,
    val latitude: Double?,
    val longitude: Double?
) : Parcelable {
    companion object {
        val EMPTY_ADDRESS = AddressApp(DEFAULT_STRING, null, null)
    }
}