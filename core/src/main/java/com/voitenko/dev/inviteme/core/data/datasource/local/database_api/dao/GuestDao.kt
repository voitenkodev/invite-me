package com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

@Dao
internal interface GuestDao {

    // COPY IN GuestWithEventDao
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(join: Ticket)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun _setGuest(guest: Guest): Long

    suspend fun setGuest(guest: Guest): Long = withContext(Dispatchers.IO) { _setGuest(guest) }

    suspend fun setGuestByEventId(guest: Guest, eventId: Long, status: Ticket.Status): Long =
        withContext(Dispatchers.IO) {
            val guestIdResult: Long = setGuest(guest)
            insert(Ticket(eventId, guest.guestId, status))
            guestIdResult
        }

    @Query("SELECT * FROM Guest WHERE guestId = :guestId")
    fun getGuest(guestId: Long): Flow<Guest>

    @Query("SELECT * FROM Guest WHERE email = :email")
    fun getGuestByEmail(email: String): Flow<Guest?>

    @Transaction
    @Query("SELECT * FROM Guest WHERE guestId = :guestId")
    fun getGuestWithEventsById(guestId: Long): Flow<GuestWithEvents>

    @Transaction
    @Query("SELECT * FROM Guest")
    fun getGuestsWithEvents(): Flow<List<GuestWithEvents>>

    @Query("SELECT * FROM Guest")
    fun getGuests(): Flow<List<Guest>>

    @Query("SELECT * FROM Guest WHERE lower(guest.guestName) LIKE '%' || :query || '%' OR lower(guest.email) LIKE '%' || :query || '%' OR lower(guest.phone) LIKE '%' || :query || '%'  LIMIT :limit")
    fun getGuestsByQuery(query: String, limit: Int): Flow<List<Guest>>
}