package com.voitenko.dev.inviteme.core.data.models.models

data class GuestWithEventsApp(
    val guest: GuestApp,
    val events: List<EventApp>
)