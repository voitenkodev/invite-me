package com.voitenko.dev.inviteme.core.di

import androidx.datastore.preferences.createDataStore
import com.voitenko.dev.inviteme.core.data.datasource.local.RoomSourceImpl
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.roomClient
import com.voitenko.dev.inviteme.core.domain.event_uc.GetEventByIdUc
import com.voitenko.dev.inviteme.core.domain.event_uc.GetEventWithGuestsByIdUc
import com.voitenko.dev.inviteme.core.domain.event_uc.GetSortedEventsWithGuestsUc
import com.voitenko.dev.inviteme.core.domain.event_uc.SetEventUc
import com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc.FindGuestOnEventUc
import com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc.SetStatusTicketUc
import com.voitenko.dev.inviteme.core.domain.guest_event_uc.SearchGuestAndEventsUc
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestByEmailUc
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestByIdUc
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestWithEventsByIdUc
import com.voitenko.dev.inviteme.core.domain.guest_uc.SetGuestUc
import com.voitenko.dev.inviteme.core.domain.location_uc.GetLocationsByQueryUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.GetNightDayModeUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.SetDayModeUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.SetNightModeUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.TryToUpdateCountOfOpenUc
import com.voitenko.dev.inviteme.core.data.datasource.remote.address_api.AddressApi
import com.voitenko.dev.inviteme.core.data.datasource.preferences.SettingsPrefImpl
import com.voitenko.dev.inviteme.core.data.datasource.remote.KtorSourceImpl
import com.voitenko.dev.inviteme.core.data.datasource.remote.address_api.ktorClient
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import com.voitenko.dev.inviteme.core.data.repository.TicketRepositoryImpl
import com.voitenko.dev.inviteme.core.data.repository.LocationRepositoryImpl
import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

@ExperimentalStdlibApi
internal val ucModule = module {
    factory { GetSortedEventsWithGuestsUc() }
    factory { GetNightDayModeUc() }
    factory { TryToUpdateCountOfOpenUc() }
    factory { SetNightModeUc() }
    factory { SetDayModeUc() }
    factory { SearchGuestAndEventsUc() }
    factory { SetGuestUc() }
    factory { GetGuestByEmailUc() }
    factory { GetEventWithGuestsByIdUc() }
    factory { GetEventByIdUc() }
    factory { GetGuestWithEventsByIdUc() }
    factory { GetGuestByIdUc() }
    factory { FindGuestOnEventUc() }
    factory { SetStatusTicketUc() }
    factory { SetEventUc() }
    factory { GetLocationsByQueryUc() }
}

internal val repoModule = module {
    single { LocationRepositoryImpl(get<KtorSourceImpl>()) }
    single { EventRepositoryImpl(get<RoomSourceImpl>()) }
    single { GuestRepositoryImpl(get<RoomSourceImpl>()) }
    single { TicketRepositoryImpl(get<RoomSourceImpl>()) }
    single { SettingsRepositoryImpl(get<SettingsPrefImpl>()) }
}

internal val storageModule = module {
    single { ktorClient() }
    single { roomClient(androidContext()) }

    single { AddressApi(get()) }

    single { KtorSourceImpl(get()) }
    single { RoomSourceImpl(get()) }

    single { androidContext().createDataStore(name = "${androidContext().packageName}.settings") }
    single { SettingsPrefImpl(get()) }
}