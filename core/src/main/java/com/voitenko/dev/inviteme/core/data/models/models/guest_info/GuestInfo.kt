package com.voitenko.dev.inviteme.core.data.models.models.guest_info

import com.voitenko.dev.inviteme.core.data.models.models.GuestApp

data class GuestInfo(val guest: GuestApp) : GuestInfoItemCombine {
    override fun getGuestInfoType() = GuestInfoItemCombine.GuestInfoItemType.GUEST_INFO
}