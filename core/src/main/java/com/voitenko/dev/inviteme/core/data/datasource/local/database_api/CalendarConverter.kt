package com.voitenko.dev.inviteme.core.data.datasource.local.database_api

import androidx.room.TypeConverter
import java.util.Calendar
import java.util.GregorianCalendar

internal class CalendarConverter {

    @TypeConverter
    fun fromTimestamp(value: Long?): Calendar? = value?.let { timeInMillis ->
        GregorianCalendar().also { calendar -> calendar.timeInMillis = timeInMillis }
    }

    @TypeConverter
    fun toTimestamp(timestamp: Calendar?): Long? = timestamp?.timeInMillis
}
