package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.datasource.local.LocalSource
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class EventRepositoryImpl(private val localSource: LocalSource) :
    EventRepository {

    override fun getEvents(): Flow<List<Event>> =
        localSource.tableEvent().getEvents()

    override fun getEventById(eventId: Long): Flow<Event> =
        localSource.tableEvent().getEvent(eventId)

    override fun getEventsByQuery(query: String, limit: Int): Flow<List<Event>> =
        localSource.tableEvent().getEventsByQuery(query.lowercase(), limit)

    override fun getSortedEventsWithGuests() =
        localSource.tableEvent().getSortedEventsWithGuests()

    override fun getEventWithGuestById(eventId: Long): Flow<EventWithGuests> =
        localSource.tableEvent().getEventWithGuestsById(eventId)

    override fun setEvent(event: Event): Flow<Long> =
        flow { emit(localSource.tableEvent().setEvent(event)) }
}