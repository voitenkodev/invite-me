package com.voitenko.dev.inviteme.core.data.models.models.search

class SearchHeader(val type: SearchHeaderType) : SearchItemCombine {
    override fun getSearchType() = SearchItemCombine.SearchItemType.SEARCH_HEAD

    enum class SearchHeaderType { EVENT_HEADER, GUEST_HEADER }
}
