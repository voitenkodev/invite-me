package com.voitenko.dev.inviteme.core.data.models.models.event_info

import com.voitenko.dev.inviteme.core.data.models.models.GuestApp

data class GuestEventInfo(val guest: GuestApp) : EventInfoItemCombine {
    override fun getEvenInfoType() = EventInfoItemCombine.EventInfoItemType.GUEST_EVENT_INFO
}