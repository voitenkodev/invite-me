package com.voitenko.dev.inviteme.core.data.datasource.remote

import com.voitenko.dev.inviteme.core.data.datasource.remote.address_api.AddressApi
import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress

internal class KtorSourceImpl(private val api: AddressApi) : RemoteSource {

    override suspend fun loadStreetsByLatLng(lat: Double, lng: Double): MapBoxAddress? =
        api.loadStreetsByLatLng(lat, lng)
}