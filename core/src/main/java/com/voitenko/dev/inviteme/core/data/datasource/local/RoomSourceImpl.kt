package com.voitenko.dev.inviteme.core.data.datasource.local

import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.AppDataBase

internal class RoomSourceImpl constructor(private val appDataBase: AppDataBase) : LocalSource {

    override fun tableEventGuest() = appDataBase.daoEventGuest()

    override fun tableEvent() = appDataBase.daoEvent()

    override fun tableGuest() = appDataBase.daoGuest()
}