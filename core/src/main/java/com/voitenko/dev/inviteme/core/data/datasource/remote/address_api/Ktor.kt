package com.voitenko.dev.inviteme.core.data.datasource.remote.address_api

import android.util.Log
import com.voitenko.dev.inviteme.core.BuildConfig
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*

class ExpectedNetworkThrowable(val serverMessage: String?) : Throwable()

internal fun ktorClient() = HttpClient(OkHttp) {
    install(JsonFeature) {
        serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
            isLenient = true; ignoreUnknownKeys = true; prettyPrint = true
        })
    }
    install(Logging) {
        level = LogLevel.ALL
        logger = object : Logger {
            override fun log(message: String) {
                Log.d("ktor", message)
            }
        }
    }
    defaultRequest {
        host = "api.mapbox.com"
        url { protocol = URLProtocol.HTTPS }
        parameter("access_token", BuildConfig.MAP_KEY)
        contentType(ContentType.Application.Json)
    }
}