package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import kotlinx.coroutines.flow.Flow

internal interface TicketRepository {

    fun findGuestOnEvent(eventId: Long, guestId: Long): Flow<Ticket?>
    fun setTicket(ticket: Ticket): Flow<Unit>
}