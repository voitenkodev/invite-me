package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress
import kotlinx.coroutines.flow.Flow

internal interface LocationRepository {
    fun getAddressByLatLng(lat: Double, lng: Double): Flow<MapBoxAddress?>
}