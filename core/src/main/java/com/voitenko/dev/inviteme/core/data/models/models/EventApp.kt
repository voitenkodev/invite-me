package com.voitenko.dev.inviteme.core.data.models.models

import android.net.Uri
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.utils.toUiString
import java.util.*

data class EventApp(
    var eventId: Long,
    val title: String,
    val description: String,
    val date: Calendar,
    val addressApp: AddressApp,
    val imageLocalPath: String?,
) {
    fun getDateInString() = date.toUiString()
    fun getImageInUri(): Uri = imageLocalPath?.let { return@let Uri.parse(it) } ?: Uri.EMPTY
}
