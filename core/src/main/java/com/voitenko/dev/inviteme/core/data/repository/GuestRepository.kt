package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import kotlinx.coroutines.flow.Flow

internal interface GuestRepository {

    fun getGuests(): Flow<List<Guest>>
    fun getGuestByEmail(email: String): Flow<Guest?>
    fun getGuestById(guestId: Long): Flow<Guest?>
    fun getGuestsByQuery(query: String, limit: Int): Flow<List<Guest>>
    fun getGuestsWithEvents(): Flow<List<GuestWithEvents>>
    fun getGuestWithEventsByGuestId(guestId: Long): Flow<GuestWithEvents>
    fun setGuest(guest: Guest, eventId: Long, status: Ticket.Status): Flow<Long>
}