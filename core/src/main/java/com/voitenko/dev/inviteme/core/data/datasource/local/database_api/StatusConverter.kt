package com.voitenko.dev.inviteme.core.data.datasource.local.database_api

import androidx.room.TypeConverter
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket

internal class StatusConverter {

    @TypeConverter
    fun toStatus(value: String) = enumValueOf<Ticket.Status>(value)

    @TypeConverter
    fun fromStatus(value: Ticket.Status) = value.name
}