package com.voitenko.dev.inviteme.core.data.models.models

import android.net.Uri
import com.voitenko.dev.inviteme.core.utils.DEFAULT_LONG
import com.voitenko.dev.inviteme.core.utils.DEFAULT_STRING

data class GuestApp(
    var guestId: Long,
    val guestName: String,
    val email: String,
    val phone: String?,
    val imageLocalPath: String?
) {

    fun getImageInUri(): Uri = imageLocalPath?.let { return@let Uri.parse(it) } ?: Uri.EMPTY

    companion object {
        val EMPTY_GUEST = GuestApp(
            DEFAULT_LONG,
            DEFAULT_STRING,
            DEFAULT_STRING,
            null,
            null
        )
    }
}