package com.voitenko.dev.inviteme.core.data.datasource.local

import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.EventDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestWithEventDao

internal interface LocalSource {

    fun tableEventGuest(): GuestWithEventDao

    fun tableEvent(): EventDao

    fun tableGuest(): GuestDao
}