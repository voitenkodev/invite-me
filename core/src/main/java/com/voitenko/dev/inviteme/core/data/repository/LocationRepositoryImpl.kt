package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.datasource.remote.RemoteSource
import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class LocationRepositoryImpl(private val remoteSource: RemoteSource) :
    LocationRepository {

    override fun getAddressByLatLng(lat: Double, lng: Double): Flow<MapBoxAddress?> =
        flow { emit(remoteSource.loadStreetsByLatLng(lat, lng)) }
}