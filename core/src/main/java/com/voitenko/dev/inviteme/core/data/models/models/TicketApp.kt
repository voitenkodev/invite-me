package com.voitenko.dev.inviteme.core.data.models.models

data class TicketApp(
    val eventId: Long,
    val guestId: Long,
    val status: StatusApp = StatusApp.ACTIVE
) {
    enum class StatusApp { ACTIVE, USED }
}

fun TicketApp.generateTicket() = "${this.eventId}::${this.guestId}"

fun String.decodeTicket(): TicketApp? {
    val list = this.split("::")
    return if (list.size == 2) {
        val eventId = list[0].toLongOrNull()
        val guestId = list[1].toLongOrNull()
        if (eventId != null && guestId != null) TicketApp(eventId, guestId)
        else null
    } else null
}