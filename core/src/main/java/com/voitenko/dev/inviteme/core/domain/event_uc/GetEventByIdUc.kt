package com.voitenko.dev.inviteme.core.domain.event_uc

import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class GetEventByIdUc : BaseUc() {

    private val eventRepo by inject<EventRepositoryImpl>()

    fun invoke(eventId: Long) = eventRepo.getEventById(eventId).map { it.toEventApp() }
}