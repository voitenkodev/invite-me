package com.voitenko.dev.inviteme.core.domain.guest_uc

import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestWithEventsApp
import com.voitenko.dev.inviteme.core.data.models.models.common.PlaceHolderItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SpaceBoxItem
import com.voitenko.dev.inviteme.core.data.models.toEventGuestInfo
import com.voitenko.dev.inviteme.core.data.models.toGuestInfo
import com.voitenko.dev.inviteme.core.data.models.toGuestWithEventsApp
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import com.voitenko.dev.inviteme.core.utils.DEFAULT_STRING
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

@ExperimentalStdlibApi
class GetGuestWithEventsByIdUc : BaseUc() {

    private val guestRepo by inject<GuestRepositoryImpl>()

    private var cacheResult: GuestWithEventsApp? = null

    fun invoke(guestId: Long, queryEvent: String = DEFAULT_STRING) = cacheResult?.let { cache ->
        flow { emit(buildUi(cache, queryEvent)) }
    } ?: guestRepo.getGuestWithEventsByGuestId(guestId).map { result ->
        val innerResult = result.toGuestWithEventsApp()
        buildUi(innerResult, queryEvent).also { cacheResult = innerResult }
    }

    private fun buildUi(result: GuestWithEventsApp, queryEvent: String) = buildList {
        if (queryEvent == DEFAULT_STRING) add(result.guest.toGuestInfo())
        else add(SpaceBoxItem())
        if (result.events.isNotEmpty()) add(SearchItem())
        result.events.filterEventsBy(queryEvent).apply {
            forEach { guest -> add(guest.toEventGuestInfo()) }
            if (this.isEmpty() && queryEvent != DEFAULT_STRING) add(PlaceHolderItem())
        }
    }

    private fun List<EventApp>.filterEventsBy(query: String) = this.filter {
        it.title.lowercase().contains(query, true)
                || it.addressApp.name.lowercase().contains(query, true)
    }
}