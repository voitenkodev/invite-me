package com.voitenko.dev.inviteme.core.data.models.models.configs

enum class Mode { LIGHT, DARK }