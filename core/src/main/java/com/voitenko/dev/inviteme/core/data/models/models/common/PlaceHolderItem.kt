package com.voitenko.dev.inviteme.core.data.models.models.common

import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfoItemCombine

class PlaceHolderItem : EventInfoItemCombine, GuestInfoItemCombine {
    override fun getEvenInfoType() = EventInfoItemCombine.EventInfoItemType.PLACE_HOLDER
    override fun getGuestInfoType() = GuestInfoItemCombine.GuestInfoItemType.PLACE_HOLDER
}