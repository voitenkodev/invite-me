package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.datasource.local.LocalSource
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class GuestRepositoryImpl constructor(private val localSource: LocalSource) :
    GuestRepository {

    override fun getGuests(): Flow<List<Guest>> =
        localSource.tableGuest().getGuests()

    override fun getGuestByEmail(email: String): Flow<Guest?> =
        localSource.tableGuest().getGuestByEmail(email)

    override fun getGuestById(guestId: Long): Flow<Guest?> =
        localSource.tableGuest().getGuest(guestId)

    override fun getGuestsWithEvents(): Flow<List<GuestWithEvents>> =
        localSource.tableGuest().getGuestsWithEvents()

    override fun getGuestWithEventsByGuestId(guestId: Long): Flow<GuestWithEvents> =
        localSource.tableGuest().getGuestWithEventsById(guestId)

    override fun getGuestsByQuery(query: String, limit: Int): Flow<List<Guest>> =
        localSource.tableGuest().getGuestsByQuery(query.lowercase(), limit)

    override fun setGuest(guest: Guest, eventId: Long, status: Ticket.Status): Flow<Long> = flow {
        emit(localSource.tableGuest().setGuestByEventId(guest, eventId, status))
    }
}