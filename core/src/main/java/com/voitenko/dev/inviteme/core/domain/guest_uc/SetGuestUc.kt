package com.voitenko.dev.inviteme.core.domain.guest_uc

import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.toGuest
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import org.koin.core.component.inject

class SetGuestUc : BaseUc() {

    private val guestRepo by inject<GuestRepositoryImpl>()

    fun invoke(guest: GuestApp, eventId: Long) =
        guestRepo.setGuest(guest.toGuest(), eventId, Ticket.Status.ACTIVE)
}