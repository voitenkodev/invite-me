package com.voitenko.dev.inviteme.core.domain.guest_event_uc

import com.voitenko.dev.inviteme.core.data.models.models.search.SearchHeader
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchItemCombine
import com.voitenko.dev.inviteme.core.data.models.toSearchEvent
import com.voitenko.dev.inviteme.core.data.models.toSearchGuest
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import org.koin.core.component.inject

class SearchGuestAndEventsUc : BaseUc() {

    private val eventRepo by inject<EventRepositoryImpl>()
    private val guestRepo by inject<GuestRepositoryImpl>()

    fun invoke(query: String): Flow<List<SearchItemCombine>> {
        val eventsFlow = eventRepo.getEventsByQuery(query, 5)
        val guestsFlow = guestRepo.getGuestsByQuery(query, 5)
        return eventsFlow.combine(guestsFlow) { events, guests ->
            val result = ArrayList<SearchItemCombine>()
            if (events.isNotEmpty()) {
                result.add(SearchHeader(SearchHeader.SearchHeaderType.EVENT_HEADER))
                result.addAll(events.map { it.toSearchEvent() })
            }
            if (guests.isNotEmpty()) {
                result.add(SearchHeader(SearchHeader.SearchHeaderType.GUEST_HEADER))
                result.addAll(guests.map { it.toSearchGuest() })
            }
            result
        }
    }
}