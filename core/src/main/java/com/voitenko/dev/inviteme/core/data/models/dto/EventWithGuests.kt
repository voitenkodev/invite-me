package com.voitenko.dev.inviteme.core.data.models.dto

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

internal data class EventWithGuests(
    @Embedded val event: Event,
    @Relation(
        parentColumn = "eventId",
        entityColumn = "guestId",
        associateBy = Junction(Ticket::class)
    )
    val guests: List<Guest>
)
