package com.voitenko.dev.inviteme.core.data.datasource.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import com.voitenko.dev.inviteme.core.data.datasource.preferences.PreferencesKeys.COUNT_OPEN_KEY
import com.voitenko.dev.inviteme.core.data.datasource.preferences.PreferencesKeys.NIGHT_MODE_KEY
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import kotlinx.coroutines.flow.map

internal class SettingsPrefImpl(private val store: DataStore<Preferences>) : SettingsPref {

    override fun getMode() = store.data
        .map { prefs -> prefs[NIGHT_MODE_KEY] ?: true }
        .map { value -> if (value) return@map Mode.DARK else return@map Mode.LIGHT }

    override suspend fun setMode(value: Mode) {
        store.edit { prefs -> prefs[NIGHT_MODE_KEY] = value == Mode.DARK }
    }

    override fun getCountOpen() = store.data.map { prefs -> prefs[COUNT_OPEN_KEY] ?: 0 }

    override suspend fun setCountOpen(value: Int) {
        store.edit { prefs -> prefs[COUNT_OPEN_KEY] = value }
    }
}

private object PreferencesKeys {
    val NIGHT_MODE_KEY = booleanPreferencesKey("night_mode_key")
    val COUNT_OPEN_KEY = intPreferencesKey("count_open_key")
}