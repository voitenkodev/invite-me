package com.voitenko.dev.inviteme.core.data.models.dto

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.voitenko.dev.inviteme.core.utils.DEFAULT_LONG

@Entity
internal data class Guest(
    @PrimaryKey(autoGenerate = true) var guestId: Long = DEFAULT_LONG,
    val guestName: String,
    val email: String,
    val phone: String?,
    val imageLocalPath: String?
)