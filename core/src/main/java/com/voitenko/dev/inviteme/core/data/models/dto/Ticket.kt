package com.voitenko.dev.inviteme.core.data.models.dto

import androidx.room.Entity

@Entity(primaryKeys = ["eventId", "guestId"])
internal data class Ticket(
    val eventId: Long,
    val guestId: Long,
    val status: Status
) {
    enum class Status { ACTIVE, USED }
}
