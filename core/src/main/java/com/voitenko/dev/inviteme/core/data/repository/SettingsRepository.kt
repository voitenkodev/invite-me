package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import kotlinx.coroutines.flow.Flow

internal interface SettingsRepository {

    fun getMode(): Flow<Mode>
    fun setNightMode(): Flow<Unit>
    fun setDayMode(): Flow<Unit>
    fun getCountOpen(): Flow<Int>
    fun setCountOpen(i: Int): Flow<Unit>
}