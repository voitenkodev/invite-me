package com.voitenko.dev.inviteme.core.data.datasource.local.database_api

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.EventDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestWithEventDao
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket

internal const val DATA_BASE_NAME = "database-app"

@Database(
    entities = [Event::class, Guest::class, Ticket::class],
    version = 3,
    exportSchema = false
)
@TypeConverters(CalendarConverter::class, StatusConverter::class)
internal abstract class AppDataBase : RoomDatabase() {

    internal abstract fun daoEventGuest(): GuestWithEventDao
    internal abstract fun daoEvent(): EventDao
    internal abstract fun daoGuest(): GuestDao
}