package com.voitenko.dev.inviteme.core.data.datasource.remote.address_api

import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress
import io.ktor.client.*
import io.ktor.client.request.*

internal class AddressApi(private val service: HttpClient) {

    suspend fun loadStreetsByLatLng(lat: Double, lng: Double): MapBoxAddress? = service.get {
        url {
            path("geocoding/v5/mapbox.places/${lng},${lat}.json")
            parameter("limit", "1")
        }
    }
}