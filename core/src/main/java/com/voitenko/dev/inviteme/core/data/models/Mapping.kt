package com.voitenko.dev.inviteme.core.data.models

import com.voitenko.dev.inviteme.core.data.models.dto.Address
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.models.EventWithGuestsApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestWithEventsApp
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfo
import com.voitenko.dev.inviteme.core.data.models.models.event_info.GuestEventInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.EventGuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchEvent
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchGuest

internal fun EventApp.toEventInfo() = EventInfo(this)
internal fun EventApp.toEventGuestInfo() = EventGuestInfo(this)
internal fun EventApp.toEvent() =
    Event(eventId, title, description, date, addressApp.toAddress(), imageLocalPath)

// -------------------------------------------------------------------------------------------------

internal fun GuestApp.toGuestEventInfo() = GuestEventInfo(this)
internal fun GuestApp.toGuestInfo() = GuestInfo(this)
internal fun GuestApp.toGuest() = Guest(guestId, guestName, email, phone, imageLocalPath)

// -------------------------------------------------------------------------------------------------

internal fun Event.toSearchEvent() = SearchEvent(this.toEventApp())
internal fun Event.toEventApp() =
    EventApp(eventId, title, description, date, addressApp.toAddressApp(), imageLocalPath)

// -------------------------------------------------------------------------------------------------

internal fun EventWithGuests.toHomeEventWithGuest(isPassed: Boolean = false) =
    HomeEventWithGuests(isPassed, this.toEventWithGuestsApp())

internal fun EventWithGuests.toEventWithGuestsApp() =
    EventWithGuestsApp(event.toEventApp(), guests.map { it.toGuestApp() })


internal fun GuestWithEvents.toGuestWithEventsApp() =
    GuestWithEventsApp(guest.toGuestApp(), events.map { it.toEventApp() })

// -------------------------------------------------------------------------------------------------

internal fun Ticket.toTicketApp() =
    TicketApp(eventId, guestId, status.toStatusApp())

internal fun TicketApp.toTicket() =
    Ticket(eventId, guestId, status.toStatus())

internal fun TicketApp.StatusApp.toStatus() = when (this) {
    TicketApp.StatusApp.ACTIVE -> Ticket.Status.ACTIVE
    TicketApp.StatusApp.USED -> Ticket.Status.USED
}

internal fun Ticket.Status.toStatusApp() = when (this) {
    Ticket.Status.ACTIVE -> TicketApp.StatusApp.ACTIVE
    Ticket.Status.USED -> TicketApp.StatusApp.USED
}

// -------------------------------------------------------------------------------------------------

internal fun Address.toAddressApp() = AddressApp(addressName, addressLatitude, addressLongitude)
internal fun AddressApp.toAddress() = Address(name, latitude, longitude)
internal fun MapBoxAddress.toAddress(): AddressApp? {
    val address =
        this.features?.firstOrNull()?.properties?.address ?: this.features?.firstOrNull()?.placeName
    return if (address == null) null
    else AddressApp(
        longitude = this.query?.firstOrNull(),
        latitude = this.query?.lastOrNull(),
        name = address
    )
}

// -------------------------------------------------------------------------------------------------

internal fun Guest.toSearchGuest() = SearchGuest(this.toGuestApp())
internal fun Guest.toGuestApp() =
    GuestApp(this.guestId, this.guestName, this.email, this.phone, this.imageLocalPath)