package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.datasource.preferences.SettingsPref
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

internal class SettingsRepositoryImpl(private val pref: SettingsPref) : SettingsRepository {

    override fun getMode() = pref.getMode()

    override fun setNightMode() = flow { emit(pref.setMode(Mode.DARK)) }

    override fun setDayMode() = flow { emit(pref.setMode(Mode.LIGHT)) }

    override fun getCountOpen() = flow { emit(pref.getCountOpen().first()) }

    override fun setCountOpen(i: Int) = flow { emit(pref.setCountOpen(i)) }
}