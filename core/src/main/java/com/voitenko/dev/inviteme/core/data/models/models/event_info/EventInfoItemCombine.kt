package com.voitenko.dev.inviteme.core.data.models.models.event_info

interface EventInfoItemCombine {
    fun getEvenInfoType(): EventInfoItemType

    enum class EventInfoItemType { EVENT_INFO, GUEST_EVENT_INFO, SEARCH_INFO, PLACE_HOLDER, SPACE_BOX }
}
