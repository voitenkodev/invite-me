package com.voitenko.dev.inviteme.core.data.models.models.common

import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfoItemCombine

class SearchItem : EventInfoItemCombine, GuestInfoItemCombine {
    override fun getEvenInfoType() = EventInfoItemCombine.EventInfoItemType.SEARCH_INFO
    override fun getGuestInfoType() = GuestInfoItemCombine.GuestInfoItemType.SEARCH_INFO
}

interface SearchItemContract {
    fun searchBy(s: CharSequence?)
}
