package com.voitenko.dev.inviteme.core.data.models.models.search

interface SearchItemCombine {
    fun getSearchType(): SearchItemType

    enum class SearchItemType { SEARCH_HEAD, SEARCH_EVENT, SEARCH_GUEST }
}