package com.voitenko.dev.inviteme.core.data.datasource.remote

import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress

internal interface RemoteSource {
    suspend fun loadStreetsByLatLng(lat: Double, lng: Double): MapBoxAddress?
}