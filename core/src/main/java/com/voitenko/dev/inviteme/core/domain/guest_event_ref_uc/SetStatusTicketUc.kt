package com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc

import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.models.toTicket
import com.voitenko.dev.inviteme.core.data.repository.TicketRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import org.koin.core.component.inject

class SetStatusTicketUc : BaseUc() {

    private val guestEventRepository by inject<TicketRepositoryImpl>()

    fun invoke(ticket: TicketApp) =
        guestEventRepository.setTicket(ticket.toTicket())
}