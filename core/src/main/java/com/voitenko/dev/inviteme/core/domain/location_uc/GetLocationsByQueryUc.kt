package com.voitenko.dev.inviteme.core.domain.location_uc

import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.data.models.toAddress
import com.voitenko.dev.inviteme.core.data.datasource.remote.address_api.ExpectedNetworkThrowable
import com.voitenko.dev.inviteme.core.data.repository.LocationRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class GetLocationsByQueryUc : BaseUc() {

    private val locationRepository by inject<LocationRepositoryImpl>()

    fun invoke(lat: Double, lng: Double): Flow<AddressApp> = locationRepository
        .getAddressByLatLng(lat, lng).map { mapBoxAddress ->
            mapBoxAddress?.toAddress() ?: throw ExpectedNetworkThrowable(mapBoxAddress?.message)
        }
}