package com.voitenko.dev.inviteme.core.domain.settings_uc

import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import org.koin.core.component.inject

class GetNightDayModeUc : BaseUc() {

    private val settingsRepo by inject<SettingsRepositoryImpl>()

    fun invoke() = settingsRepo.getMode()
}