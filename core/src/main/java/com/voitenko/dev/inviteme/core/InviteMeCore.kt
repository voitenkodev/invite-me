package com.voitenko.dev.inviteme.core

import com.voitenko.dev.inviteme.core.di.repoModule
import com.voitenko.dev.inviteme.core.di.storageModule
import com.voitenko.dev.inviteme.core.di.ucModule
import org.koin.core.context.loadKoinModules

object InviteMeCore {
    @ExperimentalStdlibApi
    fun inject() = loadKoinModules(listOf(storageModule, repoModule, ucModule))
}