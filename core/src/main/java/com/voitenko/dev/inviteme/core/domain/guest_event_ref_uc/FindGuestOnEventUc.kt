package com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc

import com.voitenko.dev.inviteme.core.data.models.toTicketApp
import com.voitenko.dev.inviteme.core.data.repository.TicketRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class FindGuestOnEventUc : BaseUc() {

    private val guestEventRepository by inject<TicketRepositoryImpl>()

    fun invoke(eventId: Long, guestId: Long) =
        guestEventRepository.findGuestOnEvent(eventId, guestId).map { it?.toTicketApp() }
}