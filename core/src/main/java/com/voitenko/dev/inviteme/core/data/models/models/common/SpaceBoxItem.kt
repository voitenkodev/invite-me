package com.voitenko.dev.inviteme.core.data.models.models.common

import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfoItemCombine

class SpaceBoxItem : EventInfoItemCombine, GuestInfoItemCombine {
    override fun getEvenInfoType() = EventInfoItemCombine.EventInfoItemType.SPACE_BOX
    override fun getGuestInfoType() = GuestInfoItemCombine.GuestInfoItemType.SPACE_BOX
}