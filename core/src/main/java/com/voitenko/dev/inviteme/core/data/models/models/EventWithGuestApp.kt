package com.voitenko.dev.inviteme.core.data.models.models

data class EventWithGuestsApp(
    val event: EventApp,
    val guests: List<GuestApp>
)