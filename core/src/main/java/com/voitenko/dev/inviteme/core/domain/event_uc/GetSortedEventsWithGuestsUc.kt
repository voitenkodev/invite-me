package com.voitenko.dev.inviteme.core.domain.event_uc

import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
import com.voitenko.dev.inviteme.core.data.models.toHomeEventWithGuest
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import com.voitenko.dev.inviteme.core.utils.isPassed
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class GetSortedEventsWithGuestsUc : BaseUc() {

    private val eventRepo: EventRepositoryImpl by inject()

    fun invoke(): Flow<List<HomeEventWithGuests>> {
        var startOfPastFlag = false
        return eventRepo.getSortedEventsWithGuests().map { list ->
            list.map { item ->
                if (!item.event.date.isPassed()) {
                    item.toHomeEventWithGuest(false)
                } else if (!startOfPastFlag && item.event.date.isPassed()) {
                    startOfPastFlag = true
                    item.toHomeEventWithGuest(true)
                } else {
                    item.toHomeEventWithGuest(true)
                }
            }
        }
    }
}