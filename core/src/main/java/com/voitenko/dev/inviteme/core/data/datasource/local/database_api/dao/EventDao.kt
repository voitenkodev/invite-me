package com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

@Dao
internal interface EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun _setEvent(event: Event): Long

    suspend fun setEvent(event: Event): Long = withContext(Dispatchers.IO){ _setEvent(event)}

    @Query("SELECT * FROM Event WHERE eventId = :eventId")
    fun getEvent(eventId: Long): Flow<Event>

    @Transaction
    @Query("SELECT * FROM Event WHERE eventId = :eventId")
    fun getEventWithGuestsById(eventId: Long): Flow<EventWithGuests>

    @Transaction
    @Query("SELECT * FROM Event ORDER BY date ASC")
    fun getSortedEventsWithGuests(): Flow<List<EventWithGuests>>

    @Query("SELECT * FROM Event")
    fun getEvents(): Flow<List<Event>>

    @Query("SELECT * FROM Event WHERE lower(event.title) LIKE '%' || :query || '%' OR lower(event.addressName) LIKE '%' || :query || '%' LIMIT :limit")
    fun getEventsByQuery(query: String, limit: Int): Flow<List<Event>>
}