package com.voitenko.dev.inviteme.core.data.datasource.local.database_api

import android.content.Context
import androidx.room.Room

internal fun roomClient(androidContext: Context) =
    Room.databaseBuilder(androidContext, AppDataBase::class.java, DATA_BASE_NAME)
        .fallbackToDestructiveMigration().build()
