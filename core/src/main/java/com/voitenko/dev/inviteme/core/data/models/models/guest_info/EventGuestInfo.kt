package com.voitenko.dev.inviteme.core.data.models.models.guest_info

import com.voitenko.dev.inviteme.core.data.models.models.EventApp

data class EventGuestInfo(val event: EventApp) : GuestInfoItemCombine {
    override fun getGuestInfoType() = GuestInfoItemCombine.GuestInfoItemType.EVENT_GUEST_INFO
}