package com.voitenko.dev.inviteme.core.data.models.dto

internal data class Address(
    val addressName: String,
    val addressLatitude: Double? = null,
    val addressLongitude: Double? = null
)
