package com.voitenko.dev.inviteme.core.domain.event_uc

import com.voitenko.dev.inviteme.core.data.models.models.EventWithGuestsApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.models.common.PlaceHolderItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SpaceBoxItem
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.toEventInfo
import com.voitenko.dev.inviteme.core.data.models.toEventWithGuestsApp
import com.voitenko.dev.inviteme.core.data.models.toGuestEventInfo
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import com.voitenko.dev.inviteme.core.utils.DEFAULT_STRING
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

@ExperimentalStdlibApi
class GetEventWithGuestsByIdUc : BaseUc() {

    private val eventRepo by inject<EventRepositoryImpl>()

    private var cacheResult: EventWithGuestsApp? = null

    fun invoke(eventId: Long, queryGuest: String = DEFAULT_STRING, forceUpdate: Boolean = false)
            : Flow<List<EventInfoItemCombine>> {
        forceUpdate.takeIf { it }?.let { cacheResult = null }
        return cacheResult?.let { cache ->
            flow { emit(buildUi(cache, queryGuest)) }
        } ?: eventRepo.getEventWithGuestById(eventId).map { result ->
            val innerResult = result.toEventWithGuestsApp()
            buildUi(innerResult, queryGuest).also { cacheResult = innerResult }
        }
    }

    private fun buildUi(result: EventWithGuestsApp, queryGuest: String) = buildList {
        if (queryGuest == DEFAULT_STRING) add(result.event.toEventInfo())
        else add(SpaceBoxItem())
        if (result.guests.isNotEmpty()) add(SearchItem())
        result.guests.filterGuestBy(queryGuest).apply {
            forEach { guest -> add(guest.toGuestEventInfo()) }
            if (this.isEmpty() && queryGuest != DEFAULT_STRING) add(PlaceHolderItem())
        }
    }

    private fun List<GuestApp>.filterGuestBy(query: String) = this.filter {
        it.guestName.lowercase().contains(query, true)
                || it.email.lowercase().contains(query, true)
                || it.phone?.lowercase()?.contains(query, true) == true
    }
}