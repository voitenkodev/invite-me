package com.voitenko.dev.inviteme.core.data.repository

import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import kotlinx.coroutines.flow.Flow

internal interface EventRepository {

    fun getEvents(): Flow<List<Event>>
    fun getEventById(eventId: Long): Flow<Event>
    fun getEventsByQuery(query: String, limit: Int): Flow<List<Event>>
    fun getSortedEventsWithGuests(): Flow<List<EventWithGuests>>
    fun getEventWithGuestById(eventId: Long): Flow<EventWithGuests>
    fun setEvent(event: Event): Flow<Long>
}