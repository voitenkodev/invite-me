package com.voitenko.dev.inviteme.core.data.models.dto

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

internal data class GuestWithEvents(
    @Embedded val guest: Guest,
    @Relation(
        parentColumn = "guestId",
        entityColumn = "eventId",
        associateBy = Junction(Ticket::class)
    )
    val events: List<Event>
)
