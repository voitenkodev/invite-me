package com.voitenko.dev.inviteme.core.data.models.models.home

import com.voitenko.dev.inviteme.core.data.models.models.EventWithGuestsApp

data class HomeEventWithGuests(
    val isPassed: Boolean = false,
    val eventWithGuests: EventWithGuestsApp
)