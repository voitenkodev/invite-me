package com.voitenko.dev.inviteme.core.data.models.models.event_info

import com.voitenko.dev.inviteme.core.data.models.models.EventApp

data class EventInfo(val event: EventApp) : EventInfoItemCombine {
    override fun getEvenInfoType() = EventInfoItemCombine.EventInfoItemType.EVENT_INFO
}