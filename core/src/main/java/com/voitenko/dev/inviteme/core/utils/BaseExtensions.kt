package com.voitenko.dev.inviteme.core.utils

import android.util.Log
import java.util.*

internal const val DEFAULT_LONG = -1L
internal const val DEFAULT_INT = -1
internal const val DEFAULT_BOOL = false
internal const val DEFAULT_FLOAT = -1F
internal const val DEFAULT_STRING = ""
internal val DEFAULT_CALENDAR = GregorianCalendar(2000, 0, 0)

// show log "showLog"
internal fun showLog(text: String?) = Log.d("showLog", text ?: "null")