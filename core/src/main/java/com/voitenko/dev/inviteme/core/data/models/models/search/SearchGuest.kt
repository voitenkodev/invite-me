package com.voitenko.dev.inviteme.core.data.models.models.search

import com.voitenko.dev.inviteme.core.data.models.models.GuestApp

data class SearchGuest(val guest: GuestApp) : SearchItemCombine {
    override fun getSearchType() = SearchItemCombine.SearchItemType.SEARCH_GUEST
}