package com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

@Dao
internal interface GuestWithEventDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun _insert(join: Ticket): Long?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(join: Ticket): Long? = withContext(Dispatchers.IO) { _insert(join) }

    @Query("UPDATE Ticket SET status = :status WHERE eventId = :eventId AND guestId = :guestId")
    fun _updateTicketStatus(eventId: Long, guestId: Long, status: Ticket.Status)

    @Query("UPDATE Ticket SET status = :status WHERE eventId = :eventId AND guestId = :guestId")
    suspend fun updateTicketStatus(eventId: Long, guestId: Long, status: Ticket.Status) =
        withContext(Dispatchers.IO) { _updateTicketStatus(eventId, guestId, status) }

    @Query("SELECT * FROM Ticket WHERE eventId = :eventId AND guestId = :guestId")
    fun getTicket(eventId: Long, guestId: Long): Flow<Ticket?>
}