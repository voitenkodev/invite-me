package com.voitenko.dev.inviteme.core.data.datasource.preferences

import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import kotlinx.coroutines.flow.Flow

internal interface SettingsPref {

    suspend fun setMode(value: Mode)
    fun getMode(): Flow<Mode>
    suspend fun setCountOpen(value: Int)
    fun getCountOpen(): Flow<Int>
}