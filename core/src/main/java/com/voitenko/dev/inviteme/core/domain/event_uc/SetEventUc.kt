package com.voitenko.dev.inviteme.core.domain.event_uc

import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.toEvent
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.utils.BaseUc
import org.koin.core.component.inject

class SetEventUc : BaseUc() {

    private val eventRepo by inject<EventRepositoryImpl>()

    fun invoke(event: EventApp) = eventRepo.setEvent(event.toEvent())
}