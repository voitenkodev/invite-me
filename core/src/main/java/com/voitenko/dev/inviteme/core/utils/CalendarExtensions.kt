package com.voitenko.dev.inviteme.core.utils

import java.text.DateFormatSymbols
import java.util.*

internal fun Calendar.toUiString(): String {
    return "${this.get(Calendar.DAY_OF_MONTH)} ${getMonthForInt(this.get(Calendar.MONTH))} (${
        String.format(
            "%02d : %02d", this.get(Calendar.HOUR_OF_DAY), this.get(Calendar.MINUTE)
        )
    })"
}

internal fun Calendar.isPassed() = (currentCalendar().timeInMillis > this.timeInMillis)

internal fun Date.toCalendar(): Calendar = currentCalendar().apply { time = this@toCalendar }

private fun currentCalendar() = Calendar.getInstance()
private fun getMonthForInt(num: Int): String {
    var month = DEFAULT_STRING
    val dfs = DateFormatSymbols()
    val months: Array<String> = dfs.months
    if (num in 0..11) {
        month = months[num]
    }
    return month
}