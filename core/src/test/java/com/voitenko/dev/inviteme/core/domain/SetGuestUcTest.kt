package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.guest_uc.SetGuestUc
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class SetGuestUcTest : BaseTest() {

    private val uc by inject<SetGuestUc>()

    @Before
    fun setUp() {
        declareMock<GuestRepositoryImpl> {
            every { setGuest(GUEST_1, 1, true) } returns flowOf(GUEST_1.guestId)
        }
    }

    @Test
    fun `success set guest invoke`() = runBlocking {
        val exp = GUEST_1.guestId

        val value = GUEST_1.toGuestApp()

        val act: Long = uc.invoke(value, 1).first()

        Assert.assertEquals(exp, act)
    }
}