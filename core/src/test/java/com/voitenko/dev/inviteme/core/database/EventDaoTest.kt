package com.voitenko.dev.inviteme.core.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.AppDataBase
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.EventDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestWithEventDao
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@Config(manifest = Config.NONE)
class EventDaoTest : BaseTest() {

    private lateinit var db: AppDataBase
    private lateinit var dao: EventDao
    private lateinit var daoGuest: GuestDao
    private lateinit var daoGuestWithEvent: GuestWithEventDao

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java).build()
        dao = db.daoEvent()
        daoGuest = db.daoGuest()
        daoGuestWithEvent = db.daoEventGuest()
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        db.close()
    }

    @Test
    fun `set and get event success`() = runBlocking {
        val exp = EVENT_1

        dao.setEvent(exp)

        val act = dao.getEvent(1).first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `set and get event fail`() = runBlocking {
        val exp = null

        val act = dao.getEvent(2).first()

        Assert.assertEquals(act, exp)
    }

    @Test
    fun `set and get guest with events by id success`() = runBlocking {
        val exp = EVENT_WITH_GUESTS

        dao.setEvent(exp.event)
        exp.guests.forEach {
            daoGuest.setGuest(it)
            daoGuestWithEvent.insert(Ticket(exp.event.eventId, it.guestId, Ticket.Status.USED))
        }

        val act = dao.getEventWithGuestsById(exp.event.eventId).first()

        Assert.assertEquals(exp.guests.size, act.guests.size)
        Assert.assertEquals(act, exp)
    }

    @Test
    fun `set and get guest with events by id fail`() = runBlocking {
        val exp = null

        val act = dao.getEventWithGuestsById(1).first()

        Assert.assertEquals(act, exp)
    }

    @Test
    fun `set and get valid list of EventsGuestsRef`() = runBlocking {
        val exp = LIST_EVENTS_WITH_LISTS_OF_GUESTS
        exp.forEach { geRef ->
            dao.setEvent(geRef.event)
            geRef.guests.forEach {
                daoGuest.setGuest(it)
                daoGuestWithEvent.insert(Ticket(geRef.event.eventId, it.guestId, Ticket.Status.USED))
            }
        }

        val act = dao.getSortedEventsWithGuests().first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }

    @Test
    fun `set and get empty list of EventsGuestsRef`() = runBlocking {
        val exp = emptyList<EventWithGuests>()

        val act = dao.getSortedEventsWithGuests().first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }

    @Test
    fun `set and get valid list of Events success`() = runBlocking {
        val exp = EVENTS

        exp.forEach { event -> dao.setEvent(event) }

        val act = dao.getEvents().first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }

    @Test
    fun `set and get valid list of Events fail`() = runBlocking {
        val exp = emptyList<Event>()

        val act = dao.getEvents().first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }

    @Test
    fun `set and get valid list of Events by query success`() = runBlocking {
        val exp = EVENTS_BY_11

        EVENTS.forEach { event -> dao.setEvent(event) }

        val act = dao.getEventsByQuery("event_11", 5).first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(exp[index], guestWithEvents)
        }
    }

    @Test
    fun `set and get valid list of Events by query fail`() = runBlocking {
        val exp = emptyList<Event>()

        val act = dao.getEventsByQuery("error", 5).first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }
}