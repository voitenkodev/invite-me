package com.voitenko.dev.inviteme.core.mapping

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.models.dto.Address
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress
import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.models.EventWithGuestsApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestWithEventsApp
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfo
import com.voitenko.dev.inviteme.core.data.models.models.event_info.GuestEventInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.EventGuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchEvent
import com.voitenko.dev.inviteme.core.data.models.toAddress
import com.voitenko.dev.inviteme.core.data.models.toAddressApp
import com.voitenko.dev.inviteme.core.data.models.toEvent
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.models.toEventGuestInfo
import com.voitenko.dev.inviteme.core.data.models.toEventInfo
import com.voitenko.dev.inviteme.core.data.models.toEventWithGuestsApp
import com.voitenko.dev.inviteme.core.data.models.toGuest
import com.voitenko.dev.inviteme.core.data.models.toGuestEventInfo
import com.voitenko.dev.inviteme.core.data.models.toGuestInfo
import com.voitenko.dev.inviteme.core.data.models.toGuestWithEventsApp
import com.voitenko.dev.inviteme.core.data.models.toTicket
import com.voitenko.dev.inviteme.core.data.models.toTicketApp
import com.voitenko.dev.inviteme.core.data.models.toHomeEventWithGuest
import com.voitenko.dev.inviteme.core.data.models.toSearchEvent
import com.voitenko.dev.inviteme.core.utils.DEFAULT_CALENDAR
import org.junit.Assert
import org.junit.Test

class MappingTest : BaseTest() {

    @Test
    fun `test mapping toEventInfo`() {
        val value = EventApp(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            AddressApp.EMPTY_ADDRESS,
            "imageUrl"
        )

        val exp = EventInfo(
            EventApp(
                1,
                "title",
                "desc",
                DEFAULT_CALENDAR,
                AddressApp.EMPTY_ADDRESS,
                "imageUrl"
            )
        )

        val act = value.toEventInfo()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toEventGuestInfo`() {
        val value = EventApp(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            AddressApp.EMPTY_ADDRESS,
            "imageUrl"
        )

        val exp = EventGuestInfo(
            EventApp(
                1,
                "title",
                "desc",
                DEFAULT_CALENDAR,
                AddressApp.EMPTY_ADDRESS,
                "imageUrl"
            )
        )

        val act = value.toEventGuestInfo()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toEvent`() {
        val value = EventApp(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            AddressApp.EMPTY_ADDRESS.copy(name = "testAddress"),
            "imageUrl"
        )

        val exp = Event(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            Address(addressName = "testAddress"),
            "imageUrl"
        )

        val act = value.toEvent()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toGuestEventInfo`() {
        val value = GuestApp(1, "name", "email@gm.c", "phone", "imageUri")

        val exp = GuestEventInfo(
            GuestApp(1, "name", "email@gm.c", "phone", "imageUri")
        )

        val act = value.toGuestEventInfo()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toGuestInfo`() {
        val value = GuestApp(1, "name", "email@gm.c", "phone", "imageUri")

        val exp = GuestInfo(
            GuestApp(1, "name", "email@gm.c", "phone", "imageUri")
        )

        val act = value.toGuestInfo()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toGuest`() {
        val value = GuestApp(1, "name", "email@gm.c", "phone", "imageUri")

        val exp = Guest(1, "name", "email@gm.c", "phone", "imageUri")

        val act = value.toGuest()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toSearchEvent`() {
        val value = Event(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            Address(addressName = "testAddress"),
            "imageUrl"
        )

        val exp = SearchEvent(
            EventApp(
                1,
                "title",
                "desc",
                DEFAULT_CALENDAR,
                AddressApp.EMPTY_ADDRESS.copy(name = "testAddress"),
                "imageUrl"
            )
        )

        val act = value.toSearchEvent()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toEventApp`() {
        val value = Event(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            Address(addressName = "testAddress"),
            "imageUrl"
        )

        val exp = EventApp(
            1,
            "title",
            "desc",
            DEFAULT_CALENDAR,
            AddressApp.EMPTY_ADDRESS.copy(name = "testAddress"),
            "imageUrl"
        )

        val act = value.toEventApp()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toHomeEventWithGuest`() {
        val value = EventWithGuests(
            Event(
                1,
                "title",
                "desc",
                DEFAULT_CALENDAR,
                Address(addressName = "testAddress"),
                "imageUrl"
            ),
            listOf(Guest(1, "name", "email@gm.c", "phone", "imageUri"))
        )

        val exp = HomeEventWithGuests(
            false,
            EventWithGuestsApp(
                EventApp(
                    1,
                    "title",
                    "desc",
                    DEFAULT_CALENDAR,
                    AddressApp.EMPTY_ADDRESS.copy(name = "testAddress"),
                    "imageUrl"
                ),
                listOf(GuestApp(1, "name", "email@gm.c", "phone", "imageUri"))
            )
        )

        val act = value.toHomeEventWithGuest(false)

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toEventWithGuestsApp`() {
        val value = EventWithGuests(
            Event(
                1,
                "title",
                "desc",
                DEFAULT_CALENDAR,
                Address(addressName = "testAddress"),
                "imageUrl"
            ),
            listOf(Guest(1, "name", "email@gm.c", "phone", "imageUri"))
        )

        val exp = EventWithGuestsApp(
            EventApp(
                1,
                "title",
                "desc",
                DEFAULT_CALENDAR,
                AddressApp.EMPTY_ADDRESS.copy(name = "testAddress"),
                "imageUrl"
            ),
            listOf(GuestApp(1, "name", "email@gm.c", "phone", "imageUri"))
        )

        val act = value.toEventWithGuestsApp()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toGuestWithEventsApp`() {
        val value = GuestWithEvents(
            Guest(1, "name", "email@gm.c", "phone", "imageUri"),
            listOf(
                Event(
                    1,
                    "title",
                    "desc",
                    DEFAULT_CALENDAR,
                    Address(addressName = "testAddress"),
                    "imageUrl"
                )
            )
        )

        val exp = GuestWithEventsApp(
            GuestApp(1, "name", "email@gm.c", "phone", "imageUri"),
            listOf(
                EventApp(
                    1,
                    "title",
                    "desc",
                    DEFAULT_CALENDAR,
                    AddressApp.EMPTY_ADDRESS.copy(name = "testAddress"),
                    "imageUrl"
                )
            )
        )

        val act = value.toGuestWithEventsApp()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toGuestsEventsRefApp`() {
        val value = Ticket(4, 3, true)

        val exp = TicketApp(4, 3, true)

        val act = value.toTicketApp()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toGuestsEventsRef`() {
        val value = TicketApp(4, 3, true)

        val exp = Ticket(4, 3, true)

        val act = value.toTicket()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toAddressApp from Address`() {
        val value = Address("address xx", -12332.42, 240113.0)

        val exp = AddressApp("address xx", -12332.42, 240113.0)

        val act = value.toAddressApp()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toAddress from AddressApp`() {
        val value = AddressApp("address xx", -12332.42, 240113.0)

        val exp = Address("address xx", -12332.42, 240113.0)

        val act = value.toAddress()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `test mapping toAddress from MopBoxAddress`() {
        val value = MapBoxAddress(
            query = listOf(
                12331231.0,
                32423.3131
            ),
            features = listOf(
                MapBoxAddress.Feature(
                    properties = MapBoxAddress.Feature.Properties(
                        address = "test address xx"
                    ), placeName = "test placeName"
                ),
            )
        )

        val exp = AddressApp("test address xx", 32423.3131, 12331231.0)

        val act = value.toAddress()

        Assert.assertEquals(exp, act)
    }
}