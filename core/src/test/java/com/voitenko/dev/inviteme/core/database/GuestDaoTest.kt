package com.voitenko.dev.inviteme.core.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.AppDataBase
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.EventDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestDao
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestWithEventDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.IOException

@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@Config(manifest = Config.NONE)
class GuestDaoTest : BaseTest() {

    private lateinit var db: AppDataBase
    private lateinit var dao: GuestDao
    private lateinit var daoEvent: EventDao
    private lateinit var daoGuestWithEvent: GuestWithEventDao

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java).build()
        dao = db.daoGuest()
        daoEvent = db.daoEvent()
        daoGuestWithEvent = db.daoEventGuest()
    }

    @After
    @Throws(IOException::class)
    fun cleanup() {
        db.close()
    }

    @Test
    fun `set and get guest success`() = runBlocking {
        val exp = GUEST_1

        dao.setGuest(exp)

        val act = dao.getGuest(1).first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `set and get guest fail`() = runBlocking {
        val exp = null

        val act = dao.getGuest(2).first()

        Assert.assertEquals(act, exp)
    }

    @Test
    fun `get guest by email success`() = runBlocking {
        val exp = GUEST_2

        dao.setGuest(exp)

        val act = dao.getGuestByEmail("2@gmail.com").first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `get guest by email fail`() = runBlocking {
        val exp = null

        val act = dao.getGuestByEmail("test_fail@gmail.com").first()

        Assert.assertEquals(act, exp)
    }

    @Test
    fun `set and get valid GuestEventRef`() = runBlocking {
        val exp = GUEST_WITH_EVENTS

        dao.setGuest(exp.guest)
        exp.events.forEach {
            daoEvent.setEvent(it)
            daoGuestWithEvent.insert(Ticket(it.eventId, exp.guest.guestId, false))
        }

        val act = dao.getGuestWithEventsById(1).first()

        Assert.assertEquals(act, exp)
    }

    @Test
    fun `set and get valid GuestEventRef fail`() = runBlocking {
        val exp = null

        val act = dao.getGuestWithEventsById(1).first()

        Assert.assertEquals(act, exp)
    }

    @Test
    fun `set and get valid list of GuestEventRef`() = runBlocking {
        val exp = LIST_GUESTS_WITH_LISTS_OF_EVENTS

        exp.forEach { geRef ->
            dao.setGuest(geRef.guest)
            geRef.events.forEach {
                daoEvent.setEvent(it)
                daoGuestWithEvent.insert(Ticket(it.eventId, geRef.guest.guestId, false))
            }
        }

        val act = dao.getGuestsWithEvents().first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }

    @Test
    fun `set and get valid list of GuestEventRef fail`() = runBlocking {
        val exp = emptyList<GuestWithEvents>()

        val act = dao.getGuestsWithEvents().first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `set and get valid list of Guests`() = runBlocking {
        val exp = GUESTS
        exp.forEach { dao.setGuest(it) }

        val act = dao.getGuests().first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }

    @Test
    fun `set and get valid list of Guests fail`() = runBlocking {
        val exp = emptyList<Guest>()

        val act = dao.getGuests().first()

        Assert.assertEquals(exp.size, act.size)
        Assert.assertEquals(exp, act)
    }

    @Test
    fun `set and get valid list of Events by query success`() = runBlocking {
        val exp = GUESTS_BY_11

        GUESTS.forEach { guest -> dao.setGuest(guest) }

        val act = dao.getGuestsByQuery("user_11", 5).first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(exp[index], guestWithEvents)
        }
    }

    @Test
    fun `set and get valid list of Events by query fail`() = runBlocking {
        val exp = emptyList<Guest>()

        val act = dao.getGuestsByQuery("error", 5).first()

        Assert.assertEquals(exp.size, act.size)
        act.forEachIndexed { index, guestWithEvents ->
            Assert.assertEquals(guestWithEvents, exp[index])
        }
    }
}