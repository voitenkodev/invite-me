package com.voitenko.dev.inviteme.core

import com.voitenko.dev.inviteme.core.database.EventDaoTest
import com.voitenko.dev.inviteme.core.database.GuestDaoTest
import com.voitenko.dev.inviteme.core.database.GuestWithEventDaoTest
import com.voitenko.dev.inviteme.core.domain.FindGuestOnEventUcTest
import com.voitenko.dev.inviteme.core.domain.GetEventByIdUcTest
import com.voitenko.dev.inviteme.core.domain.GetEventWithGuestsByIdUcTest
import com.voitenko.dev.inviteme.core.domain.GetGuestByEmailUcTest
import com.voitenko.dev.inviteme.core.domain.GetGuestByIdUcTest
import com.voitenko.dev.inviteme.core.domain.GetGuestWithEventsByIdUcTest
import com.voitenko.dev.inviteme.core.domain.GetLocationsByQueryUcTest
import com.voitenko.dev.inviteme.core.domain.GetNightDayModeUcTest
import com.voitenko.dev.inviteme.core.domain.GetSortedEventsWithGuestsUcTest
import com.voitenko.dev.inviteme.core.domain.SearchGuestAndEventsUcTest
import com.voitenko.dev.inviteme.core.domain.SetDayModeUcTest
import com.voitenko.dev.inviteme.core.domain.SetGuestUcTest
import com.voitenko.dev.inviteme.core.domain.SetNightModeUcTest
import com.voitenko.dev.inviteme.core.domain.SetStatusTicketUcTest
import com.voitenko.dev.inviteme.core.domain.TryToUpdateCountOfOpenUcTest
import com.voitenko.dev.inviteme.core.mapping.MappingTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config

@FlowPreview
@ExperimentalCoroutinesApi
@ExperimentalStdlibApi
@RunWith(Suite::class)
@Suite.SuiteClasses(
    GetGuestByIdUcTest::class,
    GetEventByIdUcTest::class,
    FindGuestOnEventUcTest::class,
    GetEventWithGuestsByIdUcTest::class,
    GetGuestByEmailUcTest::class,
    GetGuestWithEventsByIdUcTest::class,
    GetLocationsByQueryUcTest::class,
    GetNightDayModeUcTest::class,
    GetSortedEventsWithGuestsUcTest::class,
    SearchGuestAndEventsUcTest::class,
    SetDayModeUcTest::class,
    SetGuestUcTest::class,
    SetNightModeUcTest::class,
    SetStatusTicketUcTest::class,
    TryToUpdateCountOfOpenUcTest::class,

    EventDaoTest::class,
    GuestDaoTest::class,
    GuestWithEventDaoTest::class,

    MappingTest::class,
)
@Config(manifest = Config.NONE)
class UnitTestRunner
