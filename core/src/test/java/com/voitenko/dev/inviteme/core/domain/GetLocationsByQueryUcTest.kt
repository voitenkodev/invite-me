package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.location_uc.GetLocationsByQueryUc
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.data.datasource.remote.address_api.ExpectedNetworkThrowable
import com.voitenko.dev.inviteme.core.data.repository.LocationRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class GetLocationsByQueryUcTest : BaseTest() {

    private val uc by inject<GetLocationsByQueryUc>()

    @Before
    fun setUp() {
        declareMock<LocationRepositoryImpl> {
            every { getAddressByLatLng(.1, .1) } returns flowOf(responseLocations)
            every { getAddressByLatLng(.0, .0) } throws ExpectedNetworkThrowable("error")
        }
    }

    @Test
    fun `success get location by query invoke`() = runBlocking {
        val exp = AddressApp(
            name = responseLocations.features?.firstOrNull()?.placeName.toString(),
            latitude = responseLocations.query?.firstOrNull(),
            longitude = responseLocations.query?.lastOrNull()
        )

        val act = uc.invoke(.1, .1).first()

        Assert.assertEquals(exp, act)
    }

    @Test(expected = ExpectedNetworkThrowable::class)
    fun `get location by query invoke by error`() = runBlocking {
        val exp = ExpectedNetworkThrowable("error")

        val act = uc.invoke(.0, .0).first()

        Assert.assertEquals(exp, act)
    }
}