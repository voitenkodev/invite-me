package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestByEmailUc
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class GetGuestByEmailUcTest : BaseTest() {

    private val uc by inject<GetGuestByEmailUc>()

    @Before
    fun setUp() {
        declareMock<GuestRepositoryImpl> {
            every { getGuestByEmail("1@gmail.com") } returns flowOf(GUEST_1)
            every { getGuestByEmail("error") } returns flowOf(null)
        }
    }

    @Test
    fun `success get guest by event invoke`() = runBlocking {
        val exp = GUEST_1.toGuestApp()

        val act: GuestApp? = uc.invoke("1@gmail.com").first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `error get guest by event invoke`() = runBlocking {
        val exp = null

        val act: GuestApp? = uc.invoke("error").first()

        Assert.assertEquals(exp, act)
    }
}