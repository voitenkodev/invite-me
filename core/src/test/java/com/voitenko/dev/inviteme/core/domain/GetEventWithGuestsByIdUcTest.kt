package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.event_uc.GetEventWithGuestsByIdUc
import com.voitenko.dev.inviteme.core.data.models.models.common.PlaceHolderItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SpaceBoxItem
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfo
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.models.event_info.GuestEventInfo
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

@ExperimentalStdlibApi
class GetEventWithGuestsByIdUcTest : BaseTest() {

    private val uc by inject<GetEventWithGuestsByIdUc>()

    @Before
    fun setUp() {
        declareMock<EventRepositoryImpl> {
            every { getEventWithGuestById(1) } returns flowOf(EVENT_WITH_GUESTS)
        }
    }

    @Test
    fun `success invoke with empty query`() = runBlocking {
        val exp = listOf(
            EventInfo(EVENT_1.toEventApp()),
            SearchItem(),
            GuestEventInfo(GUEST_1.toGuestApp()),
            GuestEventInfo(GUEST_2.toGuestApp()),
            GuestEventInfo(GUEST_3.toGuestApp()),
            GuestEventInfo(GUEST_4.toGuestApp()),
            GuestEventInfo(GUEST_5.toGuestApp()),
            GuestEventInfo(GUEST_11.toGuestApp()),
            GuestEventInfo(GUEST_111.toGuestApp()),
        )

        val act: List<EventInfoItemCombine> = uc.invoke(1).first()

        assertThat(exp, act)
    }

    @Test
    fun `success invoke by query`() = runBlocking {
        val exp = listOf(
            SpaceBoxItem(),
            SearchItem(),
            GuestEventInfo(GUEST_11.toGuestApp()),
            GuestEventInfo(GUEST_111.toGuestApp()),
        )

        val act: List<EventInfoItemCombine> = uc.invoke(1, "11").first()

        assertThat(exp, act)
    }

    @Test
    fun `success invoke by error query`() = runBlocking {
        val exp = listOf(
            SpaceBoxItem(),
            SearchItem(),
            PlaceHolderItem()
        )

        val act: List<EventInfoItemCombine> = uc.invoke(1, "error").first()

        assertThat(exp, act)
    }

    @Test(expected = NoSuchElementException::class)
    fun `error invoke with empty query`() = runBlocking {
        val exp = NoSuchElementException::class

        val act: List<EventInfoItemCombine> = uc.invoke(2).first()

        Assert.assertEquals(exp, act)
    }

    private fun assertThat(exp: List<EventInfoItemCombine>, act: List<EventInfoItemCombine>?) {
        Assert.assertEquals(exp.size, act?.size)
        act?.forEachIndexed { index, value ->
            val expectedItem = exp[index]
            Assert.assertThat(value, instanceOf(exp[index]::class.java))
            when {
                value is EventInfo && expectedItem is EventInfo ->
                    Assert.assertEquals(value.event.eventId, expectedItem.event.eventId)
                value is GuestEventInfo && expectedItem is GuestEventInfo ->
                    Assert.assertEquals(value.guest.guestId, expectedItem.guest.guestId)
            }
        }
    }
}

