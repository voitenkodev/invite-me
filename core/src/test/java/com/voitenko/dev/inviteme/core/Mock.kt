package com.voitenko.dev.inviteme.core

import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.core.data.models.dto.Address
import com.voitenko.dev.inviteme.core.data.models.dto.Event
import com.voitenko.dev.inviteme.core.data.models.dto.EventWithGuests
import com.voitenko.dev.inviteme.core.data.models.dto.Guest
import com.voitenko.dev.inviteme.core.data.models.dto.GuestWithEvents
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.data.models.dto.MapBoxAddress
import com.voitenko.dev.inviteme.core.utils.DEFAULT_CALENDAR
import java.util.*

open class Mock {

    companion object {
        private const val stub = "stub"
        private val defaultAddress = Address(addressName = stub)
        private val defaultEvent = Event(-1, stub, stub, DEFAULT_CALENDAR, defaultAddress, null)
        private val defaultGuest = Guest(-1, stub, stub, stub, stub)

        fun dateWithYear(y: Int): Calendar = Calendar.getInstance().apply { set(Calendar.YEAR, y) }

        internal val EVENT_REF_1 = Ticket(1, 1, Ticket.Status.ACTIVE)
        internal val EVENT_REF_3 = Ticket(3, 3, Ticket.Status.ACTIVE)
        internal val EVENT_REF_2 = Ticket(2, 2, Ticket.Status.USED)

        internal val EVENT_1 = defaultEvent.copy(eventId = 1, title = "event_1")
        internal val EVENT_2 = defaultEvent.copy(eventId = 2, title = "event_2")
        internal val EVENT_3 = defaultEvent.copy(eventId = 3, title = "event_3")
        internal val EVENT_4 = defaultEvent.copy(eventId = 4, title = "event_3")
        internal val EVENT_5 = defaultEvent.copy(eventId = 5, title = "event_5")
        internal val EVENT_11 = defaultEvent.copy(eventId = 11, title = "event_11")
        internal val EVENT_111 = defaultEvent.copy(eventId = 111, title = "event_111")

        internal val GUEST_1 =
            defaultGuest.copy(guestId = 1, guestName = "user_1", email = "1@gmail.com")
        internal val GUEST_2 =
            defaultGuest.copy(guestId = 2, guestName = "user_2", email = "2@gmail.com")
        internal val GUEST_3 =
            defaultGuest.copy(guestId = 3, guestName = "user_3", email = "3@gmail.com")
        internal val GUEST_4 =
            defaultGuest.copy(guestId = 4, guestName = "user_4", email = "4@gmail.com")
        internal val GUEST_5 =
            defaultGuest.copy(guestId = 5, guestName = "user_5", email = "5@gmail.com")
        internal val GUEST_11 =
            defaultGuest.copy(guestId = 11, guestName = "user_11", email = "11@gmail.com")
        internal val GUEST_111 =
            defaultGuest.copy(guestId = 111, guestName = "user_111", email = "111@gmail.com")

        internal val GUESTS =
            listOf(GUEST_1, GUEST_2, GUEST_3, GUEST_4, GUEST_5, GUEST_11, GUEST_111)
        internal val EVENTS =
            listOf(EVENT_1, EVENT_2, EVENT_3, EVENT_4, EVENT_5, EVENT_11, EVENT_111)

        internal val EVENTS_BY_11 = listOf(EVENT_11, EVENT_111)
        internal val GUESTS_BY_11 = listOf(GUEST_11, GUEST_111)

        internal val EVENTS_TAKE_FIVE = EVENTS.take(5)
        internal val GUESTS_TAKE_FIVE = GUESTS.take(5)

        internal val EVENT_WITH_GUESTS = EventWithGuests(EVENT_1, GUESTS)
        internal val GUEST_WITH_EVENTS = GuestWithEvents(GUEST_1, EVENTS)

        private val GUEST_WITH_THREE_EVENTS =
            GuestWithEvents(GUEST_1, listOf(EVENT_1, EVENT_2, EVENT_3))
        private val GUEST_WITH_TWO_EVENTS = GuestWithEvents(GUEST_2, listOf(EVENT_1, EVENT_2))
        private val GUEST_WITH_ONE_EVENT = GuestWithEvents(GUEST_3, listOf(EVENT_1))

        private val EVENT_WITH_THREE_GUESTS =
            EventWithGuests(EVENT_1, listOf(GUEST_1, GUEST_2, GUEST_3))
        private val EVENT_WITH_TWO_GUESTS = EventWithGuests(EVENT_2, listOf(GUEST_1, GUEST_2))
        private val EVENT_WITH_ONE_GUESTS = EventWithGuests(EVENT_3, listOf(GUEST_1))

        internal val LIST_GUESTS_WITH_LISTS_OF_EVENTS =
            listOf(GUEST_WITH_THREE_EVENTS, GUEST_WITH_TWO_EVENTS, GUEST_WITH_ONE_EVENT)
        internal val LIST_EVENTS_WITH_LISTS_OF_GUESTS =
            listOf(EVENT_WITH_THREE_GUESTS, EVENT_WITH_TWO_GUESTS, EVENT_WITH_ONE_GUESTS)

        internal val responseEventsWithGuests = listOf(
            EventWithGuests(
                defaultEvent.copy(eventId = 1, date = dateWithYear(4020)),
                listOf(GUEST_4, GUEST_5)
            ), EventWithGuests(
                defaultEvent.copy(eventId = 2, date = dateWithYear(3020)),
                listOf(GUEST_1, GUEST_2, GUEST_3)
            ), EventWithGuests(EVENT_1, GUESTS)
        )

        internal val responseLocations = MapBoxAddress(
            features = listOf(MapBoxAddress.Feature(placeName = "test place name")),
            query = listOf(.1, .1)
        )

        internal val responseIsNightMode = Mode.LIGHT
    }
}