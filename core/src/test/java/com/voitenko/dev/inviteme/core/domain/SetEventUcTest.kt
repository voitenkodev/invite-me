package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.event_uc.SetEventUc
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class SetEventUcTest : BaseTest() {

    private val uc by inject<SetEventUc>()

    @Before
    fun setUp() {
        declareMock<EventRepositoryImpl> {
            every { setEvent(EVENT_1) } returns flow { EVENT_1.eventId }
        }
    }

    @Test
    fun `success set event invoke`() = runBlocking {
        val exp = 1

        val value = EVENT_1.toEventApp()

        val act: Long = uc.invoke(value).first()

        Assert.assertEquals(exp, act)
    }
}