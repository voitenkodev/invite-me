package com.voitenko.dev.inviteme.core.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.AppDataBase
import com.voitenko.dev.inviteme.core.data.datasource.local.database_api.dao.GuestWithEventDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.IOException

@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@Config(manifest = Config.NONE)
class GuestWithEventDaoTest : BaseTest() {

    private lateinit var db: AppDataBase
    private lateinit var dao: GuestWithEventDao

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(context, AppDataBase::class.java).build()
        dao = db.daoEventGuest()
    }

    @After
    @Throws(IOException::class)
    fun cleanup() {
        db.close()
    }

    @Test
    fun `get GuestEventRef fail`() = runBlocking {
        val exp = null

        dao.insert(EVENT_REF_1)
        dao.insert(EVENT_REF_2)
        dao.insert(EVENT_REF_3)

        val act = dao.getTicket(99, 99).first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `set and get valid GuestEventRef`() = runBlocking {
        val exp = EVENT_REF_2

        dao.insert(exp)

        val act = dao.getTicket(2, 2).first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `valid update status for GuestEventRef to false`() = runBlocking {
        val exp = EVENT_REF_2

        dao.insert(exp)
        dao.getTicket(EVENT_REF_2.eventId, EVENT_REF_2.guestId).first()
        dao.updateTicketStatus(EVENT_REF_2.eventId, EVENT_REF_2.guestId, false)

        val act = dao.getTicket(2, 2).first()

        Assert.assertEquals(act, exp.copy(status = false))
    }

    @Test
    fun `valid update status for GuestEventRef to true`() = runBlocking {
        val exp = EVENT_REF_3

        dao.insert(exp)
        dao.getTicket(3, 3).first()
        dao.updateTicketStatus(3, 3, true)

        val act = dao.getTicket(3, 3).first()

        Assert.assertEquals(act, exp.copy(status = true))
    }
}