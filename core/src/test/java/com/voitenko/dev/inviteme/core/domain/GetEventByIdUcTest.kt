package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.event_uc.GetEventByIdUc
import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class GetEventByIdUcTest : BaseTest() {

    private val uc by inject<GetEventByIdUc>()

    @Before
    fun setUp() {
        declareMock<EventRepositoryImpl> {
            every { getEventById(1) } returns flowOf(EVENT_1)
        }
    }

    @Test
    fun `success get event invoke`() = runBlocking {
        val exp = EVENT_1.toEventApp()

        val act: EventApp = uc.invoke(1).first()

        Assert.assertEquals(exp, act)
    }

    @Test(expected = NoSuchElementException::class)
    fun `error get event invoke`() = runBlocking {
        val exp = NoSuchElementException::class

        val act: EventApp = uc.invoke(2).first()

        Assert.assertEquals(exp, act)
    }
}


