package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.settings_uc.GetNightDayModeUc
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class GetNightDayModeUcTest : BaseTest() {

    private val uc by inject<GetNightDayModeUc>()

    @Before
    fun setUp() {
        declareMock<SettingsRepositoryImpl> {
            every { getMode() } returns flowOf(Mode.LIGHT)
        }
    }

    @Test
    fun `success night invoke`() = runBlocking {
        val exp = responseIsNightMode

        val act: Mode = uc.invoke().first()

        Assert.assertEquals(exp, act)
    }
}

