package com.voitenko.dev.inviteme.core

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.voitenko.dev.inviteme.core.di.repoModule
import com.voitenko.dev.inviteme.core.di.storageModule
import com.voitenko.dev.inviteme.core.di.ucModule
import io.mockk.mockkClass
import org.junit.Rule
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
open class BaseTest : Mock(), KoinTest {

    @get:Rule
    val mockProvider =
        MockProviderRule.create { clazz -> mockkClass(clazz, relaxed = true) }

    @ExperimentalStdlibApi
    @get:Rule
    val koinRule = KoinTestRule.create {
        androidContext(InstrumentationRegistry.getInstrumentation().targetContext)
        modules(repoModule, storageModule, ucModule)
    }
}