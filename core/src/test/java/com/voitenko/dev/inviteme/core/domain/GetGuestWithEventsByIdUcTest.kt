package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.models.models.common.PlaceHolderItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SpaceBoxItem
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.EventGuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfoItemCombine
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestWithEventsByIdUc
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

@ExperimentalStdlibApi
class GetGuestWithEventsByIdUcTest : BaseTest() {

    private val uc by inject<GetGuestWithEventsByIdUc>()

    @Before
    fun setUp() {
        declareMock<GuestRepositoryImpl> {
            every { getGuestWithEventsByGuestId(1) } returns flowOf(GUEST_WITH_EVENTS)
        }
    }

    @Test
    fun `success invoke with empty query`() = runBlocking {
        val exp = listOf(
            GuestInfo(GUEST_1.toGuestApp()),
            SearchItem(),
            EventGuestInfo(EVENT_1.toEventApp()),
            EventGuestInfo(EVENT_2.toEventApp()),
            EventGuestInfo(EVENT_3.toEventApp()),
            EventGuestInfo(EVENT_4.toEventApp()),
            EventGuestInfo(EVENT_5.toEventApp()),
            EventGuestInfo(EVENT_11.toEventApp()),
            EventGuestInfo(EVENT_111.toEventApp()),
        )

        val act: List<GuestInfoItemCombine> = uc.invoke(1).first()

        assertThat(exp, act)
    }

    @Test
    fun `success invoke by query`() = runBlocking {
        val exp = listOf(
            SpaceBoxItem(),
            SearchItem(),
            EventGuestInfo(EVENT_11.toEventApp()),
            EventGuestInfo(EVENT_111.toEventApp()),
        )

        val act: List<GuestInfoItemCombine> = uc.invoke(1, "11").first()

        assertThat(exp, act)
    }

    @Test
    fun `try success invoke by error query`() = runBlocking {
        val exp = listOf(
            SpaceBoxItem(),
            SearchItem(),
            PlaceHolderItem()
        )

        val act: List<GuestInfoItemCombine> = uc.invoke(1, "error").first()

        assertThat(exp, act)
    }

    @Test(expected = NoSuchElementException::class)
    fun `error invoke with empty query`() = runBlocking {
        val exp = NoSuchElementException::class

        val act: List<GuestInfoItemCombine> = uc.invoke(2).first()

        Assert.assertEquals(exp, act)
    }

    private fun assertThat(exp: List<GuestInfoItemCombine>, act: List<GuestInfoItemCombine>?) {
        Assert.assertEquals(exp.size, act?.size)
        act?.forEachIndexed { index, value ->
            val expectedItem = exp[index]
            Assert.assertThat(value, CoreMatchers.instanceOf(exp[index]::class.java))
            when {
                value is GuestInfo && expectedItem is GuestInfo ->
                    Assert.assertEquals(value.guest.guestId, expectedItem.guest.guestId)
                value is EventGuestInfo && expectedItem is EventGuestInfo ->
                    Assert.assertEquals(value.event.eventId, expectedItem.event.eventId)
            }
        }
    }
}