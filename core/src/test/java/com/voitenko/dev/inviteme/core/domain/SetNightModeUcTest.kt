package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.settings_uc.SetNightModeUc
import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import io.mockk.coEvery
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class SetNightModeUcTest : BaseTest() {

    private val uc by inject<SetNightModeUc>()

    @Before
    fun setUp() {
        declareMock<SettingsRepositoryImpl> {
            coEvery { setNightMode() } returns flowOf(Unit)
        }
    }

    @Test
    fun `success set night mode invoke`() = runBlocking {
        val exp = Unit

        val act: Unit = uc.invoke().first()

        Assert.assertEquals(exp, act)
    }
}