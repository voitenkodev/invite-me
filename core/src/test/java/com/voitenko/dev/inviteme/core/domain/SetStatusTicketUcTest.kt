package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc.SetStatusTicketUc
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.repository.TicketRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class SetStatusTicketUcTest : BaseTest() {

    private val uc by inject<SetStatusTicketUc>()

    @Before
    fun setUp() {
        declareMock<TicketRepositoryImpl> {
            every { setTicket(EVENT_REF_1) } returns flowOf(Unit)
        }
    }

    @Test
    fun `success set status for guest invoke`() = runBlocking {
        val exp = Unit

        val value = TicketApp(EVENT_REF_1.eventId, EVENT_REF_1.guestId, TicketApp.StatusApp.ACTIVE)

        val act = uc.invoke(value).first()

        Assert.assertEquals(exp, act)
    }
}