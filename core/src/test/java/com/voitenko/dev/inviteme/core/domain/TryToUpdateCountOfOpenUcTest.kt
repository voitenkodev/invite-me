package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.settings_uc.TryToUpdateCountOfOpenUc
import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import io.mockk.coEvery
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class TryToUpdateCountOfOpenUcTest : BaseTest() {

    private val uc by inject<TryToUpdateCountOfOpenUc>()

    @Before
    fun setUp() {
        declareMock<SettingsRepositoryImpl> {
            coEvery { getCountOpen() } returns flowOf(5)
        }
    }

    @Test
    fun `success update count invoke`() = runBlocking {
        val exp = false

        val act: Boolean = uc.invoke().first()

        Assert.assertEquals(exp, act)
    }
}