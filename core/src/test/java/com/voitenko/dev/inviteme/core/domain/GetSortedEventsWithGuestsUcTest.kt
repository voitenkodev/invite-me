package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.event_uc.GetSortedEventsWithGuestsUc
import com.voitenko.dev.inviteme.core.data.models.models.EventWithGuestsApp
import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class GetSortedEventsWithGuestsUcTest : BaseTest() {

    private val uc by inject<GetSortedEventsWithGuestsUc>()

    @Before
    fun setUp() {
        declareMock<EventRepositoryImpl> {
            every { getSortedEventsWithGuests() } returns flowOf(responseEventsWithGuests)
        }
    }

    @Test
    fun `success invoke`() = runBlocking {
        val exp = listOf(
            HomeEventWithGuests(
                false, EventWithGuestsApp(
                    EVENT_1.copy(date = dateWithYear(4020)).toEventApp(), listOf(
                        GUEST_4.toGuestApp(),
                        GUEST_5.toGuestApp(),
                    )
                )
            ),
            HomeEventWithGuests(
                false, EventWithGuestsApp(
                    EVENT_2.copy(date = dateWithYear(3020)).toEventApp(), listOf(
                        GUEST_1.toGuestApp(),
                        GUEST_2.toGuestApp(),
                        GUEST_3.toGuestApp(),
                    )
                )
            ),
            HomeEventWithGuests(
                true, EventWithGuestsApp(
                    EVENT_1.toEventApp(), listOf(
                        GUEST_1.toGuestApp(),
                        GUEST_2.toGuestApp(),
                        GUEST_3.toGuestApp(),
                        GUEST_4.toGuestApp(),
                        GUEST_5.toGuestApp(),
                        GUEST_11.toGuestApp(),
                        GUEST_111.toGuestApp(),
                    )
                )
            ),
        )

        val act: List<HomeEventWithGuests> = uc.invoke().first()

        act.forEachIndexed { index, actualValue ->
            val expectedValue = exp[index]
            Assert.assertThat(actualValue, CoreMatchers.instanceOf(expectedValue::class.java))
            Assert.assertEquals(expectedValue.isPassed, actualValue.isPassed)
            Assert.assertEquals(
                expectedValue.eventWithGuests.event.eventId,
                actualValue.eventWithGuests.event.eventId
            )
            Assert.assertEquals(
                expectedValue.eventWithGuests.guests.size,
                actualValue.eventWithGuests.guests.size
            )
        }
    }
}
