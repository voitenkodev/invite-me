package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestByIdUc
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class GetGuestByIdUcTest : BaseTest() {

    private val uc by inject<GetGuestByIdUc>()

    @Before
    fun setUp() {
        declareMock<GuestRepositoryImpl> {
            every { getGuestById(1) } returns flowOf(GUEST_1)
            every { getGuestById(2) } returns flowOf(null)
        }
    }

    @Test
    fun `success get guest by id invoke`() = runBlocking {
        val exp = GUEST_1.toGuestApp()

        val act: GuestApp? = uc.invoke(1).first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `error  get guest by id invoke`() = runBlocking {
        val exp = null

        val act: GuestApp? = uc.invoke(2).first()

        Assert.assertEquals(exp, act)
    }
}

