package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.settings_uc.SetDayModeUc
import com.voitenko.dev.inviteme.core.data.repository.SettingsRepositoryImpl
import io.mockk.coEvery
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

class SetDayModeUcTest : BaseTest() {

    private val uc by inject<SetDayModeUc>()

    @Before
    fun setUp() {
        declareMock<SettingsRepositoryImpl> {
            coEvery { setDayMode() } returns flowOf(Unit)
        }
    }

    @Test
    fun `success set day mode invoke`() = runBlocking {
        val exp = Unit

        val act: Unit = uc.invoke().first()

        Assert.assertEquals(exp, act)
    }
}

