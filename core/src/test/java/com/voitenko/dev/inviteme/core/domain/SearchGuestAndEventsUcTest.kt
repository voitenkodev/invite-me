package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.domain.guest_event_uc.SearchGuestAndEventsUc
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchEvent
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchGuest
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchHeader
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchItemCombine
import com.voitenko.dev.inviteme.core.data.models.toEventApp
import com.voitenko.dev.inviteme.core.data.models.toGuestApp
import com.voitenko.dev.inviteme.core.data.repository.EventRepositoryImpl
import com.voitenko.dev.inviteme.core.data.repository.GuestRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

@ExperimentalCoroutinesApi
class SearchGuestAndEventsUcTest : BaseTest() {

    private val uc by inject<SearchGuestAndEventsUc>()

    @Before
    fun setUp() {
        declareMock<EventRepositoryImpl> {
            every { getEventsByQuery("", 5) } returns flowOf(EVENTS_TAKE_FIVE)
            every { getEventsByQuery("11", 5) } returns flowOf(EVENTS_BY_11)
            every { getEventsByQuery("error", 5) } returns flowOf(emptyList())
            every { getEventsByQuery("event", 5) } returns flowOf(EVENTS_TAKE_FIVE)
            every { getEventsByQuery("guest", 5) } returns flowOf(emptyList())
        }
        declareMock<GuestRepositoryImpl> {
            every { getGuestsByQuery("", 5) } returns flowOf(GUESTS_TAKE_FIVE)
            every { getGuestsByQuery("11", 5) } returns flowOf(GUESTS_BY_11)
            every { getGuestsByQuery("error", 5) } returns flowOf(emptyList())
            every { getGuestsByQuery("event", 5) } returns flowOf(emptyList())
            every { getGuestsByQuery("guest", 5) } returns flowOf(GUESTS_TAKE_FIVE)
        }
    }

    @Test
    fun `success invoke with empty query`() = runBlocking {
        val exp = listOf(
            SearchHeader(SearchHeader.SearchHeaderType.EVENT_HEADER),
            SearchEvent(EVENT_1.toEventApp()),
            SearchEvent(EVENT_2.toEventApp()),
            SearchEvent(EVENT_3.toEventApp()),
            SearchEvent(EVENT_4.toEventApp()),
            SearchEvent(EVENT_5.toEventApp()),
            SearchHeader(SearchHeader.SearchHeaderType.GUEST_HEADER),
            SearchGuest(GUEST_1.toGuestApp()),
            SearchGuest(GUEST_2.toGuestApp()),
            SearchGuest(GUEST_3.toGuestApp()),
            SearchGuest(GUEST_4.toGuestApp()),
            SearchGuest(GUEST_5.toGuestApp()),
        )

        val act: List<SearchItemCombine> = uc.invoke("").first()

        assertThat(exp, act)
    }

    @Test
    fun `success invoke with query 11`() = runBlocking {
        val exp = listOf(
            SearchHeader(SearchHeader.SearchHeaderType.EVENT_HEADER),
            SearchEvent(EVENT_11.toEventApp()),
            SearchEvent(EVENT_111.toEventApp()),
            SearchHeader(SearchHeader.SearchHeaderType.GUEST_HEADER),
            SearchGuest(GUEST_11.toGuestApp()),
            SearchGuest(GUEST_111.toGuestApp()),
        )

        val act: List<SearchItemCombine> = uc.invoke("11").first()

        assertThat(exp, act)
    }


    @Test
    fun `success invoke with query error`() = runBlocking {
        val exp = listOf<SearchItemCombine>()

        val act: List<SearchItemCombine> = uc.invoke("error").first()

        assertThat(exp, act)
    }

    @Test
    fun `success invoke with empty guests`() = runBlocking {
        val exp = listOf(
            SearchHeader(SearchHeader.SearchHeaderType.EVENT_HEADER),
            SearchEvent(EVENT_1.toEventApp()),
            SearchEvent(EVENT_2.toEventApp()),
            SearchEvent(EVENT_3.toEventApp()),
            SearchEvent(EVENT_4.toEventApp()),
            SearchEvent(EVENT_5.toEventApp()),
        )

        val act: List<SearchItemCombine> = uc.invoke("event").first()

        assertThat(exp, act)
    }

    @Test
    fun `success invoke with empty events`() = runBlocking {
        val exp = listOf(
            SearchHeader(SearchHeader.SearchHeaderType.GUEST_HEADER),
            SearchGuest(GUEST_1.toGuestApp()),
            SearchGuest(GUEST_2.toGuestApp()),
            SearchGuest(GUEST_3.toGuestApp()),
            SearchGuest(GUEST_4.toGuestApp()),
            SearchGuest(GUEST_5.toGuestApp()),
        )

        val act: List<SearchItemCombine> = uc.invoke("guest").first()

        assertThat(exp, act)
    }

    private fun assertThat(exp: List<SearchItemCombine>, act: List<SearchItemCombine>?) {
        Assert.assertEquals(exp.size, act?.size)
        act?.forEachIndexed { index, value ->
            Assert.assertThat(value, CoreMatchers.instanceOf(exp[index]::class.java))
            val expectedItem = exp[index]
            when {
                value is SearchEvent && expectedItem is SearchEvent ->
                    Assert.assertEquals(value.event.eventId, expectedItem.event.eventId)
                value is SearchGuest && expectedItem is SearchGuest ->
                    Assert.assertEquals(value.guest.guestId, expectedItem.guest.guestId)
            }
        }
    }
}