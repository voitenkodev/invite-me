package com.voitenko.dev.inviteme.core.domain

import com.voitenko.dev.inviteme.core.BaseTest
import com.voitenko.dev.inviteme.core.data.models.dto.Ticket
import com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc.FindGuestOnEventUc
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.repository.TicketRepositoryImpl
import io.mockk.every
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock

@ExperimentalStdlibApi
class FindGuestOnEventUcTest : BaseTest() {

    private val uc by inject<FindGuestOnEventUc>()

    @Before
    fun setUp() {
        declareMock<TicketRepositoryImpl> {
            every { findGuestOnEvent(1, 1) } returns flowOf(EVENT_REF_1)
            every { findGuestOnEvent(2, 2) } returns flowOf(null)
        }
    }

    @Test
    fun `success find invoke`() = runBlocking {
        val exp = TicketApp(1, 1, TicketApp.StatusApp.ACTIVE)

        val act: TicketApp? = uc.invoke(1, 1).first()

        Assert.assertEquals(exp, act)
    }

    @Test
    fun `error find invoke`() = runBlocking {
        val exp = null

        val act: TicketApp? = uc.invoke(2, 2).first()

        Assert.assertEquals(exp, act)
    }
}
