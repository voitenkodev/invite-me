buildscript {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven { url = uri(Firebase.fabricRepo) }
        maven {
            url = uri(Map.mapRepository)
            authentication { create<BasicAuthentication>("basic") }
            credentials {
                username = Map.username
                password = Map.cleanMapKey
            }
        }
    }

    dependencies {
        classpath(Plugins.gradleKotlinPlug)
        classpath(RemoteStorage.serializationPluginLib)
        classpath(Plugins.gradle)
        classpath(Navigation.navPlugin)
        classpath(Firebase.fabricTools)
        classpath(Firebase.crashlyticsPlugin)
        classpath(Plugins.googleServices)
        classpath(Plugins.updateDependencies)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri(Repo.jitpack) }
        maven { url = uri(Repo.mavenGoogle) }
        maven {
            url = uri(Map.mapRepository)
            authentication { create<BasicAuthentication>("basic") }
            credentials {
                username = Map.username
                password = Map.cleanMapKey
            }
        }
    }
}
tasks.register("clean", Delete::class) { delete(rootProject.buildDir) }
