plugins {
    id("com.android.application")
    id(Navigation.pluginArgs)
    id(Firebase.crashlyticsPluginId)
    id("com.google.gms.google-services")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("name.remal.check-dependency-updates") version "1.3.4" // ./gradlew checkDependencyUpdates
}

android {
    compileSdk = App.compile
    buildToolsVersion = App.build
    defaultConfig {
        applicationId = App.appPackage
        minSdk = App.min
        targetSdk = App.target
        versionCode = App.versionCode
        versionName = App.versionName
        testInstrumentationRunner = ("${App.appPackage}.utils.InstrumentalTestRunner")
        buildConfigField("String", "MAP_KEY", Map.mapKey)
    }

    buildTypes.getByName("debug") {
        addManifestPlaceholders(mapOf("appName" to "Invite Me (debug)"))
        applicationIdSuffix = ".debug"
    }

    buildTypes.getByName("release") {
        addManifestPlaceholders(mapOf("appName" to "Invite Me"))
        isMinifyEnabled = true
        isShrinkResources = true
        val defGuard = getDefaultProguardFile("proguard-android-optimize.txt")
        proguardFiles(defGuard, ("proguard-rules.pro"))
    }

    buildFeatures.dataBinding = true
    buildFeatures.compose = true
    composeOptions.kotlinCompilerExtensionVersion = Compose.composeVers
    testOptions.unitTests.isIncludeAndroidResources = true // for robolectric
    androidExtensions.features = setOf("parcelize") // disable the generating of synthetic
    compileOptions.sourceCompatibility(JavaVersion.VERSION_11)
    compileOptions.targetCompatibility(JavaVersion.VERSION_11)
    configurations.all { resolutionStrategy.force("org.objenesis:objenesis:2.6") } // mockito force min-api 26
}

dependencies {
    implementation(fileTree(mapOf("dir" to "Libs", "include" to listOf("*.jar"))))
    implementation(project(":core"))

    implementation(Other.kotlin)
    implementation(Other.coroutines)
    implementation(Other.coreKtx)

    implementation(Other.appCompat)
    implementation(Other.fragmentApi)
    implementation(Other.constraintLayout)
    implementation(Other.viewModel)
    implementation(Other.runtime)
    implementation(Other.qrCode)
    implementation(Other.scanner)
    implementation(Other.timePicker)
    implementation(Other.lottie)
    implementation(Other.playCore)

    implementation(Other.coil)
    implementation(Other.coilTools)

    implementation(Map.mapBox)
    implementation(Map.marker)

    implementation(Firebase.analytics)
    implementation(Firebase.crashlytics)

    implementation(DI.koinExt)
    implementation(DI.koinAndroid)
    implementation(Navigation.navigationFragment)
    implementation(Navigation.navigationUi)

    implementation(Compose.ui)
    implementation(Compose.tools)
    implementation(Compose.activity)
    implementation(Compose.foundation)
    implementation(Compose.material)
    implementation(Compose.materialIcons)
    implementation(Compose.materialIconsExt)

// TESTING -------------------------------------------------------------------------------------

    androidTestImplementation(Test.junit)
    androidTestImplementation(Test.mockitoAndroid)
    androidTestImplementation(Test.testExt)
    androidTestImplementation(Test.espresso)
    androidTestImplementation(Test.rules)
    androidTestImplementation(Test.koinTest)

    testImplementation(Test.junit)
    testImplementation(Test.junitParametrize)
}
