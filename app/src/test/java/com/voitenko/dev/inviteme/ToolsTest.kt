package com.voitenko.dev.inviteme

import com.voitenko.dev.inviteme.utils.extensions.calcByRatio
import com.voitenko.dev.inviteme.utils.extensions.pruneToString
import com.voitenko.dev.inviteme.utils.extensions.round
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

// TODO move to app module
@RunWith(JUnitParamsRunner::class)
class ToolsTest {

    @Test
    @Parameters(
        value = ["192,192,16:9,192F,108F", "12,673,16:9,12F,6F", "125,42,16:9,74F,42F", "44,44,16:9,44F,24F",
            "4000,2459,16:9,4000F,2250F", "2469,4000,1:1,2469F,2469F", "4000,2469,1:1,2469F,2469F"]
    )
    fun `calculate size of image by ratio`(
        w: Int, h: Int, ratio: String, expW: Float, expH: Float
    ) {
        val act = calcByRatio(w, h, ratio)
        Assert.assertEquals(act, expW to expH)
    }

    @Test
    @Parameters(value = ["1000,1K", "43213,43K", "4321123,4M", "423321123,423M", "21123,21K"])
    fun `prune integer to ui string`(given: Int, exp: String) {
        val act = given.pruneToString()
        Assert.assertEquals(exp, act)
    }

    @Test
    @Parameters(
        value = [
            "14141F,111,14141.0F", "14141F,0,14141.0F", "14141F,1,14141.0F",
            "14141.5F,111,14141.5F", "14141.5F,0,14141.0F", "14141.5F,1,14141.5F",
            "14141.51F,111,14141.51F", "14141.51F,0,14141.0F", "14141.51F,1,14141.5F",
            "14141.59F,1,14141.6F", "14141.529111F,2,14141.53F", "14141.529111F,3,14141.529F",
        ]
    )
    fun `round float by`(given: Float, option: Int, exp: Float) {
        val act = given.round(option)
        Assert.assertEquals(exp, act)
    }

}