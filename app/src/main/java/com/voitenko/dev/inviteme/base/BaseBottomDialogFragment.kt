package com.voitenko.dev.inviteme.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.voitenko.dev.inviteme.R
import org.koin.androidx.scope.fragmentScope

abstract class BaseBottomDialogFragment<B : ViewDataBinding>(@LayoutRes val layout: Int) :
    BottomSheetDialogFragment(), FragmentContract {


    override fun getTheme() = R.style.bottom_dialog_fragment
    override val f: Fragment = this
    abstract fun B.setupUi(b: Bundle?)
    abstract val viewModel: BaseViewModel
    protected open fun B.releaseUi() {}

    private var _binding: B? = null
    protected val binding: B
        get() = _binding ?: throw NullPointerException("Binding is null (check onDestroyView)")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, layout, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        activity?.initAnalytics()
        activity?.handleBackPress()
        binding.setupUi(savedInstanceState)
        return binding.root
    }

    override fun onDestroyView() {
        binding.releaseUi()
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() = run { super.onResume(); hideKeyboard() }
}
