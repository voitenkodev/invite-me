package com.voitenko.dev.inviteme.ui.event_info

import android.graphics.Paint
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.UniversalItemHolder
import com.voitenko.dev.inviteme.core.data.models.models.common.PlaceHolderItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SpaceBoxItem
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfo
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.models.event_info.GuestEventInfo
import com.voitenko.dev.inviteme.databinding.ItemEventInfoEventBinding
import com.voitenko.dev.inviteme.databinding.ItemPlaceHolderBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchInfoBinding
import com.voitenko.dev.inviteme.utils.binding.setDrawableColor
import com.voitenko.dev.inviteme.utils.extensions.drawable
import com.voitenko.dev.inviteme.utils.extensions.inflateView
import com.voitenko.dev.inviteme.utils.extensions.ui.cleanable
import com.voitenko.dev.inviteme.utils.extensions.ui.withTint

@ExperimentalStdlibApi
class EventInfoAdapter(private val vm: EventInfoViewModel) :
    ListAdapter<EventInfoItemCombine, UniversalItemHolder>(EventInfoDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UniversalItemHolder(parent.inflateView(viewType))

    override fun getItemViewType(position: Int) = when (getItem(position).getEvenInfoType()) {
        EventInfoItemCombine.EventInfoItemType.EVENT_INFO -> R.layout.item_event_info_event
        EventInfoItemCombine.EventInfoItemType.GUEST_EVENT_INFO -> R.layout.item_event_info_guest
        EventInfoItemCombine.EventInfoItemType.SEARCH_INFO -> R.layout.item_search_info
        EventInfoItemCombine.EventInfoItemType.PLACE_HOLDER -> R.layout.item_place_holder
        EventInfoItemCombine.EventInfoItemType.SPACE_BOX -> R.layout.item_space_box
    }

    override fun onBindViewHolder(holder: UniversalItemHolder, position: Int) {
        when (getItem(holder.adapterPosition).getEvenInfoType()) {
            EventInfoItemCombine.EventInfoItemType.EVENT_INFO -> bindEventInfo(holder)
            EventInfoItemCombine.EventInfoItemType.GUEST_EVENT_INFO ->
                holder.bind(getItem(holder.adapterPosition), vm)
            EventInfoItemCombine.EventInfoItemType.SEARCH_INFO -> bindSearchItem(holder, vm)
            EventInfoItemCombine.EventInfoItemType.SPACE_BOX ->
                holder.bind(getItem(holder.adapterPosition), vm)
            EventInfoItemCombine.EventInfoItemType.PLACE_HOLDER -> bindPlaceHolder(holder)
        }
    }

    private fun bindEventInfo(holder: UniversalItemHolder) {
        val binding = holder.binding as ItemEventInfoEventBinding
        val eventItem = getItem(holder.adapterPosition) as EventInfo
        binding.addBtn.setOnClickListener { vm.navigateToAddNewGuest(eventItem.event.getImageInUri()) }
        binding.addGuestTxt.setOnClickListener { vm.navigateToAddNewGuest(eventItem.event.getImageInUri()) }
        binding.cameraBtn.setOnClickListener { vm.navigateToQrCodeScanner() }
        binding.cameraTxt.setOnClickListener { vm.navigateToQrCodeScanner() }
        binding.locationTv.setOnClickListener { vm.showLocationInMap(eventItem.event.addressApp) }
        binding.locationTv.paintFlags = binding.locationTv.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        holder.bind(getItem(holder.adapterPosition), vm)
        vm.prepareLayoutEventInfo()
    }

    private fun bindPlaceHolder(holder: UniversalItemHolder) {
        val binding = holder.binding as ItemPlaceHolderBinding
        binding.placeHolderTv.setText(R.string.search_guest_text_holder)
    }

    private fun bindSearchItem(holder: UniversalItemHolder, vm: EventInfoViewModel) {
        (holder.binding as ItemSearchInfoBinding).searchField.apply {
            val clearIcon = context.drawable(R.drawable.ic_search_clear)
                ?.withTint(R.color.color_event, context)
            setHint(R.string.hint_label_guest_search)
            cleanable(clearIcon)
            setDrawableColor(R.color.color_event)
            doOnTextChanged { text, _, _, _ -> vm.searchBy(text) }
        }
    }

    companion object EventInfoDiffUtils : DiffUtil.ItemCallback<EventInfoItemCombine>() {
        override fun areItemsTheSame(
            oldItem: EventInfoItemCombine, newItem: EventInfoItemCombine
        ) =
            if ((oldItem is EventInfo && newItem is EventInfo) && oldItem.event.eventId == newItem.event.eventId) true
            else if ((oldItem is GuestEventInfo && newItem is GuestEventInfo) && oldItem.guest.guestId == newItem.guest.guestId) true
            else if (oldItem is SearchItem && newItem is SearchItem) true
            else if (oldItem is PlaceHolderItem && newItem is PlaceHolderItem) true
            else oldItem is SpaceBoxItem && newItem is SpaceBoxItem

        override fun areContentsTheSame(
            oldItem: EventInfoItemCombine, newItem: EventInfoItemCombine
        ) = true
    }
}