package com.voitenko.dev.inviteme.ui.home

import android.view.View
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
import com.voitenko.dev.inviteme.core.domain.event_uc.GetSortedEventsWithGuestsUc
import com.voitenko.dev.inviteme.utils.AdapterTransitionBundle
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.StateErrorApp
import com.voitenko.dev.inviteme.base.StateResult
import com.voitenko.dev.inviteme.base.SuccessState
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import org.koin.core.component.inject

class HomeViewModel : BaseViewModel() {

    private val getEventsWithGuestsUc by inject<GetSortedEventsWithGuestsUc>()

    val modeEvent = shared<Mode>()
    val backToScreenTransitionEvent = shared<View>()

    val listOfEvents = stateResultFlow<List<HomeEventWithGuests>>()
    val startAnimatedNavigationEvent = shared<Pair<Int, Int>>()

    var reEnterAnimation: Pair<Boolean, PreviousFragmentState?> = false to null
    private var adapterTransitionBundle = AdapterTransitionBundle()
    private var firstEnter = true // On first open Home Fragment

    init {
        getListOfEvents()
    }

    private fun getListOfEvents() = listOfEvents.processing {
        getEventsWithGuestsUc.invoke()
    }

    fun isNeedTransitionAnim() = adapterTransitionBundle.needAnim

    fun startAnimationToEvent() =
        startAnimatedNavigationEvent.call(R.id.defaultState to R.id.openedNewEventState)

    fun startAnimationToSearch() =
        startAnimatedNavigationEvent.call(R.id.defaultState to R.id.openedSearchState)

    fun navigateToEvent() {
        reEnterAnimation = true to PreviousFragmentState.NEW_EVENT_FRAGMENT
        navigatorEvent.call(AppRouter.FromHome.ToEvent)
    }

    fun doOnFirstEnter(action: () -> Unit) = takeIf { firstEnter }?.let {
        action.invoke()
        firstEnter = false
    }

    fun navigateToEventInfo(view: View, eventId: Long, position: Int) {
        adapterTransitionBundle.update(position, view, true)
        navigatorEvent.call(AppRouter.FromHome.ToEventInfo(adapterTransitionBundle, eventId))
    }

    fun prepareEventLayout(view: View, position: Int) {
        view.transitionName = null
        if (adapterTransitionBundle.needAnim && adapterTransitionBundle.position == position) {
            backToScreenTransitionEvent.call(view)
            adapterTransitionBundle.needAnim = false
        }
    }

    fun changeMode(checked: Boolean) = modeEvent.call(if (checked) Mode.DARK else Mode.LIGHT)

    fun finishActivity() {
        navigatorEvent.call(AppRouter.FromApp.ToFinishActivity)
    }

    fun navigateToSearch() {
        reEnterAnimation = true to PreviousFragmentState.SEARCH_FRAGMENT
        navigatorEvent.call(AppRouter.FromHome.ToSearch)
    }

    fun isBackAnimationFromNewEvent() =
        reEnterAnimation.second == PreviousFragmentState.NEW_EVENT_FRAGMENT

    fun clearPreviousNavigation() {
        reEnterAnimation = false to null
    }
}

enum class PreviousFragmentState {
    SEARCH_FRAGMENT, NEW_EVENT_FRAGMENT;

    fun equals(previous: PreviousFragmentState, action: () -> Unit) = apply {
        if (previous == this@PreviousFragmentState) action.invoke()
    }
}