package com.voitenko.dev.inviteme.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.voitenko.dev.inviteme.BR

/**
use <variable name = "item">
use <variable name = "position">
use <variable name = "vm">
on xml
 */
open class UniversalItemHolder(open val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(obj: Any, vm: Any? = null, position: Any? = null) {
        binding.setVariable(BR.item, obj)
        vm?.let { binding.setVariable(BR.vm, it) }
        position?.let { binding.setVariable(BR.position, it) }
        binding.executePendingBindings()
    }
}

inline fun <reified T> RecyclerView.getBindingItemByPos(pos: Int): T? {
    val holder = this.findViewHolderForAdapterPosition(pos)
    if (holder is UniversalItemHolder) {
        val holderBinding = holder.binding
        return if (holderBinding is T) holderBinding
        else return null
    } else return null
}