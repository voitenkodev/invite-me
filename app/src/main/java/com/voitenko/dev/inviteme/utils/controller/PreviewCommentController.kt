package com.voitenko.dev.inviteme.utils.controller

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import com.google.android.play.core.ktx.launchReview
import com.google.android.play.core.ktx.requestReview
import com.google.android.play.core.review.ReviewManagerFactory
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun FragmentActivity.showPreviewCommentDialog() = lifecycleScope.launch {
    val manager = ReviewManagerFactory.create(applicationContext)
    val request = manager.requestReview()
    delay(1000)
    manager.launchReview(this@showPreviewCommentDialog, request)
}