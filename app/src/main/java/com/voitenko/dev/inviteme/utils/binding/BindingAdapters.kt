package com.voitenko.dev.inviteme.utils.binding

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.net.Uri
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.utils.extensions.color
import com.voitenko.dev.inviteme.utils.extensions.ui.addImeActionDoneListener
import com.voitenko.dev.inviteme.utils.extensions.ui.addImeActionNextListener
import com.voitenko.dev.inviteme.utils.extensions.ui.addMatrixScaleStart
import com.voitenko.dev.inviteme.utils.extensions.ui.gone
import com.voitenko.dev.inviteme.utils.extensions.ui.loadImage
import com.voitenko.dev.inviteme.utils.extensions.ui.loadRoundedImage
import com.voitenko.dev.inviteme.utils.extensions.ui.show


@BindingAdapter("app:matrixScaleStart")
fun AppCompatImageView.setMatrixScaleStart(heightOfImage: Float) {
    this.addMatrixScaleStart(heightOfImage)
}

@BindingAdapter("app:src")
fun AppCompatImageView.setSrc(uri: Uri?) {
    this.loadImage(uri, null, R.drawable.ic_event_photo_holder)
}

@BindingAdapter("app:srcRound")
fun AppCompatImageView.setSrcRound(uri: Uri?) {
    this.loadRoundedImage(uri, null, R.drawable.ic_guest_photo_holder)
}

@BindingAdapter("app:visibleOrGone")
fun View.visibleOrGone(isVisible: Boolean) {
    if (isVisible) this.show() else this.gone()
}

@BindingAdapter("app:onDoneClicked")
inline fun AppCompatEditText.onDoneClicked(crossinline onDoneHandler: () -> Unit) {
    this.addImeActionDoneListener { onDoneHandler.invoke() }
}

@BindingAdapter("app:onNextClicked")
inline fun AppCompatEditText.onNextClicked(crossinline onDoneHandler: () -> Unit) {
    this.addImeActionNextListener { onDoneHandler.invoke() }
}

@BindingAdapter("app:drawableTint")
fun AppCompatEditText.setDrawableColor(color: Int) {
    compoundDrawables.filterNotNull().forEach {
        it.colorFilter = PorterDuffColorFilter(this.context.color(color), PorterDuff.Mode.MULTIPLY)
    }
}