package com.voitenko.dev.inviteme.base

import androidx.annotation.StringRes
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel

class LoadingState<out T>(val scope: CoroutineScope) : StateResult<T>()
class ErrorState<out T>(val t: StateErrorApp?) : StateResult<T>()
class CompleteState<out T> : StateResult<T>()
class SuccessState<T>(val value: T) : StateResult<T>()

sealed class StateResult<out T> {

    fun cancel(): Boolean = when (this) {
        is LoadingState -> kotlin.runCatching { scope.cancel(CancellationException("Already cancelled")) }.isSuccess
        else -> false
    }

    companion object {
        fun <T> loading(scope: CoroutineScope): StateResult<T> = LoadingState(scope)
        fun <T> success(value: T): StateResult<T> = SuccessState(value)
        fun <T> error(t: StateErrorApp): StateResult<T> = ErrorState(t)
        fun <T> complete(): StateResult<T> = CompleteState()
    }
}

data class StateErrorApp(
    @StringRes val localizedMessage: Int? = null, // localized expected exception
    val outMessage: String? = null, // unExcepted exception from API
    val androidThrowMessage: String? = null // any android exception
) : Throwable(androidThrowMessage ?: outMessage)
