package com.voitenko.dev.inviteme.utils.controller

import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

private const val INTERVAL = 0L
private const val FIRST_DURATION = 1500L
private const val SECOND_DURATION = 1500L

class PulseAnimationController(background_one: View, background_two: View) : LifecycleObserver {

    private val handler: Handler by lazy { Handler(Looper.getMainLooper()) }
    private val runnableAnim: Runnable by lazy {
        Runnable {
            background_one.animate().scaleX(1.4f).scaleY(1.4f).alpha(0.2f)
                .setDuration(FIRST_DURATION)
                .withEndAction {
                    background_one.scaleX = 0.3f
                    background_one.scaleY = 0.3f
                    background_one.alpha = 0.4f
                    background_two.animate().scaleX(1.3f).scaleY(1.3f).alpha(0.2f)
                        .setDuration(SECOND_DURATION)
                        .withEndAction {
                            background_two.scaleX = 0.3f
                            background_two.scaleY = 0.3f
                            background_two.alpha = 0.6f
                            handler.postDelayed(runnableAnim, INTERVAL)
                        }
                }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() = runnableAnim.run()

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() = handler.removeCallbacks(runnableAnim)
}