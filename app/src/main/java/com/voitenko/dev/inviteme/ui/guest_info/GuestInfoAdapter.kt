package com.voitenko.dev.inviteme.ui.guest_info

import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.UniversalItemHolder
import com.voitenko.dev.inviteme.core.data.models.models.common.PlaceHolderItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItem
import com.voitenko.dev.inviteme.core.data.models.models.common.SpaceBoxItem
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.EventGuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfo
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfoItemCombine
import com.voitenko.dev.inviteme.databinding.ItemPlaceHolderBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchInfoBinding
import com.voitenko.dev.inviteme.utils.binding.setDrawableColor
import com.voitenko.dev.inviteme.utils.extensions.drawable
import com.voitenko.dev.inviteme.utils.extensions.inflateView
import com.voitenko.dev.inviteme.utils.extensions.ui.cleanable
import com.voitenko.dev.inviteme.utils.extensions.ui.withTint

@ExperimentalStdlibApi
class GuestInfoAdapter(private val vm: GuestInfoViewModel) :
    ListAdapter<GuestInfoItemCombine, UniversalItemHolder>(GuestInfoDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UniversalItemHolder(parent.inflateView(viewType))

    override fun getItemViewType(position: Int) = when (getItem(position).getGuestInfoType()) {
        GuestInfoItemCombine.GuestInfoItemType.GUEST_INFO -> R.layout.item_guest_info_guest
        GuestInfoItemCombine.GuestInfoItemType.EVENT_GUEST_INFO -> R.layout.item_guest_info_event
        GuestInfoItemCombine.GuestInfoItemType.SEARCH_INFO -> R.layout.item_search_info
        GuestInfoItemCombine.GuestInfoItemType.PLACE_HOLDER -> R.layout.item_place_holder
        GuestInfoItemCombine.GuestInfoItemType.SPACE_BOX -> R.layout.item_space_box
    }

    override fun onBindViewHolder(holder: UniversalItemHolder, position: Int) {
        when (getItem(holder.adapterPosition).getGuestInfoType()) {
            GuestInfoItemCombine.GuestInfoItemType.EVENT_GUEST_INFO -> {
                holder.bind(getItem(holder.adapterPosition), vm)
                vm.prepareLayoutEventInfo()
            }
            GuestInfoItemCombine.GuestInfoItemType.GUEST_INFO ->
                holder.bind(getItem(holder.adapterPosition))
            GuestInfoItemCombine.GuestInfoItemType.SEARCH_INFO -> bindSearchItem(holder, vm)
            GuestInfoItemCombine.GuestInfoItemType.SPACE_BOX ->
                holder.bind(getItem(holder.adapterPosition), vm)
            GuestInfoItemCombine.GuestInfoItemType.PLACE_HOLDER -> bindPlaceHolder(holder)
        }
    }

    private fun bindPlaceHolder(holder: UniversalItemHolder) {
        val binding = holder.binding as ItemPlaceHolderBinding
        binding.placeHolderTv.setText(R.string.search_event_text_holder)
    }

    private fun bindSearchItem(holder: UniversalItemHolder, vm: GuestInfoViewModel) {
        (holder.binding as ItemSearchInfoBinding).searchField.apply {

            val clearIcon = context.drawable(R.drawable.ic_search_clear)
                ?.withTint(R.color.color_guest_additional, context)

            setHint(R.string.hint_label_event_search)
            cleanable(clearIcon)
            setDrawableColor(R.color.color_guest_additional)
            doOnTextChanged { text, _, _, _ -> vm.searchBy(text) }
        }
    }

    companion object GuestInfoDiffUtils : DiffUtil.ItemCallback<GuestInfoItemCombine>() {
        override fun areItemsTheSame(
            oldItem: GuestInfoItemCombine,
            newItem: GuestInfoItemCombine
        ): Boolean {
            return if ((oldItem is GuestInfo && newItem is GuestInfo) && oldItem.guest.guestId == newItem.guest.guestId) true
            else if (oldItem is EventGuestInfo && newItem is EventGuestInfo) true
            else if (oldItem is SearchItem && newItem is SearchItem) true
            else if (oldItem is PlaceHolderItem && newItem is PlaceHolderItem) true
            else oldItem is SpaceBoxItem && newItem is SpaceBoxItem
        }

        override fun areContentsTheSame(
            oldItem: GuestInfoItemCombine,
            newItem: GuestInfoItemCombine
        ) = true
    }
}
