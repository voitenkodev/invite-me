package com.voitenko.dev.inviteme.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.voitenko.dev.inviteme.utils.extensions.hideKeyboardFromScreen

abstract class BaseActivity<B : ViewDataBinding>(@LayoutRes val layout: Int) : AppCompatActivity() {

    abstract fun setupBinding(binding: B)

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout)
        setupBinding(binding)
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
    }

    private fun hideKeyboard() = hideKeyboardFromScreen()
}
