package com.voitenko.dev.inviteme.ui.new_guest

import android.net.Uri
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import coil.load
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.databinding.FragmentNewGuestBinding
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.StateErrorApp
import com.voitenko.dev.inviteme.utils.binding.onDoneClicked
import com.voitenko.dev.inviteme.utils.binding.visibleOrGone
import com.voitenko.dev.inviteme.utils.controller.addMotionController
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_LONG
import com.voitenko.dev.inviteme.utils.extensions.cutByRatio
import com.voitenko.dev.inviteme.utils.extensions.getBitmap
import com.voitenko.dev.inviteme.utils.extensions.navigateBack
import com.voitenko.dev.inviteme.utils.extensions.navigateToGallery
import com.voitenko.dev.inviteme.utils.extensions.string
import com.voitenko.dev.inviteme.utils.extensions.ui.alphaTo
import com.voitenko.dev.inviteme.utils.extensions.ui.getSuccessSnackBar
import com.voitenko.dev.inviteme.utils.extensions.ui.loadBlurImage
import com.voitenko.dev.inviteme.utils.extensions.ui.loadRoundedImage
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.ui.shake
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class NewGuestFragment : BaseFragment<FragmentNewGuestBinding>(R.layout.fragment_new_guest) {

    private val args by navArgs<NewGuestFragmentArgs>()
    override val viewModel by viewModel<NewGuestViewModel> { parametersOf(args.eventId) }

    @ExperimentalCoroutinesApi
    override fun FragmentNewGuestBinding.setupUi(b: Bundle?) {
        motionRoot.listenMotionAnimation()
        viewModel.observeNavigation()
        viewModel.observeVM()
        addActions()
    }

    private fun FragmentNewGuestBinding.addActions() {
        guestImageIv.setOnClickListener { viewModel.navigateToGallery() }
        backBtn.setOnClickListener { viewModel.startEndAnimationScreen() }
        saveIv.setOnClickListener { viewModel.findOrSaveGuest() }
        nameGuestEt.doOnTextChanged { text, _, _, _ -> viewModel.changeNameGuest(text) }
        phoneGuestEt.doOnTextChanged { text, _, _, _ -> viewModel.changePhoneGuest(text) }
        emailGuestEt.doOnTextChanged { text, _, _, _ -> viewModel.changeEmailGuest(text) }
        emailGuestEt.onDoneClicked { viewModel.findOrSaveGuest() }
        phoneGuestEt.onDoneClicked { viewModel.findOrSaveGuest() }
    }

    private fun initEventViews() = binding.eventPictureIv.apply {
        loadBlurImage(args.eventImageUri, null, null)
        alphaTo(1F) {}
    }

    private fun NewGuestViewModel.observeVM() = viewLifecycleOwner
        .observe(qrCodeObservable) { state ->
            state.processingUi(success = { binding.guestQrCodeIv.load(it) })
        }.observe(changeImage) { state ->
            state.processingUi(success = { })
        }.observe(initGuestToUiEvent) { state ->
            state.processingUi(success = { binding.initGuest(it) })
        }.observe(startEndAnimationScreenEvent) {
            releaseGuestUi()
        }.observe(saveGuestEvent) { state ->
            state.processingUi(success = {
                requireActivity().getSuccessSnackBar(R.string.guest_success_added_snack)
                    ?.apply { show(); doOnEnd { backPress() } }
            })
        }

    private fun releaseGuestUi() = lifecycleScope.launch {
        binding.eventPictureIv.alphaTo(0F)
        binding.motionRoot.transitionToState(R.id.closedNewGuestState)
    }

    private fun FragmentNewGuestBinding.initGuest(guest: GuestApp) {
        emailGuestEt.clearFocus()
        if (guest != GuestApp.EMPTY_GUEST) saveIv.setImageResource(R.drawable.ic_done)
        guestImageIv.loadRoundedImage(guest.getImageInUri(), null)
        nameGuestEt.isEnabled = (guest.guestId != DEFAULT_LONG)
        phoneGuestEt.isEnabled = (guest.guestId != DEFAULT_LONG)
        emailGuestEt.isEnabled = (guest.guestId == DEFAULT_LONG)
        blockingView.visibleOrGone(guest.guestId == DEFAULT_LONG)
        blockingView2.visibleOrGone(guest.guestId != DEFAULT_LONG)
        nameGuestEt.setText(guest.guestName)
        phoneGuestEt.setText(guest.phone)
        emailGuestEt.setText(guest.email)
    }

    private fun NewGuestViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromApp.ToGallery -> galleryResult.navigateToGallery()
            is AppRouter.FromApp.ToBack -> this@NewGuestFragment.navigateBack()
        }
    }

    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            binding.guestImageIv.loadRoundedImage(uri, null)
            uri?.getBitmap(requireContext())
                ?.cutByRatio(requireContext().string(R.string.square_ratio_image))
                ?.let { bitmap -> viewModel.changeImageGuest(bitmap) } ?: viewModel.bitmapError()
        }

    override fun observeError(errorApp: StateErrorApp) {
        when (errorApp.localizedMessage) {
            R.string.error_input_guest_name -> binding.nameGuestEt.shake {}
            R.string.error_input_guest_email -> binding.emailGuestEt.shake {}
            R.string.error_input_email -> binding.emailGuestEt.shake {}
        }
        super.observeError(errorApp)
    }

    override fun backPress() {
        binding.backBtn.performClick()
    }

    @ExperimentalCoroutinesApi
    private fun MotionLayout.listenMotionAnimation() =
        this.addMotionController(viewLifecycleOwner.lifecycle)
            .doOnEnd(R.id.closedNewGuestState) { viewModel.navigateBack() }
            .doOnEnd(R.id.openedNewGuestState) { initEventViews() }
            .onStartScreen { transitionToEnd() }
}