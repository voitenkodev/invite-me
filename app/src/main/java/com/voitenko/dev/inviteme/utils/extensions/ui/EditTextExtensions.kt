package com.voitenko.dev.inviteme.utils.extensions.ui

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.BindingAdapter

fun AppCompatEditText.addImeActionDoneListener(action: () -> Unit) =
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) action.invoke()
        false
    }

fun AppCompatEditText.addImeActionNextListener(action: () -> Unit) =
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_NEXT) action.invoke()
        false
    }

@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("app:cleanable")
fun AppCompatEditText.cleanable(d: Drawable?) {
    val leftImg = this.compoundDrawablesRelative[0]
    val topImg = compoundDrawablesRelative[1]
    val bottomImg = compoundDrawablesRelative[3]
    setCompoundDrawablesWithIntrinsicBounds(leftImg, topImg, d, bottomImg)
    val updateRightDrawableAction = {
        if (text?.isNotEmpty() == true) setCompoundDrawables(leftImg, topImg, d, bottomImg)
        else setCompoundDrawables(leftImg, topImg, null, bottomImg)
    }
    updateRightDrawableAction()
    doAfterTextChanged { updateRightDrawableAction() }
    setOnTouchListener { v, event ->
        var hasConsumed = false
        if (v is EditText) {
            if (event.x >= v.width - v.totalPaddingRight) {
                if (event.action == MotionEvent.ACTION_UP) {
                    text?.clear()
                    setCompoundDrawables(leftImg, topImg, null, bottomImg)
                    requestFocus()
                }
                hasConsumed = true
            }
        }
        hasConsumed
    }
}