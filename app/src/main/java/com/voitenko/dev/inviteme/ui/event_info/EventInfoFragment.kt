package com.voitenko.dev.inviteme.ui.event_info

import android.os.Bundle
import androidx.navigation.fragment.navArgs
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.databinding.FragmentInfoEventBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchInfoBinding
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.getBindingItemByPos
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.inflateNavSharedTransition
import com.voitenko.dev.inviteme.utils.extensions.navigateTo
import com.voitenko.dev.inviteme.utils.extensions.navigateToEmail
import com.voitenko.dev.inviteme.utils.extensions.navigateToMap
import com.voitenko.dev.inviteme.utils.extensions.showLog
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

@ExperimentalStdlibApi
class EventInfoFragment : BaseFragment<FragmentInfoEventBinding>(R.layout.fragment_info_event) {

    private val adapter: EventInfoAdapter by inject { parametersOf(viewModel) }
    override val viewModel: EventInfoViewModel by viewModel {
        val args by navArgs<EventInfoFragmentArgs>(); parametersOf(args.eventId)
    }

    override fun FragmentInfoEventBinding.setupUi(b: Bundle?) {
        this@EventInfoFragment.inflateNavSharedTransition(R.transition.custom_transition)
        postponeEnterTransition()
        viewModel.observeVM()
        viewModel.observeNavigation()
        eventInfoRv.adapter = adapter
    }

    override fun FragmentInfoEventBinding.releaseUi() = let { eventInfoRv.adapter = null }

    private fun EventInfoViewModel.observeVM() = viewLifecycleOwner
        .observe(eventInfo) { state ->
            state.processingUi(success = {
                adapter.submitList(it)
                binding.eventInfoRv.scrollTo(0, 0)
            })
        }.observe(prepareInfoEventLayout) {
            startPostponedEnterTransition()
        }.observe(getEventById) { state ->
            state.processingUi(success = { })
        }

    override fun backPress() = let { if (!tryClearSearch()) super.backPress() }

    private fun tryClearSearch() =
        binding.eventInfoRv.getBindingItemByPos<ItemSearchInfoBinding>(1)?.let { bind ->
            bind.searchField.takeIf { it.text?.isBlank() == false }
                ?.let { it.setText(DEFAULT_STRING); true } ?: false
        } ?: false

    private fun EventInfoViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromEventInfo.ToScanner -> navigateTo(
                EventInfoFragmentDirections.fromEventInfoToCamera(it.eventId)
            )
            is AppRouter.FromEventInfo.ToAddGuest -> navigateTo(
                EventInfoFragmentDirections.fromEventInfoToNewGuest(it.eventId, it.eventImageUri)
            )
            is AppRouter.FromApp.ToEmailGuest -> navigateToEmail(
                email = it.emailGuest,
                title = it.event.title,
                message = it.event.description,
                bitmapUri = it.uriQrCode
            )
            is AppRouter.FromApp.ToMap -> navigateToMap(it.url)
        }
    }
}