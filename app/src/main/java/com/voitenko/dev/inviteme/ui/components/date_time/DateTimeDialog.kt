package com.voitenko.dev.inviteme.ui.components.date_time

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.utils.extensions.currentDate
import com.voitenko.dev.inviteme.utils.extensions.currentDateWithYear
import com.voitenko.dev.inviteme.utils.extensions.currentYear
import com.voitenko.dev.inviteme.utils.extensions.showLog
import java.text.SimpleDateFormat
import java.util.*

private const val FORMAT_PATTERN = "dd MMMM"

fun buildBaseDateTimeDialog(
    title: String,
    onPositiveClickText: String,
    onnNegativeClickText: String,
    onPositiveClick: (date: Date?) -> Unit,
    onNegativeClick: (() -> Unit)? = null
): SwitchDateTimeDialogFragment =
    SwitchDateTimeDialogFragment.newInstance(title, onPositiveClickText, onnNegativeClickText)
        .apply {
            setAlertStyle(R.style.Theme_SwitchDateTime)
            startAtCalendarView()
            set24HoursMode(true)
            minimumDateTime = currentDate().time
            maximumDateTime = currentDateWithYear(currentYear().plus(1)).time
            setDefaultDateTime(currentDate().time)
            try {
                simpleDateMonthAndDayFormat = SimpleDateFormat(FORMAT_PATTERN, Locale.getDefault())
            } catch (e: SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException) {
                showLog(e.message.toString())
                FirebaseCrashlytics.getInstance().recordException(e)
            }
            setOnButtonClickListener(object : SwitchDateTimeDialogFragment.OnButtonClickListener {
                override fun onPositiveButtonClick(date: Date?) {
                    onPositiveClick.invoke(date)
                }

                override fun onNegativeButtonClick(date: Date?) {
                    onNegativeClick?.invoke()
                }
            })
        }