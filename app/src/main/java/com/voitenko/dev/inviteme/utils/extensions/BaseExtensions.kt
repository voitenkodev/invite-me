package com.voitenko.dev.inviteme.utils.extensions

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import java.util.*

const val DEFAULT_LONG = -1L
const val DEFAULT_INT = -1
const val DEFAULT_DOUBLE = .0
const val DEFAULT_BOOL = false
const val DEFAULT_FLOAT = -1F
const val DEFAULT_STRING = ""
val DEFAULT_CALENDAR = GregorianCalendar(2000, 0, 0)

// hide keyboard
fun FragmentActivity.hideKeyboardFromScreen() {
    val imm = (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
    this.currentFocus?.apply {
        imm.hideSoftInputFromWindow(this.windowToken, 0)
        clearFocus()
    }
}

// inflate view on adapters for recyclerView
fun ViewGroup.inflateView(@LayoutRes layout: Int): ViewDataBinding {
    return DataBindingUtil.inflate(LayoutInflater.from(this.context), layout, this, false)
}

// show toast
fun Context.showToast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

// show log "showLog"
fun showLog(text: String?) = Log.d("showLog", text ?: "null")

// show toast
fun Context.showToast(res: Int) {
    this.showToast(string(res))
}

// show statusBar
fun FragmentActivity.showStatusBar() =
    this.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

// hide statusBar
fun FragmentActivity.hideStatusBar() = this.window.setFlags(
    WindowManager.LayoutParams.FLAG_FULLSCREEN,
    WindowManager.LayoutParams.FLAG_FULLSCREEN
)

// get string
fun Context.string(@StringRes s: Int) = this.resources.getString(s)

// get dimen
fun Context.dimen(@DimenRes d: Int) = this.resources.getDimension(d)

// get integer
fun Context.integer(i: Int) = this.resources.getInteger(i)

// get drawable
fun Context.drawable(@DrawableRes d: Int) =
    ResourcesCompat.getDrawable(this.resources, d, null)

// get color
fun Context.color(@ColorRes c: Int) = ContextCompat.getColor(this, c)

fun Context.resIdByName(resIdName: String?, resType: String): Int {
    resIdName?.let { return resources.getIdentifier(it, resType, packageName) }
    throw Resources.NotFoundException()
}

fun updateMode(mode: Mode) {
    if (mode == Mode.DARK) AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
}

fun FragmentActivity.updateStatusBar(mode: Mode) {
    if (mode == Mode.DARK) this.clearLightStatusBar()
    else this.setLightStatusBar()
}

// make black text to statusBar (time, battery)
private fun FragmentActivity.setLightStatusBar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags = this.window.decorView.systemUiVisibility
        flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        this.window.decorView.systemUiVisibility = flags
    }
}

// make white text to statusBar (time, battery)
private fun FragmentActivity.clearLightStatusBar() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags = this.window.decorView.systemUiVisibility
        flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        this.window.decorView.systemUiVisibility = flags
        flags = flags xor View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        this.window.decorView.systemUiVisibility = flags
    }
}