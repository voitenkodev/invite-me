package com.voitenko.dev.inviteme.utils.extensions.ui

import android.app.Dialog
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

fun Dialog.blockDragging(defaultState: Int) = setOnShowListener {
    with(it as BottomSheetDialog) {
        val sheetInternal: View? =
            findViewById(com.google.android.material.R.id.design_bottom_sheet)
        BottomSheetBehavior.from(sheetInternal).state = defaultState
        BottomSheetBehavior.from(sheetInternal)
            .setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bs: View, newState: Int) {
                    takeIf { newState == BottomSheetBehavior.STATE_HIDDEN }?.let { dismiss() }
                        ?: let { BottomSheetBehavior.from(sheetInternal).state = defaultState }
                }

                override fun onSlide(p0: View, p1: Float) {
                }

            })
    }
}