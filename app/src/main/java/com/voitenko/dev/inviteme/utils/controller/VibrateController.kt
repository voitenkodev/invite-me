package com.voitenko.dev.inviteme.utils.controller

import android.app.Service
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.fragment.app.FragmentActivity

fun FragmentActivity.trySuccessVibrate() = VibrateController.trySuccessVibrate(getVibrator())

fun FragmentActivity.tryErrorVibrate() = VibrateController.tryErrorVibrate(getVibrator())

private fun FragmentActivity.getVibrator() =
    this.getSystemService(Service.VIBRATOR_SERVICE) as Vibrator

private object VibrateController {

    private const val shortInterval = 300L
    private const val longInterval = shortInterval.times(2)

    fun trySuccessVibrate(mVibrator: Vibrator?) = vibrate(mVibrator, shortInterval)

    fun tryErrorVibrate(mVibrator: Vibrator?) = vibrate(mVibrator, longInterval)

    private fun vibrate(mVibrator: Vibrator?, interval: Long) = mVibrator?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            mVibrator.vibrate(
                VibrationEffect.createOneShot(interval, VibrationEffect.DEFAULT_AMPLITUDE)
            )
        else mVibrator.vibrate(interval) // Deprecated in API 26
    }
}
