package com.voitenko.dev.inviteme.ui.home

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.UniversalItemHolder
import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
import com.voitenko.dev.inviteme.databinding.ItemEventBinding
import com.voitenko.dev.inviteme.utils.extensions.inflateView

class EventPageAdapter(private val vm: HomeViewModel) :
    ListAdapter<HomeEventWithGuests, UniversalItemHolder>(EventDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UniversalItemHolder {
        return UniversalItemHolder(parent.inflateView(R.layout.item_event))
    }

    override fun onBindViewHolder(holder: UniversalItemHolder, position: Int) {
        holder.bind(getItem(holder.adapterPosition), vm, holder.adapterPosition)
        vm.prepareEventLayout(
            (holder.binding as ItemEventBinding).eventPictureIv,
            holder.adapterPosition
        )
    }

    companion object EventDiffUtils : DiffUtil.ItemCallback<HomeEventWithGuests>() {
        override fun areItemsTheSame(
            oldItem: HomeEventWithGuests,
            newItem: HomeEventWithGuests
        ) = oldItem.eventWithGuests.event.eventId == newItem.eventWithGuests.event.eventId

        override fun areContentsTheSame(
            oldItem: HomeEventWithGuests,
            newItem: HomeEventWithGuests
        ) = oldItem.eventWithGuests.guests.size == newItem.eventWithGuests.guests.size &&
                oldItem.isPassed == newItem.isPassed
    }
}