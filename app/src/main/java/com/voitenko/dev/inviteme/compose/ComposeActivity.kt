package com.voitenko.dev.inviteme.compose

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.BrokenImage
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import com.google.accompanist.coil.rememberCoilPainter
import com.voitenko.dev.inviteme.ui.splash.SplashActivity
import com.voitenko.dev.inviteme.utils.extensions.showToast

class ComposeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, true)
        setContent {
            MaterialTheme {
                Scaffold(
                    topBar = { Toolbar() },
                    content = { Body() },
                    bottomBar = {},
                    floatingActionButton = {},
                    contentColor = Color.Black
                )
            }
        }
    }

    @Composable
    fun Body() = LazyColumn(modifier = Modifier.padding(16.dp)) {
        item { ImageHeader() }
        item {
            Text(
                text = "artickle",
                modifier = Modifier.padding(top = 24.dp, start = 24.dp),
                style = TextStyle(
                    color = Color(0xFF959595),
                    fontWeight = FontWeight.Normal,
                    fontSize = 12.sp
                )
            )
        }
        item { PriceView() }
    }

    @Composable
    fun PriceView() = Row(
        modifier = Modifier
            .height(66.dp)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,

        ) {
        Text("155.00.00")
        Text(text = "y/p", modifier = Modifier.weight(1F))
        Button(
            onClick = { showToast("Hello wrold") },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.Green),
            shape = RoundedCornerShape(55, 4, 55, 4)
        ) {
            Text(
                text = "Hello world",
                color = Color.White,
                fontWeight = FontWeight.Bold,
                fontSize = 12.sp
            )
        }

    }

    @Composable
    fun Toolbar() = Column {
        Spacer(modifier = Modifier.height(31.dp))
        TopAppBar(
            title = { Text(text = "Compose example") },
            navigationIcon = {
                IconButton(onClick = {
                    startActivity(Intent(this@ComposeActivity, SplashActivity::class.java))
                }) {
                    Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Back Btn")
                }
            },
            backgroundColor = Color.Transparent,
            contentColor = Color.Gray,
            elevation = 2.dp,
            modifier = Modifier.fillMaxWidth()
        )
    }

    @Composable
    fun ImageHeader() {
        Image(
            modifier = Modifier
                .background(color = Color.Green)
                .height(313.dp)
                .fillMaxWidth(),
            painter = rememberCoilPainter(
                request = "https://cataas.com/cat/gif",
            ), contentDescription = "Image something"
        )
        Row(
            modifier = Modifier
                .height(44.dp)
                .background(MaterialTheme.colors.background)
        ) {
            Text(text = "Back")
            Text(text = "Invite me")
        }
    }
}
