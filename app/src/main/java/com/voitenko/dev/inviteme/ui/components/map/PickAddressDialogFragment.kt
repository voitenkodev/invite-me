package com.voitenko.dev.inviteme.ui.components.map

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.CompleteState
import com.voitenko.dev.inviteme.base.ErrorState
import com.voitenko.dev.inviteme.base.LoadingState
import com.voitenko.dev.inviteme.base.SuccessState
import com.voitenko.dev.inviteme.databinding.DialogMapBinding
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.askPermission
import com.voitenko.dev.inviteme.utils.extensions.checkPermission
import com.voitenko.dev.inviteme.utils.extensions.drawable
import com.voitenko.dev.inviteme.utils.extensions.navigateBack
import com.voitenko.dev.inviteme.utils.extensions.navigateToGps
import com.voitenko.dev.inviteme.utils.extensions.ui.blockDragging
import com.voitenko.dev.inviteme.utils.extensions.ui.cleanable
import com.voitenko.dev.inviteme.utils.extensions.ui.gone
import com.voitenko.dev.inviteme.utils.extensions.ui.hide
import com.voitenko.dev.inviteme.utils.extensions.ui.isInVisible
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.ui.shake
import com.voitenko.dev.inviteme.utils.extensions.ui.show
import org.koin.androidx.viewmodel.ext.android.viewModel

class PickAddressDialogFragment : BaseMapDialogFragment() {

    override val viewModel: PickAddressViewModel by viewModel()

    override fun DialogMapBinding.setupUi(b: Bundle?) {
        viewModel.observeVM()
        viewModel.observeNavigation()
        addressEt.cleanable(context?.drawable(R.drawable.ic_search_clear))
        addActions()
    }

    private fun DialogMapBinding.addActions() {
        addressEt.doOnTextChanged { t, _, _, _ -> viewModel.inputManualAddress(t.toString()) }
        okBtn.setOnClickListener { addressEt.validateAddressField { viewModel.saveAddress() } }
        manualInputAddress.setOnClickListener { manualInputState() }
    }

    private fun PickAddressViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromApp.ToGpsSettings -> this@PickAddressDialogFragment.navigateToGps()
            is AppRouter.FromNewEvent.FromMapDialog.ToNewEvent ->
                this@PickAddressDialogFragment.navigateBack(it.key to it.b)
        }
    }

    private fun PickAddressViewModel.observeVM() = viewLifecycleOwner
        .observe(modeStatusObserve) { state ->
            state.processingUi(
                success = { mode ->
                    binding.mapView.setupMap(mode) { latLng, m, s ->
                        addMapListener(latLng, m, s)
                    }
                }
            )
        }.observe(checkPermissionEvent) {
            context?.askPermission(arrayOf(ACCESS_FINE_LOCATION), 1341)
        }.observe(lastMarkerState) { state ->
            state.processingUi(
                success = { binding.addressEt.setText(it) },
                loading = { binding.progressAddress.show() },
                complete = { binding.progressAddress.hide() }
            )
        }

    private fun addMapListener(latLng: LatLng, marker: SymbolManager, style: Style): Boolean {
        binding.addressEt.setText(DEFAULT_STRING)
        return if (marker.createMarker(latLng, style)) {
            viewModel.findAddress(latLng)
            true
        } else false
    }

    private fun EditText.validateAddressField(success: () -> Unit) {
        if (this.text.isNullOrBlank()) shake {}
        else success.invoke()
    }

    private fun safeStartMap() = context?.checkPermission(
        arrayOf(ACCESS_FINE_LOCATION),
        isGranted = { if (requireContext().isGpsNotEnabled()) showGpsState() else showMapState() },
        isDenied = { showPermissionState() })


    private fun showPermissionState() = binding.apply {
        permissionBtn.setOnClickListener { viewModel.checkPermission() }
        mapView.hide()
        addressEt.hide()
        okBtn.gone()
        permissionTv.setText(R.string.location_permission_map_txt)
        permissionBtn.setText(R.string.location_permission_map_btn)
        if (permissionLayout.isInVisible()) permissionLayout.show()
    }

    private fun showGpsState() = binding.apply {
        permissionBtn.setOnClickListener { viewModel.goToGpsSettings() }
        mapView.hide()
        addressEt.hide()
        okBtn.gone()
        permissionTv.setText(R.string.gps_permission_map_txt)
        permissionBtn.setText(R.string.gps_permission_map_btn)
        if (permissionLayout.isInVisible()) permissionLayout.show()
    }

    private fun manualInputState() = binding.apply {
        mapView.hide()
        addressEt.show()
        okBtn.show()
        if (permissionLayout.isInVisible()) permissionLayout.show()
    }

    override fun onResume() {
        super.onResume()
        safeStartMap()
    }

    private fun showMapState() {
        binding.permissionLayout.gone()
//        binding.mapView.show()
        binding.addressEt.show()
        binding.okBtn.show()
        viewModel.checkDarkMode()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        dialog?.blockDragging(BottomSheetBehavior.STATE_EXPANDED)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    companion object {
        const val MAP_RESULT = "MAP_RESULT"
        const val MAP_ADDRESS_RESULT = "MAP_ADDRESS_RESULT"
    }
}