package com.voitenko.dev.inviteme.ui.new_guest

import android.app.Application
import android.graphics.Bitmap
import android.net.Uri
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.StateErrorApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.models.models.generateTicket
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestByEmailUc
import com.voitenko.dev.inviteme.core.domain.guest_uc.SetGuestUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.GetNightDayModeUc
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_LONG
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.generateQrCode
import com.voitenko.dev.inviteme.utils.extensions.isNotValidEmail
import com.voitenko.dev.inviteme.utils.extensions.isValidEmail
import com.voitenko.dev.inviteme.utils.extensions.saveToFile
import com.voitenko.dev.inviteme.utils.extensions.showLog
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class NewGuestViewModel(private val applicationContext: Application, private val idEvent: Long) :
    BaseViewModel() {

    private val setGuestUc by inject<SetGuestUc>()
    private val getGuestByEmailUc by inject<GetGuestByEmailUc>()
    private val dayNightModeUc by inject<GetNightDayModeUc>()

    private var idGuest: Long = DEFAULT_LONG
    private var nameGuest = DEFAULT_STRING
    private var emailGuest = DEFAULT_STRING
    private var phoneGuest: String? = null
    private var imageGuest: Uri? = null

    val qrCodeObservable = singleResultFlow<Uri>()
    val initGuestToUiEvent = singleResultFlow<GuestApp>()
    val saveGuestEvent = singleResultFlow<Long>()
    val changeImage = singleResultFlow<Unit>()
    val startEndAnimationScreenEvent = shared<Unit>()

    init {
        initGuestToUiEvent.processing { flowOf(GuestApp.EMPTY_GUEST) }
    }

    fun findOrSaveGuest() = takeIf { idGuest == DEFAULT_LONG }
        ?.let { findExistGuestByEmail() } ?: saveNewGuest()


    private fun findExistGuestByEmail() = initGuestToUiEvent.processing {
        flow {
            val guestEmail = when {
                emailGuest.isNotValidEmail() -> throw StateErrorApp(R.string.error_input_email)
                emailGuest.isBlank() -> throw StateErrorApp(R.string.error_input_guest_email)
                else -> emailGuest
            }
            emit(guestEmail)
        }.flatMapConcat { emailGuest ->
            getGuestByEmailUc.invoke(emailGuest).map {
                return@map it?.let { guest -> initOldGuest(guest) } ?: initNewGuest()
            }
        }
    }

    private fun saveNewGuest() = saveGuestEvent.processing {
        flow {
            val guestApp = GuestApp(
                idGuest,
                nameGuest,
                emailGuest,
                phoneGuest,
                imageGuest.toString()
            )
            when {
                guestApp.guestName.isBlank() ->
                    throw StateErrorApp(R.string.error_input_guest_name)
                guestApp.email.isBlank() ->
                    throw StateErrorApp(R.string.error_input_guest_email)
                guestApp.email.isNotBlank() && !guestApp.email.isValidEmail() ->
                    throw StateErrorApp(R.string.error_input_email)
            }
            emit(guestApp)
        }.flatMapConcat { guestApp ->
            setGuestUc.invoke(guestApp, idEvent)
        }
    }

    private fun initOldGuest(guest: GuestApp): GuestApp {
        idGuest = guest.guestId
        nameGuest = guest.guestName
        emailGuest = guest.email
        phoneGuest = guest.phone
        imageGuest = Uri.parse(guest.imageLocalPath)
        createQrCode()
        return GuestApp(idGuest, nameGuest, emailGuest, phoneGuest, imageGuest.toString())
    }

    private fun initNewGuest(): GuestApp {
        idGuest = System.currentTimeMillis()
        createQrCode()
        return GuestApp(idGuest, nameGuest, emailGuest, phoneGuest, imageGuest.toString())
    }

    private fun createQrCode() = qrCodeObservable.processing {
        dayNightModeUc.invoke().map { mode ->
            val contentIds =
                TicketApp(idEvent, idGuest).generateTicket()
            showLog("createQrCode content id -> $contentIds")
            val qrCodeBitmap = applicationContext.generateQrCode(contentIds, mode)
            val savedUri = qrCodeBitmap.saveToFile(contentIds, applicationContext)
            savedUri
        }
    }

    fun changeNameGuest(s: CharSequence?) = let { nameGuest = s.toString() }

    fun changePhoneGuest(s: CharSequence?) = let { phoneGuest = s.toString() }

    fun changeEmailGuest(s: CharSequence?) = let { emailGuest = s.toString() }

    fun bitmapError() =
        changeImage.processing { flow { throw StateErrorApp(R.string.error_invalid_bitmap) } }

    fun navigateToGallery() = let { navigatorEvent.call(AppRouter.FromApp.ToGallery) }

    fun navigateBack() = navigatorEvent.call(AppRouter.FromApp.ToBack)

    fun startEndAnimationScreen() = startEndAnimationScreenEvent.call(Unit)

    fun changeImageGuest(bitmap: Bitmap) = changeImage.processing {
        flow {
            val uri = bitmap.saveToFile(idGuest.toString(), applicationContext)
            imageGuest = uri
            emit(Unit)
        }
    }
}