package com.voitenko.dev.inviteme.utils.extensions

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.provider.Settings.ACTION_SECURITY_SETTINGS
import android.transition.TransitionInflater
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.annotation.StringRes
import androidx.annotation.TransitionRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavDirections
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController


// get navigation to other fragment
fun Fragment.navigateTo(
    navDirection: NavDirections,
    extras: FragmentNavigator.Extras? = null
) = findNavController().currentDestination?.let { cd ->
    if (cd is FragmentNavigator.Destination && cd.className == this.javaClass.name)
        extras?.let { findNavController().navigate(navDirection, extras) }
            ?: findNavController().navigate(navDirection)
}

// add shared transition
fun View.withSharedTransition(id: String): FragmentNavigator.Extras {
    transitionName = id
    return FragmentNavigatorExtras(this to id)
}

// add shared transition
fun View.withSharedTransition(@StringRes id: Int): FragmentNavigator.Extras =
    this.withSharedTransition(context.string(id))

// get navigation to back
fun Fragment.navigateBack(backResult: Pair<String, Bundle>? = null) {
    backResult?.let { setFragmentResult(it.first, it.second) }
    findNavController().popBackStack()
}

// get navigation to gallery
fun ActivityResultLauncher<String>.navigateToGallery() = this.launch("image/*")

// handle back press from fragment
fun FragmentActivity.addBackPressFragment(
    action: (() -> Unit)? = null, fragmentLifeCycle: LifecycleOwner
) {
    val callback: OnBackPressedCallback =
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                action?.let { it() }
            }
        }
    this.onBackPressedDispatcher.addCallback(fragmentLifeCycle, callback)
}

// navigate to email with qrCode
fun Fragment.navigateToEmail(
    email: String,
    title: String,
    message: String,
    bitmapUri: Uri
) {
    val emailIntent = Intent(Intent.ACTION_VIEW)
    emailIntent.data = Uri.parse("mailto:?subject=$title&body=$message&to=$email")
    emailIntent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
    startActivity(emailIntent)
}

// gps settings
fun Fragment.navigateToGps() {
    val intent = Intent(ACTION_LOCATION_SOURCE_SETTINGS)
    startActivity(intent)
}

// google maps
fun Fragment.navigateToMap(url: String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    intent.setPackage("com.google.android.apps.maps")
    startActivity(intent)
}

// set !sharedElementEnterTransition! You can use Default anim transition resource like: => (android.R.transition.move)
fun Fragment.inflateNavSharedTransition(@TransitionRes resource: Int) {
    this.sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(resource)
}