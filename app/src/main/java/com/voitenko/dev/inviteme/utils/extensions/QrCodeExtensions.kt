package com.voitenko.dev.inviteme.utils.extensions

import android.content.Context
import android.graphics.Bitmap
import androidx.core.content.ContextCompat
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.QRCodeWriter
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import java.util.EnumMap

fun Context.generateQrCode(content: String, mode: Mode): Bitmap {
    val firstColor =
        ContextCompat.getColor(this, if (mode == Mode.DARK) R.color.white else R.color.black)
    val secondColor =
        ContextCompat.getColor(this, if (mode == Mode.DARK) R.color.black else R.color.white)

    val writer = QRCodeWriter()
    val hints = EnumMap<EncodeHintType, Any>(EncodeHintType::class.java)
    hints[EncodeHintType.CHARACTER_SET] = "UTF-8"
    hints[EncodeHintType.MARGIN] = 1
    val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 200, 200, hints)
    val width = bitMatrix.width
    val height = bitMatrix.height
    val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
    for (x in 0 until width) {
        for (y in 0 until height) {
            bmp.setPixel(x, y, if (bitMatrix.get(x, y)) firstColor else secondColor)
        }
    }
    showLog("content qrCode -> $content")
    return bmp
}