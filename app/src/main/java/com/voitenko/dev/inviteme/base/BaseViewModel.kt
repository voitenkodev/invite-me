package com.voitenko.dev.inviteme.base

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.voitenko.dev.inviteme.core.data.datasource.remote.address_api.ExpectedNetworkThrowable
import com.voitenko.dev.inviteme.utils.controller.observe
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent

abstract class BaseViewModel : ViewModel(), KoinComponent {

    // Flow Utils ----------------------------------------------------------------------------------

    protected fun <T> stateResultFlow(): SharedFlow<StateResult<T>> =
        MutableSharedFlow(2, 2, BufferOverflow.DROP_OLDEST)

    protected fun <T> singleResultFlow(): SharedFlow<StateResult<T>> =
        MutableSharedFlow(0, 2, BufferOverflow.DROP_OLDEST)

    protected fun <T> shared(): SharedFlow<T> = MutableSharedFlow(0, 1)

    protected fun <T> Flow<T>.call(value: T) =
        viewModelScope.launch { this@call.suspendCall(value) }

    private suspend fun <T> Flow<T>.suspendCall(value: T) = when (this@suspendCall) {
        is MutableStateFlow<T> -> this@suspendCall.emit(value)
        is MutableSharedFlow<T> -> this@suspendCall.emit(value)
        else -> Unit
    }

    // NAVIGATION State Observing ------------------------------------------------------------------
    protected val navigatorEvent = shared<AppRouter>()
    fun observeNavigation(owner: LifecycleOwner, observer: (AppRouter) -> Unit) =
        navigatorEvent.observe(owner, { observer.invoke(it) })

    // Flow processing -----------------------------------------------------------------------------
    protected fun <T> Flow<StateResult<T>>.processing(
        progress: Boolean = true, method: () -> Flow<T>
    ) = method.invoke()
        .onStart {
            if (progress) this@processing.suspendCall(StateResult.loading(this@BaseViewModel.viewModelScope))
        }.catch {
            progressTrashHold()
            this@processing.suspendCall(StateResult.error(it.parseError()))
            this@processing.suspendCall(StateResult.complete())
        }.onEach {
            progressTrashHold()
            this@processing.suspendCall(StateResult.success(it))
            this@processing.suspendCall(StateResult.complete())
        }.launchIn(viewModelScope)

    // Internal add minimal time of request
    private val minimalRequestTime = 450L
    private suspend fun progressTrashHold() = delay(minimalRequestTime)

    // Internal parsing type errors
    private fun Throwable.parseError(): StateErrorApp = when (this) {
        is StateErrorApp -> this
        is ExpectedNetworkThrowable -> StateErrorApp(outMessage = serverMessage)
        else -> StateErrorApp(androidThrowMessage = this.message)
    }
}