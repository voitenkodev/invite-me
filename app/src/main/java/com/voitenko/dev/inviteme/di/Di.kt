package com.voitenko.dev.inviteme.di

import androidx.constraintlayout.widget.ConstraintLayout
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.voitenko.dev.inviteme.MainViewModel
import com.voitenko.dev.inviteme.ui.scanner.ScannerFragment
import com.voitenko.dev.inviteme.ui.scanner.ScannerViewModel
import com.voitenko.dev.inviteme.ui.event_info.EventInfoAdapter
import com.voitenko.dev.inviteme.ui.event_info.EventInfoFragment
import com.voitenko.dev.inviteme.ui.event_info.EventInfoViewModel
import com.voitenko.dev.inviteme.ui.guest_info.GuestInfoAdapter
import com.voitenko.dev.inviteme.ui.guest_info.GuestInfoFragment
import com.voitenko.dev.inviteme.ui.guest_info.GuestInfoViewModel
import com.voitenko.dev.inviteme.ui.home.EventPageAdapter
import com.voitenko.dev.inviteme.ui.home.HomeFragment
import com.voitenko.dev.inviteme.ui.home.HomeViewModel
import com.voitenko.dev.inviteme.ui.new_event.NewEventViewModel
import com.voitenko.dev.inviteme.ui.new_guest.NewGuestFragment
import com.voitenko.dev.inviteme.ui.new_guest.NewGuestViewModel
import com.voitenko.dev.inviteme.ui.search.SearchAdapter
import com.voitenko.dev.inviteme.ui.search.SearchFragment
import com.voitenko.dev.inviteme.ui.search.SearchViewModel
import com.voitenko.dev.inviteme.ui.splash.SplashViewModel
import com.voitenko.dev.inviteme.ui.components.loading.ProgressBottomDialog
import com.voitenko.dev.inviteme.ui.components.loading.ProgressViewModel
import com.voitenko.dev.inviteme.ui.components.map.PickAddressDialogFragment
import com.voitenko.dev.inviteme.ui.components.map.PickAddressViewModel
import com.voitenko.dev.inviteme.ui.new_event.NewEventFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@ExperimentalStdlibApi
@ExperimentalCoroutinesApi
fun getModules() = listOf(
    splashModule,
    mainActivityModule,
    eventModule,
    homeModule,
    searchModule,
    guestModule,
    eventInfoModule,
    guestInfoModule,
    cameraModule,
    progressModule,
    mapModule,
)

val mapModule = module {
    viewModel { PickAddressViewModel() }
    scope<PickAddressDialogFragment> {}
}

val homeModule = module {
    viewModel { HomeViewModel() }
    scope<HomeFragment> {
        scoped { (vm: HomeViewModel) -> EventPageAdapter(vm) }
    }
}

val progressModule = module {
    scope<ProgressBottomDialog> {}
    viewModel { ProgressViewModel() }
}

val mainActivityModule = module { viewModel { MainViewModel() } }

val eventModule = module {
    viewModel { NewEventViewModel(androidApplication()) }
    scope<NewEventFragment> { }
}

val searchModule = module {
    viewModel { SearchViewModel() }
    scope<SearchFragment> { scoped { (vm: SearchViewModel) -> SearchAdapter(vm) } }
}

val splashModule = module {
    viewModel { SplashViewModel() }
}

val guestModule = module {
    viewModel { (eventId: Long) -> NewGuestViewModel(androidApplication(), eventId) }
    scope<NewGuestFragment> {}
}

@ExperimentalStdlibApi
val eventInfoModule = module {
    viewModel { (eventId: Long) -> EventInfoViewModel(androidApplication(), eventId) }
    scope<EventInfoFragment> { scoped { (vm: EventInfoViewModel) -> EventInfoAdapter(vm) } }
}

@ExperimentalStdlibApi
val guestInfoModule = module {
    viewModel { (guestId: Long) -> GuestInfoViewModel(guestId) }
    scope<GuestInfoFragment> { scoped { (vm: GuestInfoViewModel) -> GuestInfoAdapter(vm) } }
}

val cameraModule = module {
    viewModel { (eventId: Long) -> ScannerViewModel(eventId) }
    scope<ScannerFragment> {
        scoped { (view: CodeScannerView) -> CodeScanner(androidContext(), view) }
        scoped { (view: ConstraintLayout) -> BottomSheetBehavior.from(view) }
    }
}