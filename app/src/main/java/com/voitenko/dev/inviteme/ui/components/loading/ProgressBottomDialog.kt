package com.voitenko.dev.inviteme.ui.components.loading

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.utils.extensions.string

class ProgressBottomDialog : BottomSheetDialogFragment() {

    override fun getTheme() = R.style.bottom_dialog_fragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = com.voitenko.dev.inviteme.databinding.DialogProgressBinding.inflate(inflater).apply {
        val args by navArgs<ProgressBottomDialogArgs>()
        message.text = context?.string(args.title)
    }.root
}

class ProgressViewModel : BaseViewModel()

/*
class ProgressBottomDialog :
    BaseBottomDialogFragment<DialogProgressBinding>(R.layout.dialog_progress) {

    override fun getTheme() = R.style.bottom_dialog_fragment

    override val viewModel: ProgressViewModel by viewModel()

    override fun DialogProgressBinding.setupUi(b: Bundle?) {
        val args by navArgs<ProgressBottomDialogArgs>()
        binding.message.text = context?.string(args.title)
    }

}
*/