package com.voitenko.dev.inviteme.utils.extensions.ui

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

inline fun <reified T> LifecycleOwner.observe(
    f: Flow<T>, crossinline collector: suspend (T) -> Unit
) = this.apply {
    lifecycleScope.launch {
        f.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .collect { collector.invoke(it) }
    }
}