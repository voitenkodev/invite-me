package com.voitenko.dev.inviteme.base

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.voitenko.dev.inviteme.MainGraphDirections
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.utils.extensions.addBackPressFragment
import com.voitenko.dev.inviteme.utils.extensions.hideKeyboardFromScreen
import com.voitenko.dev.inviteme.utils.extensions.navigateBack
import com.voitenko.dev.inviteme.utils.extensions.navigateTo
import com.voitenko.dev.inviteme.utils.extensions.showLog
import com.voitenko.dev.inviteme.utils.extensions.string
import com.voitenko.dev.inviteme.utils.extensions.ui.getProblemSnackBar

interface FragmentContract {

    val f: Fragment

    // STATE ---------------------------------------------------------------------------------------

    fun <T> StateResult<T>.processingUi(
        success: (T) -> Unit,
        error: ((StateErrorApp) -> Unit)? = null,
        complete: (() -> Unit)? = null,
        loading: (() -> Unit)? = null
    ) = when (this) {
        is SuccessState<T> -> success.invoke(this.value)
        is ErrorState<T> -> this.t?.let { error?.invoke(it) ?: observeError(it) }
        is LoadingState<T> -> loading?.invoke() ?: observeLoading(View.VISIBLE)
        is CompleteState<T> -> complete?.invoke() ?: observeLoading(View.GONE)
    }.also { showLog(this::class.java.name) }

    // Loading -------------------------------------------------------------------------------------

    fun observeLoading(visibility: Int) = f.viewLifecycleOwner.lifecycleScope.launchWhenStarted {
        when (visibility) {
            View.VISIBLE -> takeIf { f.findNavController().currentDestination?.id != R.id.progressDialog }
                ?.let { f.navigateTo(MainGraphDirections.toProgress(R.string.progress_dialog_title)) }
            View.GONE, View.INVISIBLE -> takeIf { f.findNavController().currentDestination?.id == R.id.progressDialog }
                ?.let { f.navigateBack() }
        }
    }

    // ERROR ---------------------------------------------------------------------------------------

    fun observeError(errorApp: StateErrorApp) {
        val errorMessage = errorApp.localizedMessage?.let { f.requireContext().string(it) }
            ?: errorApp.outMessage
            ?: f.requireContext().string(R.string.base_error)
        f.activity?.getProblemSnackBar(errorMessage)?.show()
        errorApp.androidThrowMessage?.let {
            FirebaseCrashlytics.getInstance().recordException(errorApp)
            showLog("$errorMessage :: $errorApp")
        }
    }

    // FIREBASE ------------------------------------------------------------------------------------

    @SuppressLint("MissingPermission")
    fun Context.initAnalytics() = bundleOf(
        FirebaseAnalytics.Param.SCREEN_NAME to f.javaClass.simpleName,
        FirebaseAnalytics.Param.SCREEN_CLASS to f.javaClass.simpleName
    ).let {
        FirebaseAnalytics.getInstance(f.requireContext())
            .logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, it)
    }

    // Keyboard control ----------------------------------------------------------------------------

    fun hideKeyboard() = f.requireActivity().hideKeyboardFromScreen()

    // Navigation ----------------------------------------------------------------------------------

    fun backPress() = run { f.navigateBack() }

    fun FragmentActivity.handleBackPress() =
        this.addBackPressFragment(::backPress, f.viewLifecycleOwner)
}