package com.voitenko.dev.inviteme.ui.guest_info

import android.os.Bundle
import androidx.navigation.fragment.navArgs
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.base.getBindingItemByPos
import com.voitenko.dev.inviteme.databinding.FragmentInfoGuestBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchInfoBinding
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.inflateNavSharedTransition
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

@ExperimentalStdlibApi
class GuestInfoFragment : BaseFragment<FragmentInfoGuestBinding>(R.layout.fragment_info_guest) {

    private val adapter: GuestInfoAdapter by inject { parametersOf(viewModel) }
    override val viewModel: GuestInfoViewModel by viewModel {
        val args by navArgs<GuestInfoFragmentArgs>()
        parametersOf(args.guestId)
    }

    override fun FragmentInfoGuestBinding.setupUi(b: Bundle?) {
        this@GuestInfoFragment.inflateNavSharedTransition(R.transition.custom_transition)
        postponeEnterTransition()
        viewModel.observeVM()
        viewModel.observeNavigation()
        viewModel.loadGuestInfo()
        guestInfoRv.adapter = adapter
    }

    override fun FragmentInfoGuestBinding.releaseUi() {
        guestInfoRv.adapter = null
    }

    override fun backPress() {
        if (!clearSearch()) super.backPress()
    }

    private fun clearSearch() =
        binding.guestInfoRv.getBindingItemByPos<ItemSearchInfoBinding>(1)?.let { bind ->
            bind.searchField.takeIf { it.text?.isBlank() == false }
                ?.let { it.setText(DEFAULT_STRING); true } ?: false
        } ?: false

    private fun GuestInfoViewModel.observeVM() = viewLifecycleOwner
        .observe(guestInfo) { state ->
            state.processingUi(success = { adapter.submitList(it) })
        }.observe(prepareInfoGuestLayout) {
            startPostponedEnterTransition()
        }

    private fun GuestInfoViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {}
}