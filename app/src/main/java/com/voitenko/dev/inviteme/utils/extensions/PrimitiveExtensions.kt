package com.voitenko.dev.inviteme.utils.extensions

import android.content.res.Resources
import kotlin.math.floor

fun Int.pruneToString() = when (this) {
    in 1000..1000000 -> "${(this / 1000)}K"
    in 1000000..1000000000 -> "${(this / 1000000)}M"
    else -> "$this"
}

// tracking email to '@' and '.'
fun CharSequence.isNotValidEmail() = !isValidEmail()

// tracking email to '@' and '.'
fun CharSequence.isValidEmail() =
    android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

// pixels to dp
fun Int.toDp() = (this / Resources.getSystem().displayMetrics.density).toInt()

// dp to pixels
fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density)

// cute by @Example -> 16:9
fun calcByRatio(originWidth: Int, originHeight: Int, ratio: String): Pair<Float, Float> {
    var newWidth: Float
    var newHeight: Float

    ratio.split(":").let { currentRatio ->
        val width = currentRatio[0].toFloat()
        val height = currentRatio[1].toFloat()

        if (originWidth >= originHeight && originWidth.toFloat() / originHeight.toFloat() > width / height) {
            newWidth = width / height * originHeight
            newHeight = originHeight.toFloat()
        } else {
            newWidth = originWidth.toFloat()
            newHeight = originWidth * height / width
        }
    }
    return floor(newWidth.round(2)) to floor(newHeight.round(2))
}

fun Float.round(scale: Int): Float {
    val s = this.toString()
    val countOfSymbolsAfterPoint = s.substring(s.indexOf('.') + 1, s.length)

    if(scale > countOfSymbolsAfterPoint.length) return this
    if (scale == 0) return this.toInt().toFloat()
    if (scale < 0) throw Throwable("scale should be 0 or more")

    var pow = 10
    for (i in 1 until scale) pow *= 10
    val tmp = this * pow
    return ((if (tmp - tmp.toInt() >= 0.5f) tmp + 1 else tmp).toInt()).toFloat() / pow
}