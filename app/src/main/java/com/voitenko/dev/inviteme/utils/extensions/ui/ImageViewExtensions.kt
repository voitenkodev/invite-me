package com.voitenko.dev.inviteme.utils.extensions.ui

import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import coil.Coil
import coil.request.ImageRequest
import coil.transform.BlurTransformation
import coil.transform.CircleCropTransformation
import com.voitenko.dev.inviteme.utils.extensions.drawable
import kotlin.math.round

private const val imageSize = 500
private const val crossDuration = 500

fun AppCompatImageView.startAnimatedVector(@DrawableRes anim: Int) {
    val animDrawable = AnimatedVectorDrawableCompat.create(this.context, anim)
    this.setImageDrawable(animDrawable)
    animDrawable?.start()
}

fun AppCompatImageView.loadRoundedImage(uri: Uri?, callback: ((Drawable) -> Unit)?) =
    loadRoundedImage(uri, callback, null)

fun AppCompatImageView.loadImage(uri: Uri?, callback: ((Drawable) -> Unit)?) =
    loadImage(uri, callback, null)

fun AppCompatImageView.loadRoundedImage(
    url: Uri?,
    callback: ((Drawable) -> Unit)?,
    @DrawableRes placeHolder: Int? = null
) {
    addPlaceHolder(placeHolder)
    Coil.imageLoader(context).enqueue(ImageRequest.Builder(context).data(url).apply {
        transformations(CircleCropTransformation())
        addCrossFade()
        size(imageSize, imageSize)
        target { drawable ->
            this@loadRoundedImage.scaleType = ImageView.ScaleType.CENTER_CROP
            this@loadRoundedImage.setImageDrawable(drawable)
            callback?.invoke(drawable)
        }
    }.build())
}

fun AppCompatImageView.loadImage(
    uri: Uri?,
    callback: ((Drawable) -> Unit)?,
    @DrawableRes placeHolder: Int? = null
) {
    addPlaceHolder(placeHolder)
    Coil.imageLoader(context).enqueue(ImageRequest.Builder(context).data(uri).apply {
        addCrossFade()
        size(imageSize, imageSize)
        target { drawable ->
            this@loadImage.scaleType = ImageView.ScaleType.CENTER_CROP
            this@loadImage.setImageDrawable(drawable)
            callback?.invoke(drawable)
        }
    }.build())
}

fun AppCompatImageView.loadBlurImage(
    uri: Uri?,
    callback: ((Drawable) -> Unit)?,
    @DrawableRes placeHolder: Int? = null
) {
    addPlaceHolder(placeHolder)
    Coil.imageLoader(context).enqueue(ImageRequest.Builder(context).data(uri).apply {
        addCrossFade()
        size(imageSize, imageSize)
        transformations(BlurTransformation(this@loadBlurImage.context, 10f))
        target { drawable ->
            this@loadBlurImage.scaleType = ImageView.ScaleType.CENTER_CROP
            this@loadBlurImage.setImageDrawable(drawable)
            callback?.invoke(drawable)
        }
    }.build())
}

fun ImageView.addMatrixScaleStart(heightOfImage: Float) {
    this.scaleType = ImageView.ScaleType.MATRIX
    this.imageMatrix = Matrix().apply {
        val dHeight = this@addMatrixScaleStart.drawable.intrinsicHeight
        val insidePadding = (heightOfImage - dHeight) * 0.5f
        this.setScale(1f, 1f)
        this.setTranslate(round(insidePadding), round(insidePadding))
    }
}

private fun ImageRequest.Builder.addCrossFade() {
    crossfade(true)
    crossfade(crossDuration)
}

private fun AppCompatImageView.addPlaceHolder(@DrawableRes placeHolder: Int?) {
    placeHolder?.let { ph ->
        this@addPlaceHolder.scaleType = ImageView.ScaleType.CENTER
        this@addPlaceHolder.setImageDrawable(context.drawable(ph))
    }
}