package com.voitenko.dev.inviteme.ui.scanner

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ScanMode
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.databinding.FragmentScannerBinding
import com.voitenko.dev.inviteme.utils.controller.addStateController
import com.voitenko.dev.inviteme.utils.controller.tryErrorVibrate
import com.voitenko.dev.inviteme.utils.controller.trySuccessVibrate
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.askPermission
import com.voitenko.dev.inviteme.utils.extensions.checkPermission
import com.voitenko.dev.inviteme.utils.extensions.navigateBack
import com.voitenko.dev.inviteme.utils.extensions.showLog
import com.voitenko.dev.inviteme.utils.extensions.ui.alphaTo
import com.voitenko.dev.inviteme.utils.extensions.ui.gone
import com.voitenko.dev.inviteme.utils.extensions.ui.isInVisible
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.ui.show
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ScannerFragment : BaseFragment<FragmentScannerBinding>(R.layout.fragment_scanner) {

    private val codeScanner: CodeScanner by inject { parametersOf(binding.scannerView) }
    private val bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout> by inject {
        parametersOf(binding.bottomSheet.rootBottomSheet)
    }
    override val viewModel by viewModel<ScannerViewModel> {
        val args by navArgs<ScannerFragmentArgs>(); parametersOf(args.eventId)
    }

    override fun FragmentScannerBinding.setupUi(b: Bundle?) {
        viewModel.observeVM()
        viewModel.observeNavigation()
        setupBottomSheet()
        addActions()
    }

    private fun FragmentScannerBinding.addActions() {
        permissionBtn.setOnClickListener { viewModel.checkPermission() }
    }

    private fun setupBottomSheet() {
        binding.bottomSheet.item = GuestApp.EMPTY_GUEST
        binding.bottomSheet.notifyChange()
        bottomSheetBehavior.apply {
            bottomSheetBehavior.isHideable = true
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            addStateController(lifecycle).whenHidden { viewModel.resetScanner() }
        }
    }

    private fun setupScanner() = codeScanner.apply {
        camera = CodeScanner.CAMERA_BACK
        formats = CodeScanner.ALL_FORMATS
        autoFocusMode = AutoFocusMode.SAFE
        scanMode = ScanMode.SINGLE
        isAutoFocusEnabled = true
        isFlashEnabled = false
        decodeCallback = DecodeCallback { result ->
            stopPreview()
            result.text?.let { txt ->
                viewModel.findGuestOnEvent(txt)
                showLog("content qrCode -> $txt")
            }
        }
    }

    private fun startPreview() {
        setupScanner()
        codeScanner.startPreview()
    }

    private fun ScannerViewModel.observeNavigation() = this.observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromApp.ToBack -> this@ScannerFragment.navigateBack()
        }
    }

    private fun ScannerViewModel.observeVM() = viewLifecycleOwner
        .observe(currentGuestEvent) { state ->
            state.processingUi(success = { findGuestViewState(it) })
        }.observe(setStatusGuestOnEvent) { state ->
            state.processingUi(success = { })
        }.observe(resetScannerEvent) {
            binding.bottomSheet.statusGuestTv.text = DEFAULT_STRING
            startPreview()
        }.observe(currentStatusEvent) {
            mapStatusGuest(it)
        }.observe(checkPermissionEvent) {
            requireContext().askPermission(arrayOf(android.Manifest.permission.CAMERA), 1341)
        }.observe(closeScreenEvent) {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_HIDDEN)
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            else lifecycleScope.launch {
                forceStopCamera(); viewModel.navigateBack()
            }
        }

    private fun findGuestViewState(guest: GuestApp) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        binding.bottomSheet.item = guest
        binding.bottomSheet.notifyChange()
    }

    private fun mapStatusGuest(@StringRes status: Int) = binding.bottomSheet.statusGuestTv.apply {
        text = getString(status)
        when (status) {
            R.string.guest_status_already_arrived -> {
                setBackgroundResource(R.color.warning_color); requireActivity().tryErrorVibrate()
            }
            R.string.guest_status_invited -> {
                setBackgroundResource(R.color.great_color); requireActivity().trySuccessVibrate()
            }
            else -> {
                setBackgroundResource(R.color.error_color); requireActivity().tryErrorVibrate()
            }
        }
    }

    override fun backPress() {
        viewModel.closeScreen()
    }

    override fun onResume() {
        super.onResume()
        safeStartCamera()
    }

    override fun onPause() {
        super.onPause()
        codeScanner.releaseResources()
    }

    private suspend fun forceStopCamera() {
        codeScanner.stopPreview()
        codeScanner.releaseResources()
        binding.scannerView.alphaTo(0F)
    }

    private fun safeStartCamera() = context?.checkPermission(
        arrayOf(android.Manifest.permission.CAMERA),
        isGranted = {
            binding.permissionLayout.gone()
            binding.scannerView.show()
            binding.scannerView.alphaTo(1F) {}
            startPreview()
        }, isDenied = {
            binding.scannerView.gone()
            if (binding.permissionLayout.isInVisible())
                binding.permissionLayout.show()
        })
}
