package com.voitenko.dev.inviteme.utils.controller

import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun BottomSheetBehavior<*>.addStateController(lifecycle: Lifecycle) =
    BottomSheetBehaviorStateController(this, lifecycle)

class BottomSheetBehaviorStateController(
    private val behavior: BottomSheetBehavior<*>,
    lifecycle: Lifecycle
) : LifecycleObserver {

    private val actions = HashMap<Int, () -> Unit>()

    val listener = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            when (newState) {
                BottomSheetBehavior.STATE_HIDDEN -> actions[BottomSheetBehavior.STATE_HIDDEN]?.invoke()
                BottomSheetBehavior.STATE_EXPANDED -> actions[BottomSheetBehavior.STATE_EXPANDED]?.invoke()
                BottomSheetBehavior.STATE_COLLAPSED -> actions[BottomSheetBehavior.STATE_COLLAPSED]?.invoke()
                BottomSheetBehavior.STATE_DRAGGING -> actions[BottomSheetBehavior.STATE_DRAGGING]?.invoke()
                BottomSheetBehavior.STATE_SETTLING -> actions[BottomSheetBehavior.STATE_SETTLING]?.invoke()
                BottomSheetBehavior.STATE_HALF_EXPANDED -> actions[BottomSheetBehavior.STATE_HALF_EXPANDED]?.invoke()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    private fun onAnyEvent(owner: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_START -> behavior.setBottomSheetCallback(listener)
            Lifecycle.Event.ON_STOP -> behavior.setBottomSheetCallback(null)
            else -> Unit
        }
    }

    fun whenHidden(action: () -> Unit) = apply {
        actions[BottomSheetBehavior.STATE_HIDDEN] = action
    }

    fun whenExpanded(action: () -> Unit) = apply {
        actions[BottomSheetBehavior.STATE_EXPANDED] = action
    }

    fun whenCollapsed(action: () -> Unit) = apply {
        actions[BottomSheetBehavior.STATE_COLLAPSED] = action
    }

    fun whenDragging(action: () -> Unit) = apply {
        actions[BottomSheetBehavior.STATE_DRAGGING] = action
    }

    fun whenSettling(action: () -> Unit) =
        apply { actions[BottomSheetBehavior.STATE_SETTLING] = action }

    fun whenHalfExpanded(action: () -> Unit) = apply {
        actions[BottomSheetBehavior.STATE_SETTLING] = action
    }
}
