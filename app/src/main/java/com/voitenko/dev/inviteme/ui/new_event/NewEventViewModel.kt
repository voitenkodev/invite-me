package com.voitenko.dev.inviteme.ui.new_event

import android.app.Application
import android.graphics.Bitmap
import android.net.Uri
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.StateErrorApp
import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.domain.event_uc.SetEventUc
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_CALENDAR
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.saveToFile
import com.voitenko.dev.inviteme.utils.extensions.toCalendar
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import org.koin.core.component.inject
import java.util.*

class NewEventViewModel(private val applicationContext: Application) : BaseViewModel() {

    private val setEventUc by inject<SetEventUc>()

    private val eventId: Long = System.currentTimeMillis()
    private var eventTitle: String = DEFAULT_STRING
    private var eventDescription: String = DEFAULT_STRING
    private var eventDate: Calendar = DEFAULT_CALENDAR
    private var eventAddressApp: AddressApp = AddressApp.EMPTY_ADDRESS
    private var eventImage: Uri? = null

    val successAddEvent = singleResultFlow<Long>()
    val changeImage = singleResultFlow<Unit>()

    @FlowPreview
    fun saveEvent() = successAddEvent.processing {
        flow {
            val event = when {
                eventTitle.isBlank() -> throw StateErrorApp(R.string.error_input_event_title)
                eventDescription.isBlank() -> throw StateErrorApp(R.string.error_input_event_description)
                eventAddressApp == AddressApp.EMPTY_ADDRESS -> throw StateErrorApp(R.string.error_input_event_address)
                eventDate == DEFAULT_CALENDAR -> throw StateErrorApp(R.string.error_input_event_date)
                else -> EventApp(
                    eventId = eventId,
                    title = eventTitle,
                    description = eventDescription,
                    date = eventDate,
                    addressApp = eventAddressApp,
                    imageLocalPath = eventImage?.let { return@let it.toString() },
                )
            }
            emit(event)
        }.flatMapConcat { event ->
            setEventUc.invoke(event)
        }
    }

    val navigateBackEvent = { navigatorEvent.call(AppRouter.FromApp.ToBack) }

    val pickDateAndTime = { navigatorEvent.call(AppRouter.FromNewEvent.ToDateTimePicker) }

    val pickAddress = { navigatorEvent.call(AppRouter.FromNewEvent.ToMapPicker) }

    val navigateToGallery = { navigatorEvent.call(AppRouter.FromApp.ToGallery) }

    val changeTitleEvent = { s: CharSequence -> eventTitle = s.toString() }

    val changeDescriptionEvent = { s: CharSequence -> eventDescription = s.toString() }

    val setAddress = { a: AddressApp -> eventAddressApp = a }

    val changeDateEvent = { date: Date -> eventDate = date.toCalendar() }

    val bitmapError = {
        changeImage.processing { flow { throw StateErrorApp(R.string.error_invalid_bitmap) } }
    }

    fun changeImageEvent(bitmap: Bitmap) = changeImage.processing {
        flow {
            val uri = bitmap.saveToFile(eventId.toString(), applicationContext)
            eventImage = uri
            emit(Unit)
        }
    }
}