package com.voitenko.dev.inviteme

import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.StateResult
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.core.domain.settings_uc.GetNightDayModeUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.SetDayModeUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.SetNightModeUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.TryToUpdateCountOfOpenUc
import kotlinx.coroutines.flow.map
import org.koin.core.component.inject

class MainViewModel : BaseViewModel() {

    private val setNightModeUc by inject<SetNightModeUc>()
    private val setDayModeUc by inject<SetDayModeUc>()
    private val getNightDayModeUc by inject<GetNightDayModeUc>()
    private val tryToUpdateCountOfOpenUc by inject<TryToUpdateCountOfOpenUc>()

    val modeStatusObserve = stateResultFlow<Mode>()
    val updateModeEvent = singleResultFlow<Mode>()
    val tryToUpdateCountOfOpenEvent = singleResultFlow<Boolean>()

    init {
        tryToUpdateCountOfOpen()
        modeStatusObserve.processing { getNightDayModeUc.invoke() }
    }

    private fun tryToUpdateCountOfOpen() =
        tryToUpdateCountOfOpenEvent.processing { tryToUpdateCountOfOpenUc.invoke() }

    fun updateModeStatus(mode: Mode) = updateModeEvent.processing {
        (if (mode == Mode.DARK) setNightModeUc.invoke() else setDayModeUc.invoke())
            .map { mode }
    }
}