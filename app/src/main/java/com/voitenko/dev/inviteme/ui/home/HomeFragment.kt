package com.voitenko.dev.inviteme.ui.home

import android.os.Bundle
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.recyclerview.widget.RecyclerView
import com.voitenko.dev.inviteme.MainViewModel
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.base.getBindingItemByPos
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.databinding.FragmentHomeBinding
import com.voitenko.dev.inviteme.databinding.ItemEventBinding
import com.voitenko.dev.inviteme.utils.AdapterTransitionBundle
import com.voitenko.dev.inviteme.utils.controller.addMotionController
import com.voitenko.dev.inviteme.utils.extensions.navigateTo
import com.voitenko.dev.inviteme.utils.extensions.ui.gone
import com.voitenko.dev.inviteme.utils.extensions.ui.isInVisible
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.ui.scaleToOriginal
import com.voitenko.dev.inviteme.utils.extensions.ui.show
import com.voitenko.dev.inviteme.utils.extensions.ui.up
import com.voitenko.dev.inviteme.utils.extensions.withSharedTransition
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home) {

    override val viewModel: HomeViewModel by viewModel()
    private val activityViewModel: MainViewModel by sharedViewModel()
    private val vpAdapter: EventPageAdapter by inject { parametersOf(viewModel) }

    override fun FragmentHomeBinding.setupUi(b: Bundle?) {
        navigateWithSharedTransition()
        eventRv.setupHomeRecycler()
        motionRoot.observeMotionAnimations()
        viewModel.observeVM()
        activityViewModel.observeVM()
        viewModel.observeNavigation()
        viewModel.doOnFirstEnter { animatedInitViews() }
        addActions()
    }

    private fun FragmentHomeBinding.addActions() {
        searchBtn.setOnClickListener { viewModel.startAnimationToSearch() }
        newEventBtn.setOnClickListener { viewModel.startAnimationToEvent() }
        darkModeSwitch.setOnCheckedChangeListener { _, isChecked -> viewModel.changeMode(isChecked) }
    }

    private fun RecyclerView.setupHomeRecycler() = apply { adapter = vpAdapter }

    override fun FragmentHomeBinding.releaseUi() = let { eventRv.adapter = null }

    private fun HomeViewModel.observeVM() = viewLifecycleOwner
        .observe(listOfEvents) { state ->
            state.processingUi(
                success = {
                    updatePlaceholder(it.isEmpty())
                    vpAdapter.submitList(it.asReversed())
                }
            )
        }.observe(backToScreenTransitionEvent) {
            it.transitionName = getString(R.string.image_to_info_event)
            startPostponedEnterTransition()
        }.observe(modeEvent) { mode ->
            activityViewModel.updateModeStatus(mode)
        }.observe(startAnimatedNavigationEvent) {
            binding.motionRoot.setTransition(it.first, it.second)
            binding.motionRoot.transitionToEnd()
        }

    private fun MainViewModel.observeVM() = viewLifecycleOwner
        .observe(modeStatusObserve) { state ->
            state.processingUi(success = { binding.darkModeSwitch.isChecked = it == Mode.DARK })
        }

    private fun HomeViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromApp.ToFinishActivity -> requireActivity().finish()
            is AppRouter.FromHome.ToSearch -> navigateTo(HomeFragmentDirections.fromHomeToSearch())
            is AppRouter.FromHome.ToEvent -> navigateTo(HomeFragmentDirections.fromHomeToEvent())
            is AppRouter.FromHome.ToEventInfo -> {
                binding.eventRv.clearPreviewItemTransition(it.adapterBundle)
                navigateTo(
                    HomeFragmentDirections.fromHomeToEventInfo(it.eventId),
                    it.adapterBundle.view?.withSharedTransition(R.string.image_to_info_event)
                )
            }
        }
    }

    private fun RecyclerView.clearPreviewItemTransition(adapterBundle: AdapterTransitionBundle) {
        adapterBundle.previewPosition?.let { previewPos ->
            getBindingItemByPos<ItemEventBinding>(previewPos)?.let { binding ->
                binding.eventPictureIv.transitionName = null
            }
        }
    }

    private fun onReEnterFragment() = takeIf { viewModel.reEnterAnimation.first }?.let {
        viewModel.reEnterAnimation.second?.equals(PreviousFragmentState.SEARCH_FRAGMENT) {
            binding.motionRoot.setTransition(R.id.openedSearchState, R.id.defaultState)
        }?.equals(PreviousFragmentState.NEW_EVENT_FRAGMENT) {
            binding.motionRoot.setTransition(R.id.openedNewEventState, R.id.defaultState)
        }.also { binding.motionRoot.transitionToEnd() }
    }

    private fun FragmentHomeBinding.animatedInitViews() {
        newEventBtnBackground.up(startDelay = 100)
        newEventBtn.up(startDelay = 100)
    }

    private fun updatePlaceholder(visible: Boolean) {
        if (visible && binding.placeHolder.isInVisible() && binding.placeHolderTv.isInVisible()) {
            binding.placeHolderTv.show().scaleToOriginal()
            binding.placeHolder.show().scaleToOriginal()
        } else if (!visible) {
            binding.placeHolderTv.gone()
            binding.placeHolder.gone()
        }
    }

    private fun MotionLayout.observeMotionAnimations() =
        addMotionController(viewLifecycleOwner.lifecycle)
            .doOnEnd(R.id.openedSearchState) { viewModel.navigateToSearch() }
            .doOnEnd(R.id.openedNewEventState) { viewModel.navigateToEvent() }
            .doOnEnd(R.id.defaultState) {
                takeIf { viewModel.isBackAnimationFromNewEvent() }?.let { binding.eventRv.resetAdapter() }
                viewModel.clearPreviousNavigation()
            }.onStartScreen { onReEnterFragment() }

    private fun RecyclerView.resetAdapter() {
        val temp = this.adapter
        this.adapter = temp
    }

    private fun navigateWithSharedTransition() = takeIf { viewModel.isNeedTransitionAnim() }
        ?.let { postponeEnterTransition() }

    override fun backPress() = viewModel.finishActivity()
}