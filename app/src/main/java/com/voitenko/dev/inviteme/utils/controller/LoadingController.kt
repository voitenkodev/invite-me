package com.voitenko.dev.inviteme.utils.controller

import android.view.View
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.flow.MutableStateFlow

class LoadingController {

    @Volatile
    private var loadingCounter = 0

    private val loadingEvent: MutableStateFlow<Int> = MutableStateFlow(View.GONE)

    fun observeLoading(owner: LifecycleOwner, observer: (Int) -> Unit) =
        loadingEvent.observe(owner, { observer.invoke(it) })

    suspend fun addLoading(showProgress: Boolean = true) {
        loadingCounter += 1
        if (showProgress) loadingEvent.emit(if (loadingCounter > 0) View.VISIBLE else View.GONE)
    }

    suspend fun removeLoading(showProgress: Boolean = true) {
        if (loadingCounter > 0) loadingCounter -= 1
        if (showProgress) loadingEvent.emit(if (loadingCounter > 0) View.VISIBLE else View.GONE)
    }
}