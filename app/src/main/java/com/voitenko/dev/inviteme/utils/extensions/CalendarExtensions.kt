package com.voitenko.dev.inviteme.utils.extensions

import java.text.DateFormatSymbols
import java.util.*

fun Calendar.toUiString(): String {
    return "${this.get(Calendar.DAY_OF_MONTH)} ${getMonthForInt(this.get(Calendar.MONTH))} (${
        String.format(
            "%02d : %02d", this.get(Calendar.HOUR_OF_DAY), this.get(Calendar.MINUTE)
        )
    })"
}

fun Date.toUiString(): String = this.toCalendar().toUiString()

fun Calendar.isPassed() = (currentCalendar().timeInMillis > this.timeInMillis)

fun Date.toCalendar(): Calendar = currentCalendar().apply { time = this@toCalendar }

fun currentDate() = GregorianCalendar(
    currentYear(), currentMonth(), currentDayOfMonth(), currentHour(), currentMinute()
)

fun currentDateWithYear(year: Int) =
    GregorianCalendar(year, currentMonth(), currentDayOfMonth(), currentHour(), currentMinute())

fun currentYear() = Calendar.getInstance().get(Calendar.YEAR)

private fun currentCalendar() = Calendar.getInstance()
private fun currentMonth() = currentCalendar().get(Calendar.MONTH)
private fun currentDayOfMonth() = currentCalendar().get(Calendar.DAY_OF_MONTH)
private fun currentHour() = currentCalendar().get(Calendar.HOUR_OF_DAY)
private fun currentMinute() = currentCalendar().get(Calendar.MINUTE)
private fun getMonthForInt(num: Int): String {
    var month = DEFAULT_STRING
    val dfs = DateFormatSymbols()
    val months: Array<String> = dfs.months
    if (num in 0..11) {
        month = months[num]
    }
    return month
}

// Kotlin dateTime
//fun Calendar.toUiString(): String {
//    return "${this.get(Calendar.DAY_OF_MONTH)} ${getMonthForInt(this.get(Calendar.MONTH))} (${
//        String.format(
//            "%02d : %02d", this.get(Calendar.HOUR_OF_DAY), this.get(Calendar.MINUTE)
//        )
//    })"
//}
//fun Calendar.isPassed() = (currentCalendar().toEpochMilliseconds() > this.timeInMillis)
//fun Date.toUiString(): String = this.toCalendar().toUiString()
//fun Date.toCalendar(): Calendar = GregorianCalendar().apply { time = this@toCalendar }
//
//fun currentDate() = GregorianCalendar(
//    currentYear(), currentMonth(), currentDayOfMonth(), currentHour(), currentMinute()
//)
//fun currentDateWithYear(year: Int) =
//    GregorianCalendar(year, currentMonth(), currentDayOfMonth(), currentHour(), currentMinute())
//fun currentYear() = currentLocalTime().year
//private fun currentCalendar() = Clock.System.now()
//private fun currentLocalTime() = currentCalendar().toLocalDateTime(currentSystemDefault())
//private fun currentMonth() = currentLocalTime().monthNumber
//private fun currentDayOfMonth() = currentLocalTime().dayOfMonth
//private fun currentHour() = currentLocalTime().hour
//private fun currentMinute() = currentLocalTime().minute
//
//private fun getMonthForInt(num: Int): String {
//    var month = DEFAULT_STRING
//    val dfs = DateFormatSymbols()
//    val months: Array<String> = dfs.months
//    if (num in 0..11) {
//        month = months[num]
//    }
//    return month
//}