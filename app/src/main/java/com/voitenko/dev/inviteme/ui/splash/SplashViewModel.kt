package com.voitenko.dev.inviteme.ui.splash

import com.voitenko.dev.inviteme.core.domain.settings_uc.GetNightDayModeUc
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.StateResult
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import org.koin.core.component.inject

class SplashViewModel : BaseViewModel() {

    private val dayNightModeUc: GetNightDayModeUc by inject()

    val modeEvent = singleResultFlow<Mode>()

    init {
        modeEvent.processing { dayNightModeUc.invoke() }
    }

    fun navigateToMain() = navigatorEvent.call(AppRouter.FromSplash.ToMain)
}