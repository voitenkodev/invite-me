package com.voitenko.dev.inviteme.utils.extensions.ui

import android.view.View

fun View.isVisible() = (this.visibility == View.VISIBLE)
fun View.isInVisible() = (this.visibility != View.VISIBLE)

fun View.show() = apply { this.visibility = View.VISIBLE }
fun View.hide() = apply { this.visibility = View.INVISIBLE }
fun View.gone() = apply { this.visibility = View.GONE }
