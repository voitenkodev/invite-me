package com.voitenko.dev.inviteme.utils.extensions.ui

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat

fun Drawable.withTint(tintColor: Int, context: Context) = this.apply {
    DrawableCompat.setTint(DrawableCompat.wrap(this), ContextCompat.getColor(context, tintColor))
}