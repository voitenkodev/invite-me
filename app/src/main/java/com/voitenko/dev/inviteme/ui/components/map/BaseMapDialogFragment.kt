package com.voitenko.dev.inviteme.ui.components.map

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.provider.Settings
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.voitenko.dev.inviteme.BuildConfig
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.BaseBottomDialogFragment
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.databinding.DialogMapBinding
import com.voitenko.dev.inviteme.utils.extensions.drawable
import com.voitenko.dev.inviteme.utils.extensions.ui.show
import com.voitenko.dev.inviteme.utils.extensions.ui.withTint
import kotlinx.coroutines.delay

abstract class BaseMapDialogFragment :
    BaseBottomDialogFragment<DialogMapBinding>(R.layout.dialog_map) {

    private var mapboxMap: MapboxMap? = null

    override fun DialogMapBinding.setupUi(b: Bundle?) {
        binding.mapView.onCreate(b)
    }

    protected fun Context.isGpsNotEnabled(): Boolean {
        val locationMode: Int = try {
            Settings.Secure.getInt(this.contentResolver, Settings.Secure.LOCATION_MODE)
        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
            return false
        }
        return locationMode == Settings.Secure.LOCATION_MODE_OFF
    }

    protected fun MapView.setupMap(
        mode: Mode,
        mapClick: (LatLng, markerManager: SymbolManager, style: Style) -> Boolean
    ) = getMapAsync { mapboxMap ->
        if (mapboxMap.style == null) {
            mapboxMap.uiSettings.isCompassEnabled = false
            mapboxMap.uiSettings.isLogoEnabled = false
            mapboxMap.uiSettings.isAttributionEnabled = false
            mapboxMap.uiSettings.logoGravity = Gravity.BOTTOM or Gravity.END
            mapboxMap.setStyle(if (mode == Mode.DARK) Style.DARK else Style.LIGHT) { style ->
                this.showMap()
                mapboxMap.showUserLocation(style)
                val markerManager = SymbolManager(this, mapboxMap, style)
                mapboxMap.addOnMapClickListener { latLng ->
                    mapClick.invoke(latLng, markerManager, style)
                }
            }
        }
        this@BaseMapDialogFragment.mapboxMap = mapboxMap
    }

    protected fun SymbolManager.createMarker(latLng: LatLng, style: Style) = context
        ?.drawable(R.drawable.ic_location)?.withTint(R.color.marker_color, requireContext())?.let {
            deleteAll()
            iconAllowOverlap = true
            style.addImage("marker", it)
            create(SymbolOptions().withLatLng(latLng).withIconImage("marker").withIconSize(1.1f))
            true
        } ?: false

    @SuppressLint("MissingPermission")
    protected fun MapboxMap.showUserLocation(style: Style) {
        val locationOptions = LocationComponentOptions.builder(requireContext()).build()
        val locationActivationOptions = LocationComponentActivationOptions
            .builder(requireContext(), style).locationComponentOptions(locationOptions).build()
        locationComponent.activateLocationComponent(locationActivationOptions)
        locationComponent.isLocationComponentEnabled = true
        locationComponent.cameraMode = CameraMode.TRACKING_GPS
        locationComponent.renderMode = RenderMode.NORMAL
    }

    private fun MapView.showMap() = lifecycleScope.launchWhenResumed {
        delay(300)
        this@showMap.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val token = BuildConfig.MAP_KEY
        Mapbox.getInstance(requireContext(), token)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.mapView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }

    @SuppressLint("MissingPermission")
    override fun DialogMapBinding.releaseUi() {
        mapboxMap?.locationComponent?.isLocationComponentEnabled = false
        binding.mapView.onDestroy()
    }
}