package com.voitenko.dev.inviteme.utils

import android.view.View
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_INT

data class AdapterTransitionBundle(
    var position: Int? = DEFAULT_INT,
    var previewPosition: Int? = null,
    var view: View? = null,
    var needAnim: Boolean = false
) {
    fun update(position: Int, view: View, needAnim: Boolean) {
        this.previewPosition = this.position.takeIf { it != DEFAULT_INT }
        this.position = position
        this.view = view
        this.needAnim = needAnim
    }
}