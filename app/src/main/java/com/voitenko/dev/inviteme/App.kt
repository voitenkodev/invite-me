package com.voitenko.dev.inviteme

import android.app.Application
import com.airbnb.lottie.LottieCompositionFactory
import com.voitenko.dev.inviteme.di.getModules
import com.voitenko.dev.inviteme.core.InviteMeCore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

@ExperimentalCoroutinesApi
class App : Application() {

//    @KoinExperimentalAPI
    @ExperimentalStdlibApi
    override fun onCreate() {
        super.onCreate()

        // preload animation
        LottieCompositionFactory.fromRawRes(applicationContext, R.raw.lottie_loading_style)

        startKoin {
            androidContext(this@App)
            modules(getModules())
        }
        InviteMeCore.inject()
    }
}