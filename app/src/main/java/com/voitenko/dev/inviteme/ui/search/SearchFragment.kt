package com.voitenko.dev.inviteme.ui.search

import android.os.Bundle
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.databinding.FragmentSearchBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchEventBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchGuestBinding
import com.voitenko.dev.inviteme.utils.AdapterTransitionBundle
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.getBindingItemByPos
import com.voitenko.dev.inviteme.utils.controller.addMotionController
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.navigateBack
import com.voitenko.dev.inviteme.utils.extensions.navigateTo
import com.voitenko.dev.inviteme.utils.extensions.ui.alphaTo
import com.voitenko.dev.inviteme.utils.extensions.ui.fall
import com.voitenko.dev.inviteme.utils.extensions.ui.gone
import com.voitenko.dev.inviteme.utils.extensions.ui.isInVisible
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.ui.show
import com.voitenko.dev.inviteme.utils.extensions.withSharedTransition
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class SearchFragment : BaseFragment<FragmentSearchBinding>(R.layout.fragment_search) {

    override val viewModel by viewModel<SearchViewModel>()
    private val adapter by inject<SearchAdapter> { parametersOf(viewModel) }

    @ExperimentalCoroutinesApi
    override fun FragmentSearchBinding.setupUi(b: Bundle?) {
        navigateWithSharedTransition()
        viewModel.observeVM()
        viewModel.observeNavigation()
        motionRoot.observeMotionAnimations()
        searchRv.adapter = adapter
        addActions()
    }

    @ExperimentalCoroutinesApi
    private fun FragmentSearchBinding.addActions() {
        searchField.doOnTextChanged { text, _, _, _ -> viewModel.searchBy(text) }
        cancelSearchBtn.setOnClickListener {
            if (viewModel.isNeedClearSearch) searchField.setText(DEFAULT_STRING)
            else viewModel.startEndAnimationScreen()
        }
    }

    private fun SearchViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromApp.ToBack -> this@SearchFragment.navigateBack()
            is AppRouter.FromSearch.ToEventInfo -> {
                binding.searchRv.clearPreviewItemTransition(it.adapterBundle)
                navigateTo(
                    SearchFragmentDirections.fromSearchToEventInfo(it.eventId),
                    it.adapterBundle.view?.withSharedTransition(R.string.image_to_info_event)
                )
            }
            is AppRouter.FromSearch.ToGuestInfo -> {
                binding.searchRv.clearPreviewItemTransition(it.adapterBundle)
                navigateTo(
                    SearchFragmentDirections.fromSearchToGuestInfo(it.guestId),
                    it.adapterBundle.view?.withSharedTransition(R.string.image_to_info_event)
                )
            }
        }
    }

    override fun FragmentSearchBinding.releaseUi() {
        searchRv.adapter = null
    }

    private fun SearchViewModel.observeVM() = viewLifecycleOwner
        .observe(searchResult) { state ->
            state.processingUi(success = {
                updatePlaceholder(it.isEmpty())
                adapter.submitList(it)
                if (it.isNotEmpty()) binding.searchRv.show()
            })
        }.observe(observeBackScreenTransition) {
            it.transitionName = getString(R.string.image_to_info_event)
            startPostponedEnterTransition()
        }.observe(startEndAnimationScreenEvent) {
            binding.motionRoot.transitionToStart()
            binding.searchRv.fall(binding.searchRv.height.toFloat()) {}
            updatePlaceholder(false)
        }

    private fun RecyclerView.clearPreviewItemTransition(adapterBundle: AdapterTransitionBundle) {
        adapterBundle.previewPosition?.let { previewPos ->
            getBindingItemByPos<ItemSearchEventBinding>(previewPos)
                ?.let { binding -> binding.eventPictureIv.transitionName = null }
            getBindingItemByPos<ItemSearchGuestBinding>(previewPos)
                ?.let { binding -> binding.guestPictureIv.transitionName = null }
        }
    }

    @ExperimentalCoroutinesApi
    private fun MotionLayout.observeMotionAnimations() =
        addMotionController(viewLifecycleOwner.lifecycle)
            .doOnEnd(R.id.closedSearchState) { viewModel.navigateBack() }
            .doOnEnd(R.id.openedSearchState) { viewModel.initGuestsAndEvents() }
            .onStartScreen { transitionToEnd() }

    private fun updatePlaceholder(visible: Boolean) {
        if (visible && binding.placeHolder.isInVisible() && binding.placeHolderTv.isInVisible()) {
            binding.placeHolderTv.show().apply { alpha = 0F }.alphaTo(1F) {}
            binding.placeHolder.show().apply { alpha = 0F }.alphaTo(1F) {}
        } else if (!visible) {
            binding.placeHolderTv.gone()
            binding.placeHolder.gone()
        }
    }

    private fun navigateWithSharedTransition() = takeIf { viewModel.isNeedTransitionAnim() }
        ?.let { postponeEnterTransition() }

    override fun backPress() {
        binding.cancelSearchBtn.performClick()
    }
}