package com.voitenko.dev.inviteme.ui.scanner

import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.StateErrorApp
import com.voitenko.dev.inviteme.core.data.models.models.GuestApp
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.models.models.decodeTicket
import com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc.FindGuestOnEventUc
import com.voitenko.dev.inviteme.core.domain.guest_event_ref_uc.SetStatusTicketUc
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestByIdUc
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.transform
import org.koin.core.component.inject

class ScannerViewModel(private val currentIdEvent: Long) : BaseViewModel() {

    private val getGuestByIdUc by inject<GetGuestByIdUc>()
    private val findGuestOnEventUc by inject<FindGuestOnEventUc>()
    private val setStatusGuestOnEventUc by inject<SetStatusTicketUc>()

    val currentGuestEvent = singleResultFlow<GuestApp>()
    val setStatusGuestOnEvent = singleResultFlow<Unit>()
    val currentStatusEvent = shared<Int>()
    val resetScannerEvent = shared<Unit>()
    val checkPermissionEvent = shared<Unit>()
    val closeScreenEvent = shared<Unit>()

    // It do on IO thread
    fun findGuestOnEvent(qrCodeContent: String) = currentGuestEvent.processing {
        flow {
            emit(qrCodeContent.decodeTicket())
        }.mapNotNull { ticket ->
            if (ticket != null && ticket.eventId == currentIdEvent) ticket
            else throw StateErrorApp(R.string.error_camera_cannot_find_event).also { resetScanner() }
        }.flatMapConcat { ticket ->
            findGuestOnEventUc.invoke(ticket.eventId, ticket.guestId)
        }.mapNotNull { ticket ->
            if (ticket == null) {
                resetScanner()
                throw StateErrorApp(R.string.error_camera_not_invited_on_event)
            } else ticket
        }.onEach { ticket ->
            if (ticket.status == TicketApp.StatusApp.ACTIVE) currentStatusEvent.call(R.string.guest_status_invited)
            else currentStatusEvent.call(R.string.guest_status_already_arrived)
        }.filter { ticket ->
            ticket.status == TicketApp.StatusApp.ACTIVE
        }.flatMapConcat { ticket ->
            setStatusGuestOnEventUc.invoke(ticket.copy(status = TicketApp.StatusApp.USED))
                .map { ticket }
        }.flatMapConcat { ticket ->
            getGuestByIdUc.invoke(ticket.guestId)
        }.mapNotNull { guest ->
            if (guest == null) {
                resetScanner()
                throw StateErrorApp(R.string.error_camera_cannot_find_guest)
            } else guest
        }
    }


    fun resetScanner() = resetScannerEvent.call(Unit)

    fun navigateBack() = let { navigatorEvent.call(AppRouter.FromApp.ToBack) }

    fun closeScreen() = closeScreenEvent.call(Unit)

    fun checkPermission() = checkPermissionEvent.call(Unit)

    private fun Flow<TicketApp?>.findGuestEventRef() = filter { ticket ->
        if (ticket != null && ticket.status == TicketApp.StatusApp.ACTIVE) true.also {
            currentStatusEvent.call(R.string.guest_status_invited)
        } else if (ticket != null && ticket.status != TicketApp.StatusApp.ACTIVE) true.also {
            currentStatusEvent.call(R.string.guest_status_already_arrived)
        }
        else false.also {
            resetScanner()
            throw StateErrorApp(R.string.error_camera_not_invited_on_event)
        }
    }

    private fun Flow<TicketApp>.findGuestByRef() = transform {
        emit(getGuestByIdUc.invoke(it.guestId).first())
    }.filter { guest ->
        guest?.let { true } ?: false.also {
            resetScanner()
            throw StateErrorApp(R.string.error_camera_cannot_find_guest)
        }
    }
}