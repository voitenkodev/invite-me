package com.voitenko.dev.inviteme.ui.splash

import android.content.Intent
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.lifecycleScope
import com.voitenko.dev.inviteme.MainActivity
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.databinding.ActivitySplashBinding
import com.voitenko.dev.inviteme.base.BaseActivity
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.SuccessState
import com.voitenko.dev.inviteme.utils.controller.addMotionController
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_INT
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.updateMode
import com.voitenko.dev.inviteme.utils.extensions.updateStatusBar
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : BaseActivity<ActivitySplashBinding>(R.layout.activity_splash) {

    private val viewModel: SplashViewModel by viewModel()

    override fun setupBinding(binding: ActivitySplashBinding) {
        viewModel.observeNavigation()
        viewModel.observeVM()
        binding.motionRoot.observeMotionAnimation()
    }

    private fun initAnimation() = lifecycleScope.launch {
        delay(250)
        binding.motionRoot.transitionToEnd()
    }

    private fun MotionLayout.observeMotionAnimation() =
        addMotionController(this@SplashActivity.lifecycle)
            .doOnEnd(R.id.openedSplashState) { viewModel.navigateToMain() }
            .onStartScreen { initAnimation() }

    private fun SplashViewModel.observeVM() =
        observe(modeEvent) { state ->
            when (state) {
                is SuccessState -> {
                    updateMode(state.value)
                    updateStatusBar(state.value)
                }
            }
        }

    private fun SplashViewModel.observeNavigation() = observeNavigation(this@SplashActivity) {
        when (it) {
            is AppRouter.FromSplash.ToMain -> {
                startActivity(
                    Intent(this@SplashActivity, MainActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                )
                this@SplashActivity.overridePendingTransition(DEFAULT_INT, DEFAULT_INT)
            }
        }
    }
}
