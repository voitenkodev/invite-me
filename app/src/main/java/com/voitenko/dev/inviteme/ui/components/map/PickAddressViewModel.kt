package com.voitenko.dev.inviteme.ui.components.map

import androidx.core.os.bundleOf
import com.mapbox.mapboxsdk.geometry.LatLng
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.StateResult
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.core.domain.location_uc.GetLocationsByQueryUc
import com.voitenko.dev.inviteme.core.domain.settings_uc.GetNightDayModeUc
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.showLog
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import org.koin.core.component.inject

class PickAddressViewModel : BaseViewModel() {

    private val getNightDayModeUc: GetNightDayModeUc by inject()
    private val getLocationsByQueryUc: GetLocationsByQueryUc by inject()

    private var addressName: String = DEFAULT_STRING
    private var addressLat: Double? = null
    private var addressLng: Double? = null

    val modeStatusObserve = singleResultFlow<Mode>()
    val checkPermissionEvent = shared<Unit>()

    private fun getAddress() = AddressApp(
        name = addressName,
        latitude = addressLat,
        longitude = addressLng
    )

    fun checkDarkMode() = modeStatusObserve.processing { getNightDayModeUc.invoke() }

    fun checkPermission() = checkPermissionEvent.call(Unit)

    fun inputManualAddress(s: String) = let { addressName = s }

    fun goToGpsSettings() = navigatorEvent.call(AppRouter.FromApp.ToGpsSettings)

    fun saveAddress() = navigatorEvent.call(
        AppRouter.FromNewEvent.FromMapDialog.ToNewEvent(
            PickAddressDialogFragment.MAP_RESULT,
            bundleOf(PickAddressDialogFragment.MAP_ADDRESS_RESULT to getAddress())
        )
    )

    val lastMarkerState = singleResultFlow<String>()

    fun findAddress(latLng: LatLng) = lastMarkerState.processing {
        getLocationsByQueryUc.invoke(latLng.latitude, latLng.longitude)
            .onEach {
                addressName = it.name
                addressLat = it.latitude
                addressLng = it.longitude
            }.map { it.name }
    }

    val navigateBack = { navigatorEvent.call(AppRouter.FromApp.ToBack) }
}