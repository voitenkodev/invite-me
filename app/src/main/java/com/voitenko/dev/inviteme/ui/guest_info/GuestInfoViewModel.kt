package com.voitenko.dev.inviteme.ui.guest_info

import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItemContract
import com.voitenko.dev.inviteme.core.data.models.models.guest_info.GuestInfoItemCombine
import com.voitenko.dev.inviteme.core.domain.guest_uc.GetGuestWithEventsByIdUc
import com.voitenko.dev.inviteme.utils.controller.InputDelayController
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import org.koin.core.component.inject

@ExperimentalStdlibApi
class GuestInfoViewModel(private val guestId: Long) : BaseViewModel(), SearchItemContract {

    private val getGuestWithEventsById by inject<GetGuestWithEventsByIdUc>()

    val guestInfo = stateResultFlow<List<GuestInfoItemCombine>>()
    val prepareInfoGuestLayout = shared<Unit>()

    private var inputController: InputDelayController = InputDelayController(100, 50)
    private var lastQuery = DEFAULT_STRING

    fun prepareLayoutEventInfo() = prepareInfoGuestLayout.call(Unit)

    fun loadGuestInfo(s: CharSequence? = lastQuery, progress: Boolean = false) =
        guestInfo.processing(progress) { getGuestWithEventsById.invoke(guestId, s.toString()) }

    override fun searchBy(s: CharSequence?) = inputController.onFinish { loadGuestInfo(s) }

    override fun onCleared() {
        super.onCleared()
        inputController.release()
    }
}
