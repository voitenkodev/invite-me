package com.voitenko.dev.inviteme.ui.event_info

import android.app.Application
import android.net.Uri
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.core.data.models.models.TicketApp
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.core.data.models.models.common.SearchItemContract
import com.voitenko.dev.inviteme.core.data.models.models.event_info.EventInfoItemCombine
import com.voitenko.dev.inviteme.core.data.models.models.generateTicket
import com.voitenko.dev.inviteme.core.domain.event_uc.GetEventByIdUc
import com.voitenko.dev.inviteme.core.domain.event_uc.GetEventWithGuestsByIdUc
import com.voitenko.dev.inviteme.utils.controller.InputDelayController
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import com.voitenko.dev.inviteme.utils.extensions.getUriByName
import kotlinx.coroutines.flow.onEach
import org.koin.core.component.inject

@ExperimentalStdlibApi
class EventInfoViewModel(private val applicationContext: Application, private val eventId: Long) :
    BaseViewModel(), SearchItemContract {

    private val getEventWithGuestsByIdUc by inject<GetEventWithGuestsByIdUc>()

    private val getEventByIdUc by inject<GetEventByIdUc>()

    val eventInfo = stateResultFlow<List<EventInfoItemCombine>>()
    val prepareInfoEventLayout = shared<Unit>()
    val getEventById = singleResultFlow<EventApp>()

    private val inputController = InputDelayController()
    private var lastQuery = DEFAULT_STRING

    init {
        loadEventInfo(progress = true)
    }

    fun prepareLayoutEventInfo() = prepareInfoEventLayout.call(Unit)

    fun navigateToAddNewGuest(eventImage: Uri) =
        navigatorEvent.call(AppRouter.FromEventInfo.ToAddGuest(eventId, eventImage))

    fun navigateToQrCodeScanner() = navigatorEvent.call(AppRouter.FromEventInfo.ToScanner(eventId))

    private fun loadEventInfo(
        s: CharSequence? = lastQuery,
        progress: Boolean = false,
        forceUpdate: Boolean = false
    ) = eventInfo.processing(progress) {
        getEventWithGuestsByIdUc.invoke(eventId, s.toString(), forceUpdate)
    }

    override fun searchBy(s: CharSequence?) = inputController.onFinish { loadEventInfo(s) }

    fun sendMessageToGuest(guestId: Long, guestEmail: String) = getEventById.processing {
        getEventByIdUc.invoke(eventId).onEach { event ->
            val pairOfEvents = TicketApp(eventId, guestId).generateTicket()
            val uriQrCode = applicationContext.getUriByName(pairOfEvents)
            navigatorEvent.call(AppRouter.FromApp.ToEmailGuest(guestEmail, event, uriQrCode))
        }
    }

    fun showLocationInMap(addressApp: AddressApp) {
        val url = if (addressApp.latitude != null && addressApp.longitude != null)
            "geo:<${addressApp.latitude}>,<${addressApp.longitude}>?q=<${addressApp.latitude}>,<${addressApp.longitude}>(${addressApp.name})"
        else "geo:0,0?q=${addressApp.name}"
        navigatorEvent.call(AppRouter.FromApp.ToMap(url))
    }

    override fun onCleared() {
        super.onCleared()
        inputController.release()
    }
}