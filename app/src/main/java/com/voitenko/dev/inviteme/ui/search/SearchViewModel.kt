package com.voitenko.dev.inviteme.ui.search

import android.view.View
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchItemCombine
import com.voitenko.dev.inviteme.core.domain.guest_event_uc.SearchGuestAndEventsUc
import com.voitenko.dev.inviteme.utils.AdapterTransitionBundle
import com.voitenko.dev.inviteme.base.BaseViewModel
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.utils.controller.InputDelayController
import com.voitenko.dev.inviteme.utils.extensions.DEFAULT_STRING
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.component.inject

class SearchViewModel : BaseViewModel() {

    private val searchUc by inject<SearchGuestAndEventsUc>()

    val searchResult = stateResultFlow<List<SearchItemCombine>>()
    val observeBackScreenTransition = shared<View>()
    val startEndAnimationScreenEvent = shared<Unit>()
    val isNeedClearSearch get() = takeIf { lastQuery.isNotEmpty() }?.let { true } ?: false

    private var adapterTransitionBundle = AdapterTransitionBundle()
    private var inputController: InputDelayController = InputDelayController()
    private var lastQuery = DEFAULT_STRING
    private var firstEnter = true

    @ExperimentalCoroutinesApi
    fun initGuestsAndEvents() = takeIf { firstEnter }?.let {
        loadGuestsAndEvents(progress = true)
        firstEnter = false
    }

    @ExperimentalCoroutinesApi
    fun loadGuestsAndEvents(s: CharSequence? = lastQuery, progress: Boolean = false) =
        searchResult.processing(progress) { searchUc.invoke(s.toString()) }

    @ExperimentalCoroutinesApi
    fun searchBy(s: CharSequence?) = inputController.onFinish { loadGuestsAndEvents(s) }

    // NAVIGATION ----------------------------------------------------------------------------------

    fun isNeedTransitionAnim() = adapterTransitionBundle.needAnim

    val startEndAnimationScreen = { startEndAnimationScreenEvent.call(Unit) }

    fun navigateToGuestInfo(view: View, guestId: Long, position: Int) {
        adapterTransitionBundle.update(position, view, true)
        navigatorEvent.call(AppRouter.FromSearch.ToGuestInfo(adapterTransitionBundle, guestId))
    }

    fun navigateToEventInfo(view: View, eventId: Long, position: Int) {
        adapterTransitionBundle.update(position, view, true)
        navigatorEvent.call(AppRouter.FromSearch.ToEventInfo(adapterTransitionBundle, eventId))
    }

    fun prepareItemEvent(view: View, position: Int) {
        view.transitionName = null
        if (adapterTransitionBundle.needAnim && adapterTransitionBundle.position == position) {
            observeBackScreenTransition.call(view)
            adapterTransitionBundle.needAnim = false
        }
    }

    fun prepareItemGuest(view: View, position: Int) {
        view.transitionName = null
        if (adapterTransitionBundle.needAnim && adapterTransitionBundle.position == position) {
            observeBackScreenTransition.call(view)
            adapterTransitionBundle.needAnim = false
        }
    }

    val navigateBack = { navigatorEvent.call(AppRouter.FromApp.ToBack) }

    override fun onCleared() {
        super.onCleared()
        inputController.release()
    }
}