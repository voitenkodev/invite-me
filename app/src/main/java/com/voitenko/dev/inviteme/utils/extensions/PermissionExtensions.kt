package com.voitenko.dev.inviteme.utils.extensions

import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

fun Context.checkPermission(
    permission: Array<String>,
    isGranted: (() -> Unit)?,
    isDenied: (() -> Unit)?
) {
    val isPermissionGranted = permission.map {
        ActivityCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }
    if (isPermissionGranted.findLast { !it } == null) {
        isGranted?.invoke()
    } else {
        isDenied?.invoke()
    }
}

fun Context.askPermission(permission: Array<String>, code: Int) {
    ActivityCompat.requestPermissions(this as AppCompatActivity, permission, code)
}