package com.voitenko.dev.inviteme

import androidx.lifecycle.lifecycleScope
import com.voitenko.dev.inviteme.databinding.ActivityMainBinding
import com.voitenko.dev.inviteme.base.BaseActivity
import com.voitenko.dev.inviteme.base.SuccessState
import com.voitenko.dev.inviteme.core.data.models.models.configs.Mode
import com.voitenko.dev.inviteme.utils.controller.showPreviewCommentDialog
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.updateMode
import com.voitenko.dev.inviteme.utils.extensions.updateStatusBar
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main) {

    val viewModel by viewModel<MainViewModel>()

    override fun setupBinding(binding: ActivityMainBinding) {
        viewModel.observeVM()
    }

    private fun MainViewModel.observeVM() = this@MainActivity
        .observe(modeStatusObserve) { state ->
            when (state) {
                is SuccessState -> updateStatusBar(state.value)
            }
        }.observe(updateModeEvent) { state ->
            when (state) {
                is SuccessState -> updateUiMode(state.value)
            }
        }.observe(tryToUpdateCountOfOpenEvent) { state ->
            when (state) {
                is SuccessState -> state.value.takeIf { !it }?.let { showPreviewCommentDialog() }
            }
        }

    private fun updateUiMode(mode: Mode) = lifecycleScope.launch {
        delay(350)
        updateMode(mode)
    }
}
