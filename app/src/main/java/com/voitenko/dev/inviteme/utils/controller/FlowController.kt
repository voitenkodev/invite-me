package com.voitenko.dev.inviteme.utils.controller

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

inline fun <reified T> Flow<T>.observe(
    lifecycleOwner: LifecycleOwner,
    noinline collector: suspend (T) -> Unit
) = FlowController(lifecycleOwner, this, collector)


class FlowController<T>(
    lifecycleOwner: LifecycleOwner,
    private val flow: Flow<T>,
    private val collector: suspend (T) -> Unit
) : LifecycleObserver {
    private var job: Job? = null

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start(owner: LifecycleOwner) {
        job = owner.lifecycleScope.launch { flow.collect { collector(it) } }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop(owner: LifecycleOwner) {
        job?.cancel()
        job = null
    }
}