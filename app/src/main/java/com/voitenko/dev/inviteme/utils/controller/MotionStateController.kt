package com.voitenko.dev.inviteme.utils.controller

import androidx.annotation.IdRes
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent

fun MotionLayout.addMotionController(lifecycle: Lifecycle) = MotionController(this, lifecycle)

class MotionController(private val motion: MotionLayout, lifecycle: Lifecycle) : LifecycleObserver {

    private val mapDoOnEnd = HashMap<Int, () -> Unit>()
    private val mapDoOnStart = HashMap<Int, () -> Unit>()
    private var doOnEnd: ((id: Int) -> Unit)? = null
    private var doOnStart: ((id: Int, id2: Int) -> Unit)? = null
    private var action: (() -> Unit)? = null

    private val listener = object : MotionLayout.TransitionListener {
        override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}

        override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
            doOnStart?.invoke(p1, p2)
        }

        override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
        }

        override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
            doOnEnd?.invoke(p1)
        }
    }

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    private fun onAnyEvent(owner: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_CREATE -> init()
            Lifecycle.Event.ON_START -> action?.invoke()
            Lifecycle.Event.ON_DESTROY -> release()
            else -> Unit
        }
    }

    fun onStartScreen(action: () -> Unit) = apply { this.action = action }

    fun doOnEnd(@IdRes idState: Int, action: () -> Unit) =
        apply { mapDoOnEnd[idState] = action }

    fun doOnStart(@IdRes idState: Int, action: () -> Unit) =
        apply { mapDoOnStart[idState] = action }

    private fun init() {
        this@MotionController.doOnStart = { it1, _ ->
            motion.isInteractionEnabled = false
            mapDoOnStart[it1]?.invoke()
        }
        this@MotionController.doOnEnd = {
            motion.isInteractionEnabled = true
            mapDoOnEnd[it]?.invoke()
        }
        motion.setTransitionListener(listener)
    }

    private fun release() {
        motion.removeTransitionListener(listener)
        doOnEnd = null
        doOnStart = null
        mapDoOnEnd.clear()
        mapDoOnStart.clear()
    }
}
