package com.voitenko.dev.inviteme.utils.extensions.ui

import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.utils.extensions.integer
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun View.shake() = suspendCoroutine<Unit> { c -> this.shake { c.resume(Unit) } }
suspend fun View.drop() = suspendCoroutine<Unit> { c -> this.drop { c.resume(Unit) } }
suspend fun View.up() = suspendCoroutine<Unit> { c -> this.up { c.resume(Unit) } }
suspend fun View.alphaTo(alpha: Float) =
    suspendCoroutine<Unit> { c -> this.alphaTo(alpha) { c.resume(Unit) } }

fun View.alphaTo(
    alpha: Float,
    duration: Long = this.context.integer(R.integer.default_mini_animation_duration).toLong(),
    startDelay: Long = 0,
    doOnEnd: (() -> Unit)? = null
) {
    val anim = ObjectAnimator.ofFloat(this, View.ALPHA, alpha)
    anim.duration = duration
    anim.doOnEnd { doOnEnd?.invoke() }
    anim.startDelay = startDelay
    anim.start()
}

fun View.scaleY(startScale: Float, endScale: Float) = apply {
    val anim: Animation = ScaleAnimation(
        1f, 1f,
        startScale, endScale,
        Animation.RELATIVE_TO_SELF, 0f,
        Animation.RELATIVE_TO_SELF, 1f
    )
    anim.fillAfter = true // Needed to keep the result of the animation
    anim.duration = 900
    this.startAnimation(anim)
}

fun View.scaleToOriginal() = apply {
    val scale = ScaleAnimation(
        0f, 1f, 0f, 1f,
        Animation.RELATIVE_TO_SELF, 0.5f,
        Animation.RELATIVE_TO_SELF, 0.5f
    )
    scale.duration = 400
    scale.fillAfter = true
    this@scaleToOriginal.startAnimation(scale)
}

fun View.shake(doOnEnd: (() -> Unit)? = null) = apply {
    ObjectAnimator
        .ofFloat(this, "translationX", 0F, 25F, -25F, 25F, -25F, 15F, -15F, 6F, -6F, 0F)
        .setDuration(500)
        .apply {
            startDelay = 200
            doOnEnd { doOnEnd?.invoke() }
        }
        .start()
}

fun View.drop(startDelay: Long = 0, doOnEnd: (() -> Unit)? = null) = apply {
    this.hide()
    ObjectAnimator
        .ofFloat(this, "translationY", -200F, 0F)
        .setDuration(500)
        .apply {
            this.startDelay = startDelay
            doOnStart { this@drop.show() }
            doOnEnd { doOnEnd?.invoke() }
        }.start()
}

fun View.fall(height: Float, startDelay: Long = 0, doOnEnd: (() -> Unit)? = null) = apply {
    ObjectAnimator
        .ofFloat(this, "translationY", 0F, height)
        .setDuration(300)
        .apply {
            this.startDelay = startDelay
            doOnEnd { doOnEnd?.invoke() }
        }.start()
}

fun View.up(height: Float = 200F, startDelay: Long = 0, doOnEnd: (() -> Unit)? = null) = apply {
    this.hide()
    ObjectAnimator
        .ofFloat(this, "translationY", height, 0F)
        .setDuration(500)
        .apply {
            this.startDelay = startDelay
            doOnStart { this@up.show() }
            doOnEnd { doOnEnd?.invoke() }
        }.start()
}