package com.voitenko.dev.inviteme.utils.extensions.ui

import android.app.Activity
import android.content.Context
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.ui.components.error_success.SuccessErrorSnackBar
import com.voitenko.dev.inviteme.ui.components.error_success.SnackType
import com.voitenko.dev.inviteme.ui.components.date_time.buildBaseDateTimeDialog
import java.util.*

// get problem SNACK
fun Activity.getProblemSnackBar(message: String) = SuccessErrorSnackBar.make(
    this.window.findViewById(android.R.id.content),
    message,
    1300,
    SnackType.PROBLEM_SNACK
)

// get success SNACK
fun Activity.getSuccessSnackBar(message: Int) = SuccessErrorSnackBar.make(
    this.window.findViewById(android.R.id.content),
    this.getString(message),
    1300,
    SnackType.SUCCESS_SNACK
)

// get DateTime Dialog
fun Context.getDateTimeDialog(
    title: Int,
    onPositiveClick: (date: Date?) -> Unit,
    onNegativeClick: (() -> Unit)? = null
): SwitchDateTimeDialogFragment = buildBaseDateTimeDialog(
    getString(title),
    getString(R.string.date_time_picker_ok),
    getString(R.string.date_time_picker_cancel),
    onPositiveClick, onNegativeClick
)