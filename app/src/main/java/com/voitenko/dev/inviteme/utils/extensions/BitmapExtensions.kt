package com.voitenko.dev.inviteme.utils.extensions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.RectF
import android.net.Uri
import android.os.Environment
import androidx.core.net.toUri
import com.google.firebase.crashlytics.FirebaseCrashlytics
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

private fun Context.pictureDirectory(nameFile: String): String =
    "${this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)}${File.separator}$nameFile.png"

fun Bitmap.saveToFile(filename: String, context: Context): Uri {
    val outStream: FileOutputStream
    try {
        outStream = FileOutputStream(context.pictureDirectory(filename))
        this.compress(Bitmap.CompressFormat.PNG, 100, outStream)
        outStream.flush()
        outStream.close()
    } catch (e: Exception) {
        showLog(e.message.toString())
        FirebaseCrashlytics.getInstance().recordException(e)
        return Uri.EMPTY
    }
    val filepath: String = context.pictureDirectory(filename)
    val file = File(filepath)
    return if (file.exists()) file.toUri() else Uri.EMPTY
}

fun Context.getUriByName(filename: String): Uri {
    val filepath: String = this.pictureDirectory(filename)
    val file = File(filepath)
    return if (file.exists()) file.toUri() else Uri.EMPTY
}

fun Uri.getBitmap(context: Context): Bitmap? {
    val iStream: InputStream? = context.contentResolver.openInputStream(this)
    val bitmap = BitmapFactory.decodeStream(iStream)
    iStream?.close()
    return bitmap
}

fun Bitmap.cutByRatio(ratio: String): Bitmap {
    val (newWidth, newHeight) = calcByRatio(this.width, this.height, ratio)
    val startX = ((this.width / 2) - (newWidth / 2).toInt()).takeIf { it >= 0 } ?: 0
    val endY = ((this.height / 2) - (newHeight / 2).toInt()).takeIf { it >= 0 } ?: 0
    return Bitmap.createBitmap(
        this,
        startX,
        endY,
        newWidth.toInt(),
        newHeight.toInt()
    )
}

fun Bitmap.roundByRadius(dp: Int): Bitmap {
    val output = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(output)
    val color = -0xbdbdbe
    val paint = Paint()
    val rect = Rect(0, 0, this.width, this.height)
    val rectF = RectF(rect)
    paint.isAntiAlias = true
    canvas.drawARGB(0, 0, 0, 0)
    paint.color = color
    canvas.drawRoundRect(rectF, dp.toPx(), dp.toPx(), paint)
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawBitmap(this, rect, rect, paint)
    return output
}
