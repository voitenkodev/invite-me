package com.voitenko.dev.inviteme.ui.components.error_success

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.marginBottom
import androidx.core.view.marginEnd
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.ContentViewCallback
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.utils.extensions.color
import com.voitenko.dev.inviteme.utils.extensions.drawable
import com.voitenko.dev.inviteme.utils.extensions.toPx

// resource: https://medium.com/@fabionegri/make-snackbar-great-again-51edf7c940d4

private const val SUCCESS_IMAGE = R.drawable.ic_success
private const val ERROR_IMAGE = R.drawable.ic_problem

private const val SUCCESS_COLOR = R.color.snack_success_background
private const val SUCCESS_TEXT_COLOR = R.color.snack_success_text_background
private const val ERROR_COLOR = R.color.snack_default_background
private const val ERROR_TEXT_COLOR = R.color.snack_default_text_background

enum class SnackType { SUCCESS_SNACK, PROBLEM_SNACK }

private const val TAG_VIEW = "CustomSnackBar"

class SuccessErrorSnackBar(parent: ViewGroup, content: CustomSnackBarView) :
    BaseTransientBottomBar<SuccessErrorSnackBar>(parent, content, content) {

    init {
        getView()
            .setBackgroundColor(ContextCompat.getColor(view.context, android.R.color.transparent))
        getView().setPadding(0, 0, 0, 0)
    }

    companion object {
        fun make(view: View, message: String, duration: Int, type: SnackType): SuccessErrorSnackBar? {
            val parent =
                view.findSuitableParent() ?: throw IllegalArgumentException("No suitable parent")
            return if (parent.findViewWithTag<CustomSnackBarView>(TAG_VIEW) == null) {
                val customView = (LayoutInflater.from(view.context)
                    .inflate(
                        R.layout.snack_bar_layout,
                        parent,
                        false
                    ) as CustomSnackBarView).apply {
                    setSnackType(type)
                    tag = TAG_VIEW
                    setMessage(message)
                }
                SuccessErrorSnackBar(parent, customView).apply { setDuration(duration) }
            } else null
        }
    }

    fun doOnEnd(action: () -> Unit) {
        addCallback(object : BaseTransientBottomBar.BaseCallback<SuccessErrorSnackBar>() {
            override fun onDismissed(transientBottomBar: SuccessErrorSnackBar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                action.invoke()
            }
        })
    }
}

class CustomSnackBarView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), ContentViewCallback {

    private var type: SnackType = SnackType.SUCCESS_SNACK
    private val problemImage: AppCompatImageView
    private val message: BackgroundTextView

    private var additionalRightMargin: Float = 0F
    private var additionalLeftMargin: Float = 0F
    private var additionalTopMargin: Float = 0F
    private var additionalBottomMargin: Float = 0F

    init {
        View.inflate(context, R.layout.snack_bar_view, this)
        this.problemImage = findViewById(R.id.problemIv)
        this.message = findViewById(R.id.messageTv)
        clipToPadding = false
    }

    fun setSnackType(type: SnackType) {
        this.type = type
        this.message.setSnackType(type)
        setupType()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        (layoutParams as MarginLayoutParams).apply {
            setMargins(
                additionalLeftMargin.toInt(),
                additionalTopMargin.toInt(),
                additionalRightMargin.toInt(),
                additionalBottomMargin.toInt()
            )
        }
        val realWidth = widthMeasureSpec - marginEnd - marginStart
        super.onMeasure(realWidth, heightMeasureSpec)
        measureChildren(realWidth, heightMeasureSpec)
    }

    private fun setupType() = when (type) {
        SnackType.SUCCESS_SNACK -> {
            problemImage.setImageDrawable(context.drawable(SUCCESS_IMAGE))
            message.setTextColor(context.color(SUCCESS_TEXT_COLOR))
        }
        SnackType.PROBLEM_SNACK -> {
            problemImage.setImageDrawable(context.drawable(ERROR_IMAGE))
            message.setTextColor(context.color(ERROR_TEXT_COLOR))
        }
    }.also {
        additionalBottomMargin = marginBottom.toFloat()
        additionalRightMargin = marginRight.toFloat()
        additionalLeftMargin = marginLeft.toFloat()
        additionalTopMargin = marginTop.toFloat()
    }

    fun setMessage(text: String) {
        message.text = text
    }

    override fun animateContentIn(delay: Int, duration: Int) {
        val scaleX = ObjectAnimator.ofFloat(problemImage, View.SCALE_X, 1f, 0.9f, 1f)
        val scaleY = ObjectAnimator.ofFloat(problemImage, View.SCALE_Y, 1f, 0.9f, 1f)
        AnimatorSet().apply {
            interpolator = OvershootInterpolator()
            setDuration(700)
            startDelay = 400
            playTogether(scaleX, scaleY)
            requestLayout()
        }.apply { start() }
    }

    override fun animateContentOut(delay: Int, duration: Int) {}
}

class BackgroundTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    private var type: SnackType = SnackType.PROBLEM_SNACK
    private val bgRectangle = RectF()
    private val mainPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply { style = Paint.Style.FILL }

    init {
        this.setWillNotDraw(false)
        this.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    fun setSnackType(type: SnackType) {
        this.type = type
        when (type) {
            SnackType.SUCCESS_SNACK ->
                mainPaint.color =
                    ContextCompat.getColor(this@BackgroundTextView.context, SUCCESS_COLOR)
            SnackType.PROBLEM_SNACK ->
                mainPaint.color =
                    ContextCompat.getColor(this@BackgroundTextView.context, ERROR_COLOR)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.apply {
            bgRectangle.apply {
                top = 0F
                left = 0F
                right = width.toFloat()
                bottom = height.toFloat()
            }.also { this.drawRoundRect(it, 8.toPx(), 8.toPx(), mainPaint) }
        }
        super.onDraw(canvas)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val realWidth = widthMeasureSpec - marginEnd - marginStart
        super.onMeasure(realWidth, heightMeasureSpec)
    }
}

private fun View?.findSuitableParent(): ViewGroup? {
    var view = this
    var fallback: ViewGroup? = null
    do {
        if (view is CoordinatorLayout) {
            return view
        } else if (view is FrameLayout) {
            if (view.id == android.R.id.content) {
                return view
            } else {
                fallback = view
            }
        }
        if (view != null) {
            val parent = view.parent
            view = if (parent is View) parent else null
        }
    } while (view != null)
    return fallback
}