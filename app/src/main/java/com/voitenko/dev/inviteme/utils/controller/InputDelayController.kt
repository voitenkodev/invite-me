package com.voitenko.dev.inviteme.utils.controller

import android.os.CountDownTimer

class InputDelayController(millisInFuture: Long = 450, countDownInterval: Long = 225) {

    private var timer: CountDownTimer? = null

    private var finish: (() -> Unit)? = null

    init {
        timer = object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                finish?.invoke()
            }
        }
    }

    fun onFinish(finish: () -> Unit) {
        timer?.cancel()
        this.finish = finish
        timer?.start()
    }

    fun cancel() = timer?.cancel()

    fun release() {
        finish = null
        timer?.cancel()
        timer = null
    }
}