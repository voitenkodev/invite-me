package com.voitenko.dev.inviteme.ui.search

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.UniversalItemHolder
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchEvent
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchGuest
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchHeader
import com.voitenko.dev.inviteme.core.data.models.models.search.SearchItemCombine
import com.voitenko.dev.inviteme.databinding.ItemSearchEventBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchGuestBinding
import com.voitenko.dev.inviteme.databinding.ItemSearchHeaderBinding
import com.voitenko.dev.inviteme.utils.extensions.inflateView
import com.voitenko.dev.inviteme.utils.extensions.string

class SearchAdapter(private val vm: SearchViewModel) :
    RecyclerView.Adapter<UniversalItemHolder>() {

    private val currentList = ArrayList<SearchItemCombine>()

    fun submitList(list: List<SearchItemCombine>) {
        val diffCallback = SearchDiffUtils(currentList, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        currentList.clear()
        currentList.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount() = currentList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UniversalItemHolder(parent.inflateView(viewType))

    override fun getItemViewType(position: Int) = when (currentList[position].getSearchType()) {
        SearchItemCombine.SearchItemType.SEARCH_HEAD -> R.layout.item_search_header
        SearchItemCombine.SearchItemType.SEARCH_EVENT -> R.layout.item_search_event
        SearchItemCombine.SearchItemType.SEARCH_GUEST -> R.layout.item_search_guest
    }

    override fun onBindViewHolder(holder: UniversalItemHolder, position: Int) =
        when (currentList[holder.adapterPosition].getSearchType()) {
            SearchItemCombine.SearchItemType.SEARCH_EVENT -> bindSearchEvent(holder)
            SearchItemCombine.SearchItemType.SEARCH_GUEST -> bindSearchGuest(holder)
            SearchItemCombine.SearchItemType.SEARCH_HEAD -> bindSearchHeader(holder)
        }

    private fun bindSearchEvent(holder: UniversalItemHolder) {
        val binding = holder.binding as ItemSearchEventBinding
        val pos = holder.layoutPosition
        val currentSearchEvent = (currentList[pos] as SearchEvent)
        holder.bind(currentSearchEvent, vm, pos)
        vm.prepareItemEvent(binding.eventPictureIv, pos)
    }

    private fun bindSearchHeader(holder: UniversalItemHolder) {
        val binding = holder.binding as ItemSearchHeaderBinding
        val pos = holder.layoutPosition
        val currentSearchHeader = (currentList[pos] as SearchHeader)
        val title = when (currentSearchHeader.type) {
            SearchHeader.SearchHeaderType.EVENT_HEADER -> R.string.search_header_item_event
            SearchHeader.SearchHeaderType.GUEST_HEADER -> R.string.search_header_item_guest
        }
        holder.bind(binding.root.context.string(title), vm, pos)
    }

    private fun bindSearchGuest(holder: UniversalItemHolder) {
        val binding = holder.binding as ItemSearchGuestBinding
        val pos = holder.layoutPosition
        val currentSearchGuest = (currentList[pos] as SearchGuest)
        holder.bind(currentSearchGuest, vm, pos)
        vm.prepareItemGuest(binding.guestPictureIv, pos)
    }

    class SearchDiffUtils(
        private val oldItems: List<SearchItemCombine>,
        private val newItems: List<SearchItemCombine>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val newItem = newItems[newItemPosition]
            val oldItem = oldItems[oldItemPosition]
            return if ((oldItem is SearchEvent && newItem is SearchEvent) && oldItem.event.eventId == newItem.event.eventId) true
            else if ((oldItem is SearchGuest && newItem is SearchGuest) && oldItem.guest.guestId == newItem.guest.guestId) true
            else oldItem is SearchHeader && newItem is SearchHeader
        }

        override fun getOldListSize() = oldItems.size

        override fun getNewListSize() = newItems.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItemPosition == newItemPosition
    }
}