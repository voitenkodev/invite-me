package com.voitenko.dev.inviteme.ui.new_event

import android.net.Uri
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import com.voitenko.dev.inviteme.R
import com.voitenko.dev.inviteme.base.AppRouter
import com.voitenko.dev.inviteme.base.BaseFragment
import com.voitenko.dev.inviteme.base.StateErrorApp
import com.voitenko.dev.inviteme.core.data.models.models.address.AddressApp
import com.voitenko.dev.inviteme.databinding.FragmentNewEventBinding
import com.voitenko.dev.inviteme.ui.components.map.PickAddressDialogFragment
import com.voitenko.dev.inviteme.utils.controller.addMotionController
import com.voitenko.dev.inviteme.utils.extensions.cutByRatio
import com.voitenko.dev.inviteme.utils.extensions.getBitmap
import com.voitenko.dev.inviteme.utils.extensions.navigateBack
import com.voitenko.dev.inviteme.utils.extensions.navigateTo
import com.voitenko.dev.inviteme.utils.extensions.navigateToGallery
import com.voitenko.dev.inviteme.utils.extensions.string
import com.voitenko.dev.inviteme.utils.extensions.toUiString
import com.voitenko.dev.inviteme.utils.extensions.ui.alphaTo
import com.voitenko.dev.inviteme.utils.extensions.ui.getDateTimeDialog
import com.voitenko.dev.inviteme.utils.extensions.ui.getSuccessSnackBar
import com.voitenko.dev.inviteme.utils.extensions.ui.loadImage
import com.voitenko.dev.inviteme.utils.extensions.ui.observe
import com.voitenko.dev.inviteme.utils.extensions.ui.shake
import com.voitenko.dev.inviteme.utils.extensions.ui.startAnimatedVector
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewEventFragment : BaseFragment<FragmentNewEventBinding>(R.layout.fragment_new_event) {

    override val viewModel by viewModel<NewEventViewModel>()

    override fun FragmentNewEventBinding.setupUi(b: Bundle?) {
        viewModel.observeNavigation()
        viewModel.observeVM()
        motionRoot.listenMotionAnimation()
        vm = viewModel
        nextTv.startAnimatedVector(R.drawable.ic_add_event_to_success_add)
        observeScreenResults()
    }

    override fun observeError(errorApp: StateErrorApp) {
        when (errorApp.localizedMessage) {
            R.string.error_input_event_title -> binding.titleEventEt.shake {}
            R.string.error_input_event_description -> binding.descriptionEventEt.shake {}
            R.string.error_input_event_address -> binding.addressIv.shake {}
            R.string.error_input_event_date -> binding.dateIv.shake {}
        }
        super.observeError(errorApp)
    }

    private fun NewEventViewModel.observeVM() =
        viewLifecycleOwner.observe(successAddEvent) { state ->
            state.processingUi(success = {
                requireActivity().getSuccessSnackBar(R.string.event_success_added_snack)
                    ?.apply { show(); doOnEnd(::backPress) }
            })
        }.observe(changeImage) { state ->
            state.processingUi(success = {})
        }

    private fun NewEventViewModel.observeNavigation() = observeNavigation(viewLifecycleOwner) {
        when (it) {
            is AppRouter.FromApp.ToGallery -> galleryResult.navigateToGallery()
            is AppRouter.FromApp.ToBack -> this@NewEventFragment.navigateBack()
            is AppRouter.FromNewEvent.ToDateTimePicker -> startDateTimePicker()
            is AppRouter.FromNewEvent.ToMapPicker -> navigateTo(NewEventFragmentDirections.newEventToMap())
        }
    }

    private fun observeScreenResults() =
        setFragmentResultListener(PickAddressDialogFragment.MAP_RESULT) { _, bundle ->
            bundle.getParcelable<AddressApp>(PickAddressDialogFragment.MAP_ADDRESS_RESULT)?.let {
                viewModel.setAddress.invoke(it)
                binding.addressEt.setText(it.name)
            }
        }

    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            binding.eventPictureIv.loadImage(uri, null)
            uri?.getBitmap(requireContext())
                ?.cutByRatio(requireContext().string(R.string.default_ratio_image))
                ?.let { bitmap -> viewModel.changeImageEvent(bitmap) } ?: viewModel.bitmapError()
        }

    override fun backPress() {
        lifecycleScope.launch {
            binding.nextTv.startAnimatedVector(R.drawable.ic_success_add_to_add_event)
            binding.eventPictureIv.alphaTo(0F)
            viewModel.navigateBackEvent()
        }
    }

    private fun startDateTimePicker() =
        context?.getDateTimeDialog(R.string.date_time_picker_title, { selectedDate ->
            selectedDate?.let { date ->
                binding.dateEt.setText(date.toUiString())
                viewModel.changeDateEvent(date)
            }
        })?.show(childFragmentManager, "dialog_time")

    private fun MotionLayout.listenMotionAnimation() =
        addMotionController(viewLifecycleOwner.lifecycle)
            .doOnStart(R.id.openedDateState) {
                binding.addressEt.clearFocus().also { hideKeyboard() }
            }.doOnStart(R.id.openedLocationState) {
                binding.dateEt.clearFocus().also { hideKeyboard() }
            }
}