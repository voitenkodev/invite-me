package com.voitenko.dev.inviteme.base

import android.net.Uri
import android.os.Bundle
import com.voitenko.dev.inviteme.core.data.models.models.EventApp
import com.voitenko.dev.inviteme.utils.AdapterTransitionBundle

sealed class AppRouter {
    sealed class FromSplash {
        object ToMain : AppRouter()
    }

    sealed class FromHome {
        object ToSearch : AppRouter()
        object ToEvent : AppRouter()
        data class ToEventInfo(val adapterBundle: AdapterTransitionBundle, val eventId: Long) :
            AppRouter()
    }

    sealed class FromSearch {
        data class ToEventInfo(val adapterBundle: AdapterTransitionBundle, val eventId: Long) :
            AppRouter()

        data class ToGuestInfo(val adapterBundle: AdapterTransitionBundle, val guestId: Long) :
            AppRouter()
    }

    sealed class FromNewEvent {
        object ToDateTimePicker : AppRouter()
        object ToMapPicker : AppRouter()
        sealed class FromMapDialog {
            data class ToNewEvent(val key: String, val b: Bundle) : AppRouter()
        }
    }

    sealed class FromEventInfo {
        data class ToScanner(val eventId: Long) : AppRouter()
        data class ToAddGuest(val eventId: Long, val eventImageUri: Uri) : AppRouter()
    }

    sealed class FromApp {
        object ToGallery : AppRouter()
        object ToBack : AppRouter()
        object ToGpsSettings : AppRouter()
        object ToFinishActivity : AppRouter()
        data class ToMap(val url: String) : AppRouter()
        data class ToEmailGuest(val emailGuest: String, val event: EventApp, val uriQrCode: Uri) :
            AppRouter()
    }
}