package com.voitenko.dev.inviteme.utils

//import android.Manifest
//import androidx.test.ext.junit.rules.ActivityScenarioRule
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import androidx.test.platform.app.InstrumentationRegistry
//import androidx.test.rule.GrantPermissionRule
//import com.voitenko.dev.inviteme.MainActivity
//import io.mockk.mockkClass
//import kotlinx.coroutines.ExperimentalCoroutinesApi
//import org.junit.Rule
//import org.junit.runner.RunWith
//import org.koin.android.ext.koin.androidContext
//import org.koin.core.logger.Level
//import org.koin.test.KoinTest
//import org.koin.test.KoinTestRule
//import org.koin.test.mock.MockProviderRule
//
//@RunWith(AndroidJUnit4::class)
//open class BaseTest : KoinTest {
//
//    @get:Rule
//    val mockProvider =
//        MockProviderRule.create { clazz -> mockkClass(clazz, relaxed = true) }
//
//    @ExperimentalCoroutinesApi
//    @ExperimentalStdlibApi
//    @get:Rule
//    val koinRule = KoinTestRule.create {
//        printLogger(Level.DEBUG)
//        androidContext(InstrumentationRegistry.getInstrumentation().targetContext)
//        modules(com.voitenko.dev.inviteme.modules)
//    }
//
//    @get:Rule
//    val scenario = ActivityScenarioRule(MainActivity::class.java)
//
//    @get:Rule
//    var mRuntimePermissionRule: GrantPermissionRule =
//        GrantPermissionRule.grant(
//            Manifest.permission.CAMERA,
//            Manifest.permission.INTERNET,
//            Manifest.permission.VIBRATE
//        )
//
//}