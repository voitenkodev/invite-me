package com.voitenko.dev.inviteme.utils

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.voitenko.dev.inviteme.App
import kotlinx.coroutines.ExperimentalCoroutinesApi

//class InstrumentalTestRunner : AndroidJUnitRunner() {
//    @ExperimentalCoroutinesApi
//    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application =
//        super.newApplication(cl, App::class.java.name, context)
//}