package com.voitenko.dev.inviteme.screens
//
//import android.os.SystemClock
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.assertion.ViewAssertions
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.filters.LargeTest
//import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
//import com.voitenko.dev.inviteme.R
//import com.voitenko.dev.inviteme.utils.BaseTest
//import org.junit.After
//import org.junit.Before
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.koin.test.inject
//
//@RunWith(AndroidJUnit4ClassRunner::class)
//@LargeTest
//class NewEventScreenTest : BaseTest() {
//
//    @Before
//    fun setUp() {
//    }
//
//    @After
//    fun tearDown() {
//    }
//
//    @Test
//    fun test_navigation_home_to_new_event() {
//        // given
//        Espresso.onView(ViewMatchers.withId(R.id.eventPager))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        Espresso.onView(ViewMatchers.withId(R.id.newEventBtn)).perform(ViewActions.click())
//        SystemClock.sleep(500)
//
//        // result
//        Espresso.onView(ViewMatchers.withId(R.id.addressEt))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        Espresso.onView(ViewMatchers.withId(R.id.titleEventEt))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        Espresso.onView(ViewMatchers.withId(R.id.descriptionEventEt))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//
//        // reverse test
//        Espresso.pressBack()
//        SystemClock.sleep(500)
//        Espresso.onView(ViewMatchers.withId(R.id.eventPager))
//            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//    }
//}