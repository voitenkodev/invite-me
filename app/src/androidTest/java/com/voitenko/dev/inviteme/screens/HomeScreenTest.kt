package com.voitenko.dev.inviteme.screens

//import android.os.SystemClock
//import androidx.test.espresso.Espresso.onView
//import androidx.test.espresso.Espresso.pressBack
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
//import androidx.test.espresso.matcher.ViewMatchers.withId
//import androidx.test.filters.LargeTest
//import com.voitenko.dev.inviteme.R
//import com.voitenko.dev.inviteme.core.domain.event_uc.GetSortedEventsWithGuestsUc
//import com.voitenko.dev.inviteme.core.data.models.models.home.HomeEventWithGuests
//import com.voitenko.dev.inviteme.utils.BaseTest
//import io.mockk.every
//import kotlinx.coroutines.flow.flowOf
//import org.junit.After
//import org.junit.Before
//import org.junit.Test
//
//@LargeTest
//class HomeScreenTest : BaseTest() {
//
//    @ExperimentalStdlibApi
//    @Before
//    fun setUp() {
//        declareMock<GetSortedEventsWithGuestsUc> {
//            every { this@declareMock.invoke() } returns flowOf(emptyList<HomeEventWithGuests>())
//        }
//    }
//
//    @After
//    fun tearDown() {
//    }
//
//    @Test
//    fun test_navigation_home_to_new_event() {
//
//        // given
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.newEventBtn)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withId(R.id.addressEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.titleEventEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.descriptionEventEt)).check(matches(isDisplayed()))
//
//        // reverse test
//        pressBack()
//        SystemClock.sleep(500)
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//    }

//    @Test
//    fun test_navigation_home_to_new_guest() {
//        // given
//        runBlocking {
//            db.daoEvent()
//                .setEvent(EVENT_1.copy(date = DEFAULT_CALENDAR.apply { set(Calendar.YEAR, 4020) }))
//        }
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.addBtn)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withId(R.id.nameGuestEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.phoneGuestEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.emailGuestEt)).check(matches(isDisplayed()))
//
//        // reverse test
//        pressBack()
//        SystemClock.sleep(500)
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun test_navigation_home_to_event_info() {
//        // given
//        runBlocking {
//            db.daoEvent()
//                .setEvent(EVENT_1.copy(date = DEFAULT_CALENDAR.apply { set(Calendar.YEAR, 4020) }))
//        }
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.title)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.description)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.address)).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.eventPictureIv)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withText(EVENT_1.title)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.description)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.address)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.date.toUiString())).check(matches(isDisplayed()))
//
//        // reverse test
//        pressBack()
//        SystemClock.sleep(500)
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.title)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.description)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.address)).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun test_change_theme() {
//        // given
//        runBlocking { pref.setNightMode(true) }
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withId(R.id.darkModeSwitch)).check(matches(isChecked()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.darkModeSwitch)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withId(R.id.darkModeSwitch)).check(matches(isNotChecked()))
//    }
//
//    @Test
//    fun test_navigation_home_to_camera() {
//        // given
//        runBlocking {
//            db.daoEvent()
//                .setEvent(EVENT_1.copy(date = DEFAULT_CALENDAR.apply { set(Calendar.YEAR, 4020) }))
//        }
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.cameraBtn)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withId(R.id.scannerView)).check(matches(isDisplayed()))
//
//        // reverse test
//        pressBack()
//        SystemClock.sleep(500)
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun test_navigation_home_to_search() {
//        // given
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.searchBtn)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withId(R.id.searchField)).check(matches(isDisplayed()))
//        onView(withId(R.id.searchRv)).check(matches(isDisplayed()))
//
//        // reverse test
//        pressBack()
//        SystemClock.sleep(500)
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun test_create_event_flow() {
//        // given
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.newEventBtn)).perform(click())
//        SystemClock.sleep(500)
//        onView(withId(R.id.addressEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.titleEventEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.descriptionEventEt)).check(matches(isDisplayed()))
//
//        // filling fields
//        onView(withId(R.id.addressEt)).perform(typeText("country, city, home"), closeSoftKeyboard())
//        onView(withId(R.id.dateIv)).perform(click())
//        onView(withId(R.id.dateEt)).perform(click())
//        onView(withText(R.string.date_time_picker_ok)).perform(click())
//        onView(withId(R.id.titleEventEt)).perform(
//            typeText("title title title"), closeSoftKeyboard()
//        )
//        onView(withId(R.id.descriptionEventEt)).perform(
//            typeText("description description"), closeSoftKeyboard()
//        )
//
//        // save event
//        onView(withId(R.id.nextTv)).perform(click())
//        onView(withId(R.id.snack)).check(matches(isDisplayed()))
//        SystemClock.sleep(3500)
//
//        // back to home
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withText("title title title")).check(matches(isDisplayed()))
//        onView(withText("description description")).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun test_add_guest_flow() {
//        // given
//        runBlocking {
//            db.daoEvent()
//                .setEvent(EVENT_1.copy(date = DEFAULT_CALENDAR.apply { set(Calendar.YEAR, 4020) }))
//        }
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withId(R.id.countGuestIv)).check(matches(isDisplayed()))
//        onView(withId(R.id.countGuestIv)).check(matches(withText(containsString("0"))))
//        onView(withText(EVENT_1.title)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.description)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.address)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.date.toUiString())).check(matches(isDisplayed()))
//        SystemClock.sleep(500)
//
//        // test
//        onView(withId(R.id.addBtn)).perform(click())
//        SystemClock.sleep(500)
//
//        // result
//        onView(withId(R.id.nameGuestEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.phoneGuestEt)).check(matches(isDisplayed()))
//        onView(withId(R.id.emailGuestEt)).check(matches(isDisplayed()))
//
//        // filling fields
//        onView(withId(R.id.emailGuestEt)).perform(typeText(GUEST_1.email), closeSoftKeyboard())
//        onView(withId(R.id.saveIv)).perform(click())
//        SystemClock.sleep(1500) // add qr code
//        onView(withId(R.id.nameGuestEt)).perform(typeText(GUEST_1.guestName), closeSoftKeyboard())
//        onView(withId(R.id.phoneGuestEt)).perform(typeText(GUEST_1.phone), closeSoftKeyboard())
//
//        // save guest
//        onView(withId(R.id.saveIv)).perform(click())
//        onView(withId(R.id.snack)).check(matches(isDisplayed()))
//        SystemClock.sleep(3500)
//
//        // back to home
//        onView(withId(R.id.eventPager)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.title)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.description)).check(matches(isDisplayed()))
//        onView(withText(EVENT_1.address)).check(matches(isDisplayed()))
//        onView(withId(R.id.countGuestIv)).check(matches(isDisplayed()))
//        onView(withId(R.id.countGuestIv)).check(matches(withText(containsString("1"))))
//        onView(withText(EVENT_1.date.toUiString())).check(matches(isDisplayed()))
//    }
//}