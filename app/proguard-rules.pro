# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.kts.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# For androidx.fragment.app.FragmentContainerView
-keepnames class androidx.navigation.fragment.NavHostFragment

# For ktx.Serializable
-keepclassmembers class com.voitenko.dev.inviteme.core.data.models.dto** {*;}

# For Ktor
-keep public class com.voitenko.dev.inviteme.core.data.datasource.remote.address_api** { public <methods>; private <methods>;}
-keep public class io.ktor**  { public <methods>; private <methods>; }
